cmake_minimum_required(VERSION 3.14)
project(rshow LANGUAGES CXX)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY $ENV{RDE_DIR})
find_package(Qt6 REQUIRED COMPONENTS Gui Quick Qml DBus)
find_package(LayerShellQt REQUIRED)
add_executable(rshow
    main.cpp
    Application.h Application.cpp
    Window.h Window.cpp
    Plugin.h Plugin.cpp
    PluginModel.h PluginModel.cpp
    PluginInterface.h
)
qt_add_resources(rshow "qml"
    FILES
    Surface.qml
    ConfigWindow.qml    
)
qt6_add_qml_module(rshow
    URI org.rde.rshow
    VERSION 1.0
)
target_link_libraries(rshow PRIVATE Qt6::Gui Qt6::Quick Qt6::Qml Qt6::DBus LayerShellQt::Interface)
add_subdirectory(Plugin/Camera)
add_subdirectory(Plugin/Picture)
add_subdirectory(Plugin/Visualizer)