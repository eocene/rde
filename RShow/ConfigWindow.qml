import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.rde.ui
import org.rde.rshow
Window{
    color:Qt.rgba(0,0,0,0.5)
    width:1280
    //height:layout.contentHeight
    height:720
    visible:true
    //flags:Qt.Dialog
    modality:Qt.WindowModal


    RowLayout{
        anchors.fill:parent
        spacing:0
        RList{
            id:list
            Layout.preferredWidth:300
            Layout.fillHeight:true
            //anchors.fill:parent
            model:PluginModel{id:pluginModel}
            delegate:delegate
            onCurrentItemChanged:console.log(currentItem.text)

            // delegate:CheckBox{
            //     text:model.display
            //     checked:Plugin.loaded
            //     onCheckedChanged:Plugin.loaded=checked
            // }
        }
        Rectangle{
            id:configPanel
            Layout.fillWidth:true
            Layout.fillHeight:true
            color:'black'
        }
    }
    Component{
        id:delegate
        RListDelegate{
            width:list.width
            onPressed:list.currentIndex=index
            CheckBox{
                anchors{right:parent.right;verticalCenter:parent.verticalCenter}
                checked:Plugin.loaded
                onCheckedChanged:Plugin.loaded=checked
            }
        }
    }


    /*
    ColumnLayout{
        id:layout
        anchors.fill:parent
        spacing:0
        RGroupBox{
            id:pictureGroup
            Layout.fillWidth:true
            Layout.fillHeight:true
            title:'Picture'
            checkBox{
                //checked:pictureLayer
                onCheckedChanged:{
                    if(checkBox.checked){
                        pictureLayer=Qt.createComponent("Layer/PictureLayer.qml").createObject(surface);
                        pictureLayer.config.createObject(pictureConfig)
                    }
                    else{
                        pictureLayer.destroy()
                        pictureLayer=null
                        pictureConfig.children[0].destroy()
                    }
                }
            }
            Item{
                id:pictureConfig
                anchors.fill:parent
            }
        }
        RGroupBox{
            id:cameraGroup
            Layout.fillWidth:true
            Layout.fillHeight:true
            title:'Camera'
            checkBox{
                //checked:cameraLayer
                onCheckedChanged:{
                    if(checkBox.checked){
                        cameraLayer=Qt.createComponent("Layer/CameraLayer.qml").createObject(surface);
                        cameraLayer.config.createObject(cameraConfig)
                    }
                    else{
                        cameraLayer.destroy()
                        cameraLayer=null
                        cameraConfig.children[0].destroy()
                    }
                }
            }
            Item{
                id:cameraConfig
                anchors.fill:parent
            }
        }
        RGroupBox{
            id:visualizerGroup
            Layout.fillWidth:true
            Layout.fillHeight:true
            title:'Visualizer'
            checkBox{
                //checked:visualizerLayer
                onCheckedChanged:{
                    if(checkBox.checked){
                        visualizerLayer=Qt.createComponent("Layer/VisualizerLayer.qml").createObject(surface);
                        visualizerLayer.config.createObject(visualizerConfig)
                    }
                    else{
                        visualizerLayer.destroy()
                        visualizerLayer=null
                        visualizerConfig.children[0].destroy()
                    }
                }
            }
            Item{
                id:visualizerConfig
                anchors.fill:parent
            }
        }
    }
    onClosing:destroy() // for some reason this makes picture layer stop swapping
    Component.onDestruction:configWindow=null
    */
}