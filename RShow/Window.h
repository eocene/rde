#ifndef WINDOW_H
#define WINDOW_H
#include <QQuickItem>
#include <QQuickWindow>
#include <QQmlComponent>
//#include <QQmlContext>
#include <QQmlEngine>
#include <LayerShellQt/Shell>
#include <LayerShellQt/Window>
class Window:public QQuickWindow
{
    Q_OBJECT
public:
    static Window* instance();
    QQmlEngine* engine(){return m_engine;}
private:
    Window();
    static Window* s_instance;
    QQmlEngine* m_engine=nullptr;
    QQmlComponent* m_component=nullptr;
    QQuickItem* m_item=nullptr;
};
#endif