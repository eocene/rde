#include "Plugin.h"
Plugin::Plugin(const QString& file):QPluginLoader(file)
{
    QJsonObject meta=metaData();
    m_iid=meta.value("IID").toString();
    QJsonObject metaData=meta.value("MetaData").toObject();
    m_name=metaData.value("Name").toString();
    m_icon=metaData.value("Icon").toString();
}
void Plugin::toggle(bool on)
{
    if(on){
        if(load()){
            m_instance=instance();
            m_instance->setParent(this);
            if(m_instance){
                m_interface=qobject_cast<PluginInterface*>(m_instance);

                QSettings settings;
                settings.beginGroup(m_name);
                m_interface->load(&settings);
                QResource layer("/"+m_iid+"/Layer.qml");
                if(layer.isValid()){
                    m_context=new QQmlContext(Window::instance()->engine()->rootContext(),m_instance);
                    m_context->setContextProperty("Plugin",m_instance);
                    m_layer=new QQmlComponent(Window::instance()->engine(),this);
                    connect(m_layer,&QQmlComponent::statusChanged,[=](QQmlComponent::Status status){
                        switch(status){
                        case QQmlComponent::Null:break;
                        case QQmlComponent::Loading:
                            //qDebug() << "Loading layer for" << m_name;
                            break;
                        case QQmlComponent::Ready:{
                            m_item=qobject_cast<QQuickItem*>(m_layer->create(m_context));
                            m_item->setParentItem(Window::instance()->contentItem());
                            m_item->setParent(m_instance);
                            m_layer->deleteLater();
                            m_layer=nullptr;
                            break;
                        }
                        case QQmlComponent::Error:
                            qDebug() << m_layer->errorString();
                            break;
                        }
                    });
                    m_layer->loadUrl(QUrl("qrc"+layer.absoluteFilePath()),QQmlComponent::Asynchronous);
                }
            }
        }else
            qWarning() << "Plugin" << m_name << "failed to load:" << errorString();
    }
    else{
        if(unload()){
            m_instance=nullptr;
            m_interface=nullptr;
            m_context=nullptr;
            m_item=nullptr;
        }else{
            qWarning() << "Plugin" << m_name << "failed to unload:" << errorString();
        }
    }
}
void Plugin::loadPlugins()
{
    QDir dir("/usr/lib/rde/rshow","*.so");
    if(dir.isReadable()){
        foreach(QFileInfo info,dir.entryInfoList()){
            Plugin* plugin=new Plugin(info.absoluteFilePath());
            if(plugin->iid().startsWith("org.rde.rshow.")){
                plugin->setParent(Window::instance());
                s_plugins.append(plugin);
            }
            else
                plugin->deleteLater();
        }
    }
}
Plugin::~Plugin()
{
    //delete m_interface;
    //QSettings settings;
    //settings.beginGroup(m_name);
    //m_interface->save(&settings);
}
QList<Plugin*> Plugin::s_plugins;