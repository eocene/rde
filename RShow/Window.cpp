#include "Window.h"
Window::Window()
{
    setColor(QColor(10,10,10,255));

    LayerShellQt::Window* l=LayerShellQt::Window::get(this);
    l->setKeyboardInteractivity(LayerShellQt::Window::KeyboardInteractivityNone);
    l->setLayer(LayerShellQt::Window::LayerBackground);
    l->setExclusiveZone(-1);    
    //l->setScreenConfiguration(LayerShellQt::Window::ScreenFromQWindow);

    m_engine=new QQmlEngine(this);
    m_component=new QQmlComponent(m_engine,this);

    connect(m_component,&QQmlComponent::statusChanged,[=](QQmlComponent::Status status){
        switch(status){
        case QQmlComponent::Null:break;
        case QQmlComponent::Loading:break;
        case QQmlComponent::Ready:{
            m_item=qobject_cast<QQuickItem*>(m_component->create(m_engine->rootContext()));
            m_item->setParent(this);
            m_item->setParentItem(contentItem());
            m_component->deleteLater();
            m_component=nullptr;
            showFullScreen();
            break;
        }
        case QQmlComponent::Error:{
            qWarning() << m_component->errors();
            m_component->deleteLater();
            m_component=nullptr;
            break;
        }
        }
    });
    m_component->loadUrl(QUrl("qrc:/Surface.qml"),QQmlComponent::Asynchronous);
}
Window* Window::s_instance;
Window* Window::instance(){if(!s_instance){LayerShellQt::Shell::useLayerShell();s_instance=new Window;}return s_instance;}