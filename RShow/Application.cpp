#include "Application.h"
Application::Application(int &argc,char **argv):QGuiApplication(argc,argv)
{
    setOrganizationName("rde");
    setApplicationName("rshow");
    setApplicationDisplayName("RShow");
    connect(Window::instance(),&QQuickWindow::closing,[=](){Window::instance()->deleteLater();});
    Plugin::loadPlugins();
}
void Application::quit()
{
    qGuiApp->quit();
}
Application::~Application()
{
}