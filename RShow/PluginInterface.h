#ifndef PLUGININTERFACE_H
#define PLUGININTERFACE_H
#include <QtPlugin>
#include <QSettings>
class PluginInterface
{
public:
    virtual ~PluginInterface(){}
    virtual void load(QSettings* settings){}
    virtual void save(QSettings* settings){}
};
Q_DECLARE_INTERFACE(PluginInterface,"org.rde.rshow.plugin")
#endif