#ifndef APPLICATION_H
#define APPLICATION_H
#include <QGuiApplication>
#include "Plugin.h"
#include "Window.h"
class Application:public QGuiApplication
{
    Q_OBJECT
public:
    explicit Application(int &argc,char **argv);
    ~Application();
public slots:
    Q_SCRIPTABLE void quit();
};
#endif