import QtQuick
//import org.rde.rshow
// import 'Layer'
Item{
    id:surface
    anchors.fill:parent
    property ConfigWindow configWindow
    // property PictureLayer pictureLayer
    // property CameraLayer cameraLayer
    // property VisualizerLayer visualizerLayer
    MouseArea{
        anchors.fill:parent
        acceptedButtons:Qt.RightButton
        onClicked:configWindow=Qt.createComponent("ConfigWindow.qml").createObject(surface)
    }
}