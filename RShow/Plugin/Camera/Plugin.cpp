#include "Plugin.h"
Plugin::Plugin()
{
    QMediaDevices devices;
    // won't fire due to a bug: https://github.com/qt/qtmultimedia/commit/7b75983575518109a4630058149d74819bf0cf64
    connect(&devices,&QMediaDevices::videoInputsChanged,this,&Plugin::onVideoInputsChanged);
    onVideoInputsChanged();
}
void Plugin::load(QSettings* settings)
{
    //m_camera->setZoomFactor(settings.value("zoom",m_camera->minimumZoomFactor()).toFloat());
}
void Plugin::save(QSettings* settings)
{
    //QSettings settings;
    //settings.beginGroup("Camera");
    //settings->setValue("zoom",m_camera->zoomFactor());
}
void Plugin::onVideoInputsChanged()
{
    QList<QCameraDevice> inputs=QMediaDevices::videoInputs();
    if(!inputs.isEmpty()){
        m_camera=new QCamera(inputs.first(),this);
        connect(m_camera,&QCamera::activeChanged,[=](bool active){
            // m_working=watching;
            // emit workingChanged();
            // if(watching)
            //     //emit speak("I can see!");
            // else{
            //     m_camera->deleteLater();
            //     //emit speak("I'm blind!");
            // }
        });
        connect(m_camera,&QCamera::errorOccurred,[=](QCamera::Error error,const QString& errorString){
            switch(error){
            case QCamera::NoError:break;
            case QCamera::CameraError:{
                //qWarning() << "RCam:" << errorString;
                break;
            }
            }
        });
        //        connect(m_camera,&QCamera::cameraDeviceChanged,this,&Eye::onCameraDeviceChanged);
        //        connect(m_camera,&QCamera::cameraFormatChanged,this,&Eye::onCameraFormatChanged);
        //        connect(m_camera,&QCamera::errorChanged,this,&Eye::onErrorChanged);
        //        connect(m_camera,&QCamera::supportedFeaturesChanged,this,&Eye::onSupportedFeaturesChanged);
        connect(m_camera,&QCamera::torchModeChanged,[=](){
            switch(m_camera->torchMode()){
            case QCamera::TorchOff:/*emit speak("torch off")*/;break;
            case QCamera::TorchOn:/*emit speak("torch on")*/;break;
            case QCamera::TorchAuto:/*emit speak("torch auto")*/;break;
            }
        });
        //        connect(m_camera,&QCamera::colorTemperatureChanged,this,&Eye::onColorTemperatureChanged);

        connect(m_camera,&QCamera::focusDistanceChanged,[=](const float& distance){
            //qDebug() << "Camera focus distance:" << distance;
        });
        connect(m_camera,&QCamera::manualExposureTimeChanged,this,[=](const float& speed){
            //qDebug() << "Camera manual exposure time:" << speed;
        });
        connect(m_camera,&QCamera::whiteBalanceModeChanged,[=](){
            switch(m_camera->whiteBalanceMode()){
            case QCamera::WhiteBalanceAuto:break;
            case QCamera::WhiteBalanceManual:break;
            case QCamera::WhiteBalanceSunlight:break;
            case QCamera::WhiteBalanceCloudy:break;
            case QCamera::WhiteBalanceShade:break;
            case QCamera::WhiteBalanceTungsten:break;
            case QCamera::WhiteBalanceFluorescent:break;
            case QCamera::WhiteBalanceFlash:break;
            case QCamera::WhiteBalanceSunset:break;
            }
        });
        m_camera->setActive(true);
    }
    else{
        // no cam?
    }
}
Plugin::~Plugin()
{
}