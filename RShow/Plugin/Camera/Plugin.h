#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
#include <QMediaDevices>
#include <QCamera>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rshow.camera" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
    Q_PROPERTY(QCamera* camera READ camera CONSTANT)
    Q_PROPERTY(float zoom READ zoom WRITE setZoom NOTIFY zoomChanged)
    Q_PROPERTY(float minZoom READ minZoom CONSTANT)
    Q_PROPERTY(float maxZoom READ maxZoom CONSTANT)
public:
    Plugin();
    ~Plugin() override;
    void load(QSettings* settings) override;
    void save(QSettings* settings) override;
    QCamera* camera(){return m_camera;}
    inline float minZoom()const{return m_camera->minimumZoomFactor();}
    inline float maxZoom()const{return m_camera->maximumZoomFactor();}
    inline float zoom()const{return m_camera->zoomFactor();}
    void setZoom(float zoom){m_camera->setZoomFactor(zoom);emit zoomChanged();}
private:
    QCamera* m_camera=nullptr;
private slots:
    void onVideoInputsChanged();
signals:
    void zoomChanged();
};
#endif