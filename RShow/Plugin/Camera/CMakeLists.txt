cmake_minimum_required(VERSION 3.14)
project(Camera LANGUAGES CXX)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "$ENV{RDE_DIR}/rshow-plugin")
find_package(Qt6 REQUIRED COMPONENTS Multimedia)
add_library(Camera SHARED
    Plugin.h Plugin.cpp
)
qt_add_resources(Camera "qml"
    PREFIX "org.rde.rshow.camera"
    FILES
    Layer.qml
    Config.qml
)
target_link_libraries(Camera PRIVATE Qt6::Multimedia)
target_compile_definitions(Camera PRIVATE CAMERA_LIBRARY)
set_target_properties(Camera PROPERTIES PREFIX "")