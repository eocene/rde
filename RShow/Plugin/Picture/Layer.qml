import QtQuick
import QtQuick.Effects
WindowContainer{
    anchors.fill:parent
    property int nextPic
    window:Window{
        id:window
        flags:Qt.WindowTransparentForInput|Qt.WindowStaysOnBottomHint|Qt.WindowDoesNotAcceptFocus
        color:'transparent'



        onFrameSwapped:{
            if(strip.atXEnd){picture.createObject(row)}
            if(nextPic===0)nextPic=row.children[0].paintedWidth
            if(strip.contentX===nextPic){row.children[0].destroy();nextPic=0;strip.contentX=0}
            strip.contentX++
        }



        // Connections{
        //     target:window
        //     enabled:Plugin.scroll
        //     function onFrameSwapped(){
        //         if(strip.atXEnd){picture.createObject(row)}
        //         if(nextPic===0)nextPic=row.children[0].paintedWidth
        //         if(strip.contentX===nextPic){row.children[0].destroy();nextPic=0;strip.contentX=0}
        //         strip.contentX++
        //     }
        // }
        // Connections{
        //     target:Plugin
        //     // function onStarting(){
        //     //     update.enabled=false
        //     //     strip.contentX=0
        //     //     for(var i=0;i<row.children.length;i++){row.children[i].destroy()}
        //     // }
        // }
        Flickable{
            id:strip
            anchors.fill:parent
            interactive:false
            contentWidth:row.implicitWidth
            // transform:Rotation{
            //     id:rotation
            //     origin{x:controls.originX;y:controls.originY}
            //     axis{x:controls.axisX;y:controls.axisY;z:controls.axisZ}
            //     angle:controls.angle
            // }
            Row{
                id:row
                anchors.fill:parent
                spacing:0
            }
            layer.smooth:true
            layer.enabled:true
            layer.effect:MultiEffect{
                blurEnabled:true
                maskEnabled:true
                maskSource:mask
                maskSpreadAtMin:1
                maskSpreadAtMax:1
                maskThresholdMin:Plugin.maskThresholdMin
                maskThresholdMax:1
                brightness:Plugin.brightness
                saturation:Plugin.saturation
                blur:Plugin.blur
            }
        }
        Rectangle{
            id:mask
            layer.enabled:true
            anchors.fill:parent
            visible:false
            gradient:Gradient{
                orientation:Gradient.Horizontal
                GradientStop{position:0;color:Qt.rgba(0,0,0,0)}
                GradientStop{position:0.5;color:Qt.rgba(0,0,0,1)}
                GradientStop{position:1;color:Qt.rgba(0,0,0,0)}
            }
        }
        Component{
            id:picture
            AnimatedImage{
                height:parent.height
                sourceSize{height:height}
                fillMode:Image.PreserveAspectFit
                source:'file://'+Plugin.image
                mipmap:true
                smooth:true
                antialiasing:true
                cache:false
                asynchronous:true
            }
        }
    }
    Component.onCompleted:{Plugin.ready();}

}