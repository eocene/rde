#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
#include <QDirIterator>
#include <QFutureWatcher>
#include <QImageReader>
#include <QRandomGenerator>
#include <QtConcurrentRun>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rshow.picture" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
    Q_PROPERTY(bool scroll READ scroll NOTIFY scrollChanged)
    Q_PROPERTY(QString image READ image CONSTANT)
    Q_PROPERTY(QString path READ path WRITE setPath CONSTANT)
    Q_PROPERTY(qreal blur READ blur WRITE setBlur NOTIFY blurChanged)
    Q_PROPERTY(qreal brightness READ brightness WRITE setBrightness NOTIFY brightnessChanged)
    Q_PROPERTY(qreal saturation READ saturation WRITE setSaturation NOTIFY saturationChanged)
    Q_PROPERTY(qreal maskThresholdMin READ maskThresholdMin WRITE setMaskThresholdMin NOTIFY maskThresholdMinChanged)
public:
    Plugin();
    ~Plugin() override;
    void load(QSettings* settings) override;
    void save(QSettings* settings) override;
    inline QString image(){return m_images.at(QRandomGenerator::global()->bounded(m_images.size()));}
    inline QString path(){return m_path;}
    void setPath(const QString& path);
    inline qreal blur()const{return m_blur;}
    inline void setBlur(qreal blur){m_blur=blur;emit blurChanged();}
    inline qreal brightness()const{return m_brightness;}
    inline void setBrightness(qreal brightness){m_brightness=brightness;emit brightnessChanged();}
    inline qreal saturation()const{return m_saturation;}
    inline void setSaturation(qreal saturation){m_saturation=saturation;emit saturationChanged();}
    inline qreal maskThresholdMin()const{return m_maskThresholdMin;}
    inline void setMaskThresholdMin(qreal maskThresholdMin){m_maskThresholdMin=maskThresholdMin;emit maskThresholdMinChanged();}
    Q_INVOKABLE void ready();
    bool scroll(){return m_scroll;}
private:
    QStringList scanThread(QDir dir);
    QFutureWatcher<QStringList>* m_watcher=nullptr;
    QString m_path;
    qreal m_blur;
    qreal m_brightness;
    qreal m_saturation;
    qreal m_maskThresholdMin;
    QStringList m_images;
    bool m_scroll=false;
    QString m_fileTypes;
signals:
    void scrollChanged();
    void blurChanged();
    void saturationChanged();
    void brightnessChanged();
    void maskThresholdMinChanged();
};
#endif