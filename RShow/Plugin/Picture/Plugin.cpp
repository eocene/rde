#include "Plugin.h"
Plugin::Plugin()
{
    m_fileTypes="*."+QImageReader::supportedImageFormats().join(" *.");
}
void Plugin::load(QSettings* settings)
{
    m_path=settings->value("path").toString();
    m_blur=settings->value("blur").toReal();
    m_brightness=settings->value("brightness").toReal();
    m_saturation=settings->value("saturation").toReal();
    m_maskThresholdMin=settings->value("maskthresholdmin").toReal();     
}
void Plugin::save(QSettings* settings)
{
    settings->setValue("path",m_path);
    settings->setValue("blur",m_blur);
    settings->setValue("brightness",m_brightness);
    settings->setValue("saturation",m_saturation);
    settings->setValue("maskthresholdmin",m_maskThresholdMin);
}
void Plugin::ready()
{
    setPath(m_path);
}
void Plugin::setPath(const QString &path)
{
    if(!path.isEmpty()){
        QDir dir(path,m_fileTypes,QDir::Name);
        if(dir.isReadable()){
            m_watcher=new QFutureWatcher<QStringList>(this);
            connect(m_watcher,&QFutureWatcherBase::finished,[=](){
                m_images=m_watcher->result();



                m_watcher->deleteLater();
                m_scroll=true;



                emit scrollChanged();

                qDebug() << "SCROLL!!";
            });
            m_watcher->setFuture(QtConcurrent::run(&Plugin::scanThread,this,dir));
        }
    }
}
extern QStringList Plugin::scanThread(QDir dir)
{
    QStringList l;
    QDirIterator i(dir,QDirIterator::Subdirectories|QDirIterator::FollowSymlinks);
    while(i.hasNext())
        l.append(i.next());
    return l;
}
Plugin::~Plugin()
{
    qDebug() << "Destroying picture plugin";
}