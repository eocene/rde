#ifndef PROJECTM_H
#define PROJECTM_H
#include <QDirIterator>
#include <QFileInfo>
#include <QFutureWatcher>
#include <QtConcurrentRun>
#include <QOpenGLWindow>
#include <QQmlEngine>
#include <QRandomGenerator>
#include <projectM-4/projectM.h>
class ProjectM:public QOpenGLWindow
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(QString preset READ preset WRITE setPreset NOTIFY presetChanged)
public:
    ProjectM(){}
    ~ProjectM();
    void setPreset(const QString& preset);
    QString preset(){return m_preset;}
    Q_INVOKABLE void setRandomPreset();
    static QFileInfoList Presets;
    static QString TexturePath;
    //static float Level;
private:
    void setup_opengl(int w,int h);
    void initializeGL() override;
    void resizeGL(int w,int h) override;
    void inline paintGL() override {projectm_opengl_render_frame(m_handle);}
    projectm_handle m_handle;
    int m_timerId;
    inline void timerEvent(QTimerEvent* event){
        setRandomPreset();
    }
    QString m_preset;
    QFileInfoList scanPresets(QDir dir);
    QFutureWatcher<QFileInfoList>* m_watcher;
    QFileInfoList m_presets;
private slots:
    inline void feedAudio(const int16_t* data,int size){
        //projectm_pcm_add_int16(m_handle,data,size,projectm_channels(PROJECTM_STEREO));
    }
signals:
    void presetChanged();
};
#endif