#include "ProjectM.h"
extern QFileInfoList ProjectM::scanPresets(QDir dir)
{
    QFileInfoList l;
    QDirIterator i(dir,QDirIterator::Subdirectories);
    while(i.hasNext())
        l << i.nextFileInfo();
    return l;
}
void ProjectM::initializeGL()
{
    m_handle=projectm_create();
    projectm_set_fps(m_handle,60);
    projectm_set_mesh_size(m_handle,96,54);
    projectm_set_aspect_correction(m_handle,true);
    projectm_set_beat_sensitivity(m_handle,1);
    //const char* texturePaths[1]={TexturePath.toUtf8()};
    //projectm_set_texture_search_paths(m_handle,&texturePaths[0],1);
    connect(this,SIGNAL(frameSwapped()),this,SLOT(update()),Qt::UniqueConnection);


    QDir presetDir("/data/projectm/presets/","*.milk");
    if(presetDir.isReadable()){
        m_watcher=new QFutureWatcher<QFileInfoList>(this);
        connect(m_watcher,&QFutureWatcherBase::finished,[=](){
            m_presets=m_watcher->result();
            m_watcher->deleteLater();
            setRandomPreset();
            startTimer(10000);
        });
    }
    m_watcher->setFuture(QtConcurrent::run(&ProjectM::scanPresets,this,presetDir));
}
void ProjectM::resizeGL(int w,int h)
{
    setup_opengl(w,h);
    projectm_set_window_size(m_handle,static_cast<size_t>(w),static_cast<size_t>(h));
}
void ProjectM::setup_opengl(int w,int h)
{
    glShadeModel(GL_SMOOTH);
    //    glCullFace( GL_BACK );
    //    glFrontFace( GL_CCW );
    //    glEnable( GL_CULL_FACE );
    //glClearColor(0,0,0,0);
    //glClearColor(0,0,1,1);
    glViewport(0,0,w,h);
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    //    gluOrtho2D(0.0, (GLfloat) width, 0.0, (GLfloat) height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //    glFrustum(0.0, height, 0.0,width,10,40);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glDrawBuffer(GL_BACK);
    glReadBuffer(GL_BACK);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    // glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POINT_SMOOTH);
    glClearColor(0.0f,0.0f,0.0f,0.0f);
    //   glClear(GL_COLOR_BUFFER_BIT);
    // glCopyTexImage2D(GL_TEXTURE_2D,0,GL_RGB,0,0,texsize,texsize,0);
    //glCopyTexSubImage2D(GL_TEXTURE_2D,0,0,0,0,0,texsize,texsize);
    glLineStipple(2,0xAAAA);
}
void ProjectM::setPreset(const QString& preset)
{
    if(!preset.isEmpty()){
        QFileInfo info(preset);
        if(info.isReadable()){
            if(m_handle){
                m_preset=preset;
                projectm_load_preset_file(m_handle,preset.toLocal8Bit().data(),true);
                emit presetChanged();
            }else
                qWarning() << "ProjectM not initialized!";
        }
    }
}
void ProjectM::setRandomPreset()
{
    if(!m_presets.isEmpty()){
        int v=QRandomGenerator::global()->bounded(m_presets.size());
        setPreset(m_presets.at(v).absoluteFilePath());
    }
}
ProjectM::~ProjectM()
{
    disconnect(this,SIGNAL(frameSwapped()),this,SLOT(update()));
    projectm_destroy(m_handle);
}
QFileInfoList ProjectM::Presets;
QString ProjectM::TexturePath;
//float ProjectM::Level;