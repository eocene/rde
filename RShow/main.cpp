#define Service "org.rde.rshow"
#define Path "/org/rde/RShow"
#include <QDBusInterface>
#include "Application.h"
int main(int argc,char *argv[])
{
    if(QDBusConnection::systemBus().isConnected()){
        if(QDBusConnection::sessionBus().registerService(Service)){
            Application app(argc,argv);
            if(QDBusConnection::sessionBus().registerObject(Path,&app,QDBusConnection::ExportScriptableSlots))
                return app.exec();
            else return 3;
        }else{
            QDBusInterface i(Service,Path,"",QDBusConnection::sessionBus());
            if(i.isValid())
                i.call("quit");
            else return 2;
        }
    }else return 1;
}