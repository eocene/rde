#ifndef PLUGINMODEL_H
#define PLUGINMODEL_H
#include <QAbstractListModel>
#include <QQmlEngine>
#include "Plugin.h"
class PluginModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    PluginModel();
    QVariant data(const QModelIndex& index,int role) const override;
private:
    int rowCount(const QModelIndex &parent)const{Q_UNUSED(parent);return Plugin::s_plugins.size();}
    QHash<int,QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QAbstractListModel::roleNames();
        h[Qt::UserRole]="Plugin";
        return h;
    }
};
#endif