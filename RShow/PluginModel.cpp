#include "PluginModel.h"
PluginModel::PluginModel()
{
    qRegisterMetaType<Plugin*>("Plugin");
}
QVariant PluginModel::data(const QModelIndex& index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return QVariant(Plugin::s_plugins.at(index.row())->name());
    case Qt::DecorationRole:return QVariant(Plugin::s_plugins.at(index.row())->icon());
    case Qt::UserRole:return QVariant::fromValue(Plugin::s_plugins.at(index.row()));
    }
}