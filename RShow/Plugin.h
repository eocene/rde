#ifndef PLUGIN_H
#define PLUGIN_H
#include <QDir>
#include <QPluginLoader>
#include <QResource>
#include <QQmlContext>
#include "PluginInterface.h"
#include "Window.h"
class Plugin:public QPluginLoader
{
    Q_OBJECT
    Q_PROPERTY(bool loaded READ isLoaded WRITE toggle NOTIFY loadedChanged)
    Q_PROPERTY(QString name READ name CONSTANT)
public:
    explicit Plugin(const QString& file);
    ~Plugin();
    QString iid()const{return m_iid;}
    QString name()const{return m_name;}
    QString icon()const{return m_icon;}
    void toggle(bool on);
    static QList<Plugin*> s_plugins;
    static void loadPlugins();
private:
    QObject* m_instance=nullptr;
    PluginInterface* m_interface=nullptr;
    QQmlContext* m_context=nullptr;
    QQmlComponent* m_layer=nullptr;
    QQuickItem* m_item=nullptr;
    QString m_iid;
    QString m_name;
    QString m_icon;
signals:
    void loadedChanged();
};
#endif