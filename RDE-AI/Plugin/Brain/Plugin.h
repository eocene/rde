#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
#include "Adaptor.h"
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.ai.brain" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
public:
    QDBusAbstractAdaptor* adaptor()override{return new Adaptor(this);}
};
#endif