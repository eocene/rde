#include "Adaptor.h"
Adaptor::Adaptor(QObject* object):QDBusAbstractAdaptor(object)
{
    QSettings settings;
    settings.beginGroup("LocalAI");
    m_model=settings.value("model").toString();
    m_temperature=settings.value("temperature").toDouble();

    QUrl u("http://localhost:8080/v1/chat/completions");
    if(u.isValid()){
        m_url=u;
        m_manager=new QNetworkAccessManager(this);
        m_request=QNetworkRequest(m_url);
        m_request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    }
    else{
        qWarning() << "AI received bad URL";
    }
}
void Adaptor::ask(const QString& question)
{
    QElapsedTimer timer;
    QNetworkReply* reply=m_manager->post(m_request,json(question));
    timer.start();
    qDebug() << "Waiting for answer...";
    connect(reply,&QNetworkReply::finished,[=](){
        qDebug() << "TIME:" << timer.elapsed()/1000;
        if(reply->error()==QNetworkReply::NoError){
            QJsonDocument doc=QJsonDocument::fromJson(reply->readAll());
            QString r=doc.object().value("choices").toArray().at(0).toObject().value("message").toObject().value("content").toString();
            reply->deleteLater();
            emit speak(r);
        }else{
            qWarning() << reply->errorString();
        }
        reply->deleteLater();
    });
}
QByteArray Adaptor::json(const QString& question)
{
    QJsonObject obj;
    QJsonObject role;
    role.insert("role","user");
    QJsonObject content;
    content.insert("content",question);
    QJsonArray messages;
    messages.append(role);
    messages.append(content);
    obj.insert("messages",messages);
    obj.insert("model",m_model);
    obj.insert("temperature",m_temperature);
    QJsonDocument doc(obj);
    return doc.toJson();
}