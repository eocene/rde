#ifndef ADAPTOR_H
#define ADAPTOR_H
#include <QDBusAbstractAdaptor>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QElapsedTimer>
#include <QSettings>
class Adaptor:public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface","org.rde.ai.brain")
public:
    explicit Adaptor(QObject* object);
public slots:
    void ask(const QString& question);
private:
    QNetworkRequest m_request;
    QNetworkAccessManager* m_manager=nullptr;
    QUrl m_url;
    QString m_model;
    QByteArray json(const QString& question);
    double m_temperature;
signals:
    void speak(const QString& text);

};
#endif