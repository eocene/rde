#include "Adaptor.h"
Adaptor::Adaptor(QObject* object):QDBusAbstractAdaptor(object)
{
    m_format.setChannelConfig(QAudioFormat::ChannelConfigMono);
    m_format.setChannelCount(1);
    m_format.setSampleFormat(QAudioFormat::Int16);
    m_format.setSampleRate(16000);

    QSettings settings;
    settings.beginGroup("Vosk");

    QFileInfo info(settings.value("model").toString());
    if(info.isDir()){
        m_wakeWord=settings.value("wakeword","computer").toString();

        QAudioDevice device=QMediaDevices::defaultAudioInput();
        if(device.isFormatSupported(m_format)){
            qDebug() << "LISTENING TO" << device.description();

            m_model=vosk_model_new(info.absoluteFilePath().toUtf8());
            m_recognizer=vosk_recognizer_new(m_model,m_format.sampleRate());

            m_source=new QAudioSource(device,m_format);
            m_source->setBufferSize(4000);
            m_source->setVolume(8);
            connect(m_source,&QAudioSource::stateChanged,[=](QAudio::State state){
                switch(state){
                case QAudio::ActiveState:
                    qDebug() << "VOSK ACTIVE";
                    break;
                case QAudio::SuspendedState:
                    qDebug() << "VOSK SUSPENDED";
                    break;
                case QAudio::StoppedState:
                    qDebug() << "VOSK STOPPED";
                    break;
                case QAudio::IdleState:
                    qDebug() << "VOSK IDLE";
                    break;
                }
            });

            emit setActive(true);
            //suspend(false);
        }
        else
            qWarning() << "DEVICE" << device.description() << "DOES NOT SUPPORT FORMAT!";
    }
    else{
        qDebug() << "WRONG MODEL=" << info.absoluteFilePath();
    }


    connect(this,&Adaptor::setActive,[=](const bool& active){
        if(active){
            m_device=m_source->start();
            connect(m_device,&QIODevice::readyRead,[=](){
                QAudioBuffer buffer(m_device->readAll(),m_format);
                if(vosk_recognizer_accept_waveform(m_recognizer,buffer.constData<char>(),buffer.byteCount())){
                    QString text=QJsonDocument::fromJson(vosk_recognizer_result(m_recognizer)).object().value("text").toString();
                    if(!text.isEmpty()){
                        if(m_wake)
                            process(text);
                        if(!m_wake && text==m_wakeWord){
                            //emit attention();
                            m_wake=true;
                        }
                    }
                }
            });
        }else{
            m_source->stop();
        }

        m_active=active;
    });
}
void Adaptor::process(const QString& text)
{
    if(text=="stop listening"){
        //emit confirm();
        m_repeat=false;
        m_wake=false;
    }
    else if(text=="repeat after me"){
        //emit confirm();
        m_repeat=true;
    }
    // else{
    //     if(m_repeat)
    //         emit speak(text);
    //     else{
    //         emit askAI(text);
    //     }
    // }
}