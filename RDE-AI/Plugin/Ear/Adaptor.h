#ifndef ADAPTOR_H
#define ADAPTOR_H
#include <QAudioBuffer>
#include <QAudioSource>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMediaDevices>
#include <QDBusAbstractAdaptor>
#include <QFileInfo>
#include <QSettings>
#include "vosk_api.h"
class Adaptor:public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface","org.rde.ai.ear")
    Q_PROPERTY(bool active READ active WRITE setActive)
public:
    explicit Adaptor(QObject* object);
    bool active()const{return m_active;}
private:
    QString m_modelName;
    QAudioFormat m_format;
    QAudioSource* m_source=nullptr;
    QIODevice* m_device=nullptr;
    VoskModel* m_model=nullptr;
    VoskRecognizer* m_recognizer=nullptr;
    QString m_wakeWord=nullptr;
    bool m_wake=false;
    bool m_repeat=false;
    void process(const QString& text);
    bool m_active=false;
Q_SIGNALS:
    void setActive(const bool& active);
//signals:
    //void attention();
    //void confirm();
    //void deny();
    //void speak(const QString& text);
    //void askAI(const QString& question);
};
#endif