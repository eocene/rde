#include "Adaptor.h"
Adaptor::Adaptor(QObject* object):QDBusAbstractAdaptor(object)
{
    m_localeName=QLocale::system().name();

    m_voicePath="/data/rde/rai/piper-voices";

    m_attention << "yes?";
    m_attention << "what's up?";

    m_confirm << "yes";
    m_confirm << "yes sir";
    m_confirm << "yep";
    m_confirm << "yeah";
    m_confirm << "sure";
    m_confirm << "sure thing";
    m_confirm << "absolutely";
    m_confirm << "of course";
    m_confirm << "with pleasure";
    m_confirm << "alright";
    m_confirm << "okay";
    m_confirm << "fine";
    m_confirm << "acknowledge";
    m_confirm << "confirm";
    m_confirm << "done";
    m_confirm << "will do";
    m_confirm << "aye";
    m_confirm << "aye aye";
    m_confirm << "aye aye sir";

    m_deny << "no";
    m_deny << "no can do";
    m_deny << "sorry";
    m_deny << "can't do it";
    m_deny << "sorry";

    QString path=QStandardPaths::findExecutable("piper-tts");
    if(!path.isEmpty()){
        m_piper=new QProcess(this);
        m_piper->setProgram(path);
    }

    QAudioFormat format;
    format.setChannelConfig(QAudioFormat::ChannelConfigMono);
    format.setChannelCount(1);
    format.setSampleRate(22050);
    format.setSampleFormat(QAudioFormat::Int16);

    connect(m_piper,&QProcess::stateChanged,[=](QProcess::ProcessState state){
        switch(state){
        case QProcess::NotRunning:m_sink->stop();break;
        case QProcess::Starting:break;
        case QProcess::Running:break;
        }
    });
    connect(m_piper,&QProcess::errorOccurred,[=](QProcess::ProcessError error){
        switch(error){
        case QProcess::FailedToStart:qWarning() << "Piper Failed To Start";break;
        case QProcess::Crashed:qWarning() << "Piper Crashed Error";break;
        case QProcess::Timedout:qWarning() << "Piper Timeout Error";break;
        case QProcess::WriteError:qWarning() << "Piper Write Error";break;
        case QProcess::ReadError:qWarning() << "Piper Read Error";break;
        case QProcess::UnknownError:qWarning() << "Unknown Piper Error";break;
        }
    });

    m_sink=new QAudioSink(format,this);
    connect(m_sink,&QAudioSink::stateChanged,[=](QAudio::State state){
        switch(state){
        case QAudio::ActiveState:/*emit speaking(true)*/;break;
        case QAudio::SuspendedState:break;
        case QAudio::StoppedState:/*emit speaking(false)*/;break;
        case QAudio::IdleState:{m_sink->stop();break;}
        }
    });

    connect(this,&Adaptor::setVoice,[=](const QString& voice){
        if(!voice.isEmpty()){
            QFileInfo fi(m_voicePath+"/"+m_localeName+"-"+voice+".onnx");
            if(fi.isReadable()){
                m_voice=voice;
                if(m_piper->isOpen())
                    m_piper->close();

                QStringList a;
                a << "-q";
                a << "-m" << fi.absoluteFilePath();
                //a << "--length_scale" << QString::number(m_speed);
                a << "--output_raw";
                m_piper->setArguments(a);
                if(m_piper->open(QProcess::ReadWrite|QProcess::Truncate)){
                    // QSettings settings;
                    // settings.beginGroup("Piper");
                    // settings.setValue("voice",voice);
                }
            }
        }
    });
    connect(this,&Adaptor::setVoicePath,[=](const QString& voicePath){
        m_voicePath=voicePath;
        qDebug() << "Setting voicepath to" << m_voicePath;
    });
    connect(this,&Adaptor::setSpeed,[=](const double& speed){
        m_speed=speed;
        qDebug() << "Setting speed to" << m_speed;
    });
    emit setVoice("amy-medium");
}
QStringList Adaptor::voices()
{
    QStringList list;
    QDir dir(m_voicePath,m_localeName+"*.onnx");
    if(dir.isReadable()){
        foreach(QFileInfo info,dir.entryInfoList(QDir::Files,QDir::Name)){
            QString voice=info.baseName().remove(m_localeName+"-");
            list << voice;
        }
    }
    return list;
}
void Adaptor::speak(const QString& text)
{
    if(!text.isEmpty() && m_piper->isOpen()){
        switch(m_sink->state()){
        case QAudio::ActiveState:
            m_sink->stop();
            m_piper->skip(m_piper->bytesAvailable());
            m_sink->start(m_piper);
            m_piper->write(text.toUtf8()+"\n");
            break;
        case QAudio::SuspendedState:break;
        case QAudio::StoppedState:{
            m_sink->start(m_piper);
            m_piper->write(text.toUtf8()+"\n");
            break;
        }
        case QAudio::IdleState:break;
        }
    }
}