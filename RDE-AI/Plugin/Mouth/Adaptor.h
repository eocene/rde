#ifndef ADAPTOR_H
#define ADAPTOR_H
#include <QAudioSink>
#include <QDir>
#include <QFileInfo>
#include <QLocale>
#include <QDBusAbstractAdaptor>
#include <QProcess>
#include <QStandardPaths>
#include <QRandomGenerator>
class Adaptor:public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface","org.rde.ai.mouth")    
    Q_PROPERTY(QString voice READ voice WRITE setVoice)
    Q_PROPERTY(QString voicePath READ voicePath WRITE setVoicePath)
    Q_PROPERTY(QStringList voices READ voices CONSTANT)
    Q_PROPERTY(double speed READ speed WRITE setSpeed)
public:
    explicit Adaptor(QObject* object);
    QString voice()const{return m_voice;}
    QString voicePath(){return m_voicePath;}
    double speed()const{return m_speed;}
    QStringList voices();
public Q_SLOTS:
    Q_NOREPLY void speak(const QString& text);

    void attention()
    {
        speak(m_attention.at(QRandomGenerator::global()->bounded(m_attention.size())));
    }
    void confirm()
    {
        speak(m_confirm.at(QRandomGenerator::global()->bounded(m_confirm.size())));
    }
    void deny()
    {
        speak(m_deny.at(QRandomGenerator::global()->bounded(m_deny.size())));
    }
private:
    QAudioSink* m_sink=nullptr;
    QProcess* m_piper=nullptr;
    QString m_voice;
    QString m_voicePath;
    double m_speed;
    QString m_localeName;

    QStringList m_confirm;
    QStringList m_deny;
    QStringList m_attention;
Q_SIGNALS:
    void setSpeed(const double& speed);
    void setVoice(const QString& voice);
    void setVoicePath(const QString& path);
};
#endif