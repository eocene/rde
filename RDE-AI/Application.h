#ifndef APPLICATION_H
#define APPLICATION_H
#include <QDBusConnection>
#include <QCoreApplication>
#include "Plugin.h"
class Application:public QCoreApplication
{
    Q_OBJECT
public:
    explicit Application(int& argc,char** argv);
};
#endif