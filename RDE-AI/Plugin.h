#ifndef PLUGIN_H
#define PLUGIN_H
#include <QDBusAbstractAdaptor>
#include <QDBusConnection>
#include <QDir>
#include <QPluginLoader>
#include "PluginInterface.h"
class Plugin:public QPluginLoader
{
    Q_OBJECT
public:
    Plugin(const QString& file);
    QString iid()const{return m_iid;}
    QString name()const{return m_name;}
    static void loadPlugins();
    static QList<Plugin*> s_plugins;
private:
    QObject* m_instance=nullptr;
    PluginInterface* m_interface=nullptr;
    QDBusAbstractAdaptor* m_adaptor=nullptr;
    QString m_iid;
    QString m_name;
};
#endif