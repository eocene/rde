#define Service "org.rde.ai"
#define Path "/org/rde/ai"
#include <QDBusInterface>
#include "Application.h"
int main(int argc,char *argv[])
{
    if(QDBusConnection::systemBus().isConnected()){
        if(QDBusConnection::sessionBus().registerService(Service)){
            Application app(argc,argv);            
            return app.exec();
        }else return 2;
    }else return 1;
}