#ifndef PLUGININTERFACE_H
#define PLUGININTERFACE_H
#include <QtPlugin>
#include <QDBusAbstractAdaptor>
class PluginInterface
{
public:
    virtual QDBusAbstractAdaptor* adaptor()=0;
};
Q_DECLARE_INTERFACE(PluginInterface,"org.rde.ai.plugin")
#endif