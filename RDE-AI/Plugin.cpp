#include "Plugin.h"
Plugin::Plugin(const QString& file):QPluginLoader(file)
{
    QJsonObject meta=metaData();
    m_iid=meta.value("IID").toString();
    QJsonObject data=meta.value("MetaData").toObject();
    m_name=data.value("name").toString();
    if(load()){        
        m_instance=instance();
        if(m_instance){
            m_instance->setParent(this);
            m_interface=qobject_cast<PluginInterface*>(m_instance);
            m_adaptor=m_interface->adaptor();
            if(QDBusConnection::sessionBus().registerObject("/org/rde/ai/"+m_name,m_instance)){
            }
        }
    }
}
void Plugin::loadPlugins()
{
    QDir dir("/usr/lib/rde/ai","*.so");
    if(dir.isReadable()){
        foreach(QFileInfo info,dir.entryInfoList()){
            Plugin* plugin=new Plugin(info.absoluteFilePath());
            if(plugin->iid().startsWith("org.rde.ai."))
                Plugin::s_plugins.append(plugin);
            else
                plugin->deleteLater();
        }
    }
}
QList<Plugin*> Plugin::s_plugins;