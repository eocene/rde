#include "Application.h"
Application::Application(int& argc,char** argv):QCoreApplication(argc,argv)
{
    setOrganizationName("rde");
    setApplicationName("ai");
    Plugin::loadPlugins();
}