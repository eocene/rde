import QtQuick
import QtQuick.Controls
ComboBox{
    id:control
    valueRole:'display'
    textRole:'display'
    background:RBar{
        anchors.fill:parent
        radius:8
        opacity:popup.visible?0.5:1
    }
    contentItem:RText{
        anchors{fill:parent;margins:4}
        text:control.displayText
    }
    popup:Popup{
        id:popup
        clip:true
        padding:0
        width:control.width
        y:control.height
        //font{family:RShell.font;pointSize:RShell.fontSize}
        //        background:RBackground{}
        implicitHeight: contentItem.implicitHeight


        //contentItem:ListView{
        contentItem:RList{
            //anchors.fill:parent
            implicitHeight:contentHeight
            model:control.popup.visible?control.delegateModel:null
            //delegate:Rectangle{color:'black'}
        }
    }
}