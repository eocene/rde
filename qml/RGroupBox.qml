import QtQuick.Controls
GroupBox{
    property alias checkBox:checkBox
    label:CheckBox{
        id:checkBox
        text:title
        contentItem:RText{
            text:checkBox.text
            font:checkBox.font
            opacity:enabled?1.0:0.3
            leftPadding:checkBox.indicator.width+checkBox.spacing
        }
    }
}