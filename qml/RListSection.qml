import QtQuick
RBarRaised{
    anchors{left:parent.left;right:parent.right}
    RText{
        anchors.fill:parent
        verticalAlignment:Text.AlignVCenter
        horizontalAlignment:Text.AlignHCenter
    }
}