import QtQuick
Item{
    id:surface
    clip:true
    property Component component;
    property Item oldItem;
    property Item newItem;
    ParallelAnimation{
        id:fade
        NumberAnimation{target:oldItem;property:'opacity';from:1;to:0;duration:300}
        NumberAnimation{target:newItem;property:'opacity';from:0;to:1;duration:300}
        onFinished:{
            oldItem.destroy()
        }
    }
    onComponentChanged:{
        if(component.status===Component.Ready){
            if(newItem)
                oldItem=newItem;
            newItem=component.createObject(surface)
            fade.start()
        }
    }
}