import QtQuick
RBar{
    property alias box:box
    property alias label:label.text
    RText{
        id:label
        anchors{top:parent.top;bottom:parent.bottom;left:parent.left;right:parent.horizontalCenter;margins:4}
    }
    RComboBox{
        id:box
        anchors{top:parent.top;bottom:parent.bottom;left:parent.horizontalCenter;right:parent.right;margins:4}
    }
}