import QtQuick
RBar{
    property alias label:label.text
    property alias slider:slider
    RText{
        id:label
        anchors{top:parent.top;bottom:parent.bottom;left:parent.left;right:parent.horizontalCenter;margins:4}
    }
    RSlider{
        id:slider
        anchors{top:parent.top;bottom:parent.bottom;left:parent.horizontalCenter;right:parent.right;margins:4}
    }
}