import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import Qt.labs.qmlmodels
Item{
    id:root
    signal activated(int row)
    signal checked(int row,bool check)
    property alias table:view
    ColumnLayout{
        anchors.fill:parent
        spacing:0
        HorizontalHeaderView{
            id:header
            Layout.fillWidth:true
            syncView:view
            resizableColumns:false
            boundsMovement:Flickable.StopAtBounds
            delegate:RBarRaised{
                implicitWidth:100
                implicitHeight:40
                RText2{
                    anchors.fill:parent
                    verticalAlignment:Text.AlignVCenter
                    horizontalAlignment:Text.AlignHCenter                    
                    text:model.display
                }
            }
        }
        TableView{
            id:view
            Layout.fillWidth:true
            Layout.fillHeight:true
            clip:true
            focus:true
            boundsMovement:Flickable.StopAtBounds
            columnWidthProvider:function(column){return view.model?view.width/view.model.columnCount():0}
            rowHeightProvider:function(row){return 40}
            selectionBehavior:TableView.SelectRows
            selectionModel:ItemSelectionModel{}
            Keys.onReturnPressed:root.activated(currentRow)
            ScrollBar.vertical:ScrollBar{}
            delegate:DelegateChooser{
                role:'type'
                DelegateChoice{
                    roleValue:'IconText'
                    ItemDelegate{
                        padding:0
                        text:model.display
                        palette.text:'white'
                        icon{
                            source:decoration
                            width:32
                            height:32
                        }
                        background:RBar{}
                        //contentItem:RIconTextItem{}
                        required property bool current
                        current:row===view.currentRow
                        //onPressed:view.selectionModel.setCurrentIndex(view.model.index(row,0),ItemSelectionModel.current)
                        onPressed:view.model.currentRow=row
                        onDoubleClicked:root.activated(row)
                        RHighlight{
                            anchors{fill:parent;margins:1}
                            visible:current
                        }
                    }
                }
                DelegateChoice{
                    roleValue:'Text';
                    ItemDelegate{
                        id:textDelegate
                        padding:0
                        background:RBar{}
                        contentItem:RText{
                            text:model.display
                            anchors{fill:parent;margins:4}
                            verticalAlignment:Text.AlignVCenter
                            horizontalAlignment:model.textAlignment
                        }
                        required property bool current
                        current:row===view.currentRow
                        //onPressed:view.selectionModel.setCurrentIndex(view.model.index(row,0),ItemSelectionModel.current)
                        onPressed:view.model.currentRow=row
                        onDoubleClicked:root.activated(row)
                        RHighlight{
                            anchors{fill:parent;margins:1}
                            visible:current
                        }
                    }
                }
                DelegateChoice{
                    roleValue:'CheckBox'
                    CheckDelegate{
                        background:RBar{}
                        checkState:model.check
                        //checkState:model.checkState
                        onToggled:root.checked(row,checked)
                        required property bool current
                        //current:row===view.currentRow
                        //onPressed:view.selectionModel.setCurrentIndex(view.model.index(row,0),ItemSelectionModel.current)
                        onPressed:view.model.currentRow=row
                        RHighlight{
                            anchors{fill:parent;margins:1}
                            visible:current
                        }
                    }
                }
            }
        }
    }
    RBarShadow{
        parent:view;
        anchors{left:parent.left;right:parent.right;top:view.top}
    }
}