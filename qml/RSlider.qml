import QtQuick
import QtQuick.Controls
Slider{
    id:slider
    padding:1
    background:Rectangle{
        radius:4
        border{width:1;color:Qt.rgba(0,0,0,0.5)}
        color:Qt.rgba(0,0,0,0.3)
    }
//    handle:Rectangle{
//        border{width:1;color:Qt.rgba(0,0,0,0.3)}
//        gradient:Gradient{
//            GradientStop{position:0;color:Qt.rgba(0.7,0.7,0.7,1)}
//            GradientStop{position:0.5;color:Qt.rgba(0.4,0.4,0.4,1)}
//            GradientStop{position:1;color:Qt.rgba(0.3,0.3,0.3,1)}
//        }
//        radius:4
//        width:parent.width
//        height:width
//        y:slider.visualPosition*slider.availableHeight
//    }
}