import QtQuick.Controls
ItemDelegate{
    width:parent.width
    height:40
    padding:0
    background:RBar{}    
    text:model.display
    palette.text:'white'
    icon{
        width:32
        height:32
        name:decoration
    }
}