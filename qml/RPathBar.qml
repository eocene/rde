import QtQuick
RBar{
    property alias label:label.text
    property alias input:input
    RText{
        id:label
        anchors{top:parent.top;bottom:parent.bottom;left:parent.left;right:parent.horizontalCenter;margins:4}
        verticalAlignment:Text.AlignVCenter
    }
    Rectangle{
        z:0
        anchors{left:parent.horizontalCenter;right:parent.right;top:parent.top;bottom:parent.bottom;margins:4}
        border{width:1;color:Qt.rgba(0,0,0,0.5)}
        radius:8
        gradient:Gradient{
            GradientStop{position:0;color:Qt.rgba(0.3,0.3,0.3,0.8)}
            GradientStop{position:1;color:Qt.rgba(0.7,0.7,0.7,0.8)}
        }
        RTextInput{
            id:input
            anchors{fill:parent;margins:4}            
        }
    }
}