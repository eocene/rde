import QtQuick
GridView{
    clip:true
    focus:true
    boundsBehavior:Flickable.StopAtBounds
    keyNavigationEnabled:true
    highlight:RHighlight{}
    highlightFollowsCurrentItem:true
    highlightMoveDuration:0
    populate:Transition{NumberAnimation{property:'opacity';from:0;to:1;duration:200}}
    remove:Transition{NumberAnimation{property:'opacity';from:1;to:1;duration:200}}
    cellWidth:128
    cellHeight:128
}