import QtQuick
import QtQuick.Effects
Item{
    property alias source:image.source
    property alias image:image    
    width:40
    height:width
    Image{
        id:image
        anchors.centerIn:parent
        fillMode:Image.PreserveAspectFit
        smooth:true
        antialiasing:true
        asynchronous:true
        mipmap:true
        clip:false
        cache:false        
        width:32
        height:width
        sourceSize{height:height;width:width}
        layer{
            enabled:enabled
            smooth:true
            effect:MultiEffect{
                shadowEnabled:true
                shadowHorizontalOffset:3
                shadowVerticalOffset:3
            }
        }
    }
    NumberAnimation{
        id:fadeOut
        target:image        
        duration:300
        property:'opacity'
        to:0
        onFinished:{
            var src=image.source;
            image.source=''
            image.source=src;
            fadeIn.start()
        }
    }
    NumberAnimation{
        id:fadeIn
        target:image        
        duration:300
        property:'opacity'
        to:1
    }
    // Connections{
    //     target:RShell
    //     function onIconThemeChanged(){fadeOut.start()}
    // }
    Behavior on opacity{NumberAnimation{duration:300}}
}