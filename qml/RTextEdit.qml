import QtQuick
//import QtQuick.Effects
Flickable{
    property alias text:textEdit.text
    property alias readOnly:textEdit.readOnly
    contentHeight:textEdit.implicitHeight
    boundsBehavior:Flickable.StopAtBounds
    clip:true
    TextEdit{
        id:textEdit
        anchors.fill:parent
        clip:true
        color:'white'
        textFormat:Text.PlainText
        wrapMode:TextEdit.WordWrap
    }
//    layer.enabled:true
//    layer.smooth:true
//    layer.effect:MultiEffect{
//        shadowEnabled:true
//        shadowBlur:0.5
//        shadowColor:'cyan'
//        shadowHorizontalOffset:3
//        shadowVerticalOffset:3
//    }
}