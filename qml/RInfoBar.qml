import QtQuick
Item{
    height:40
    property alias key:key
    property alias value:value
    RBarRaised{
        id:keyBar
        anchors{top:parent.top;bottom:parent.bottom;left:parent.left;right:parent.horizontalCenter}
        RText2{
            id:key
            anchors{fill:parent;margins:4}
            verticalAlignment:Text.AlignVCenter
            horizontalAlignment:Text.AlignRight
        }
    }
    RBar{
        id:valueBar
        anchors{top:parent.top;bottom:parent.bottom;left:keyBar.right;right:parent.right}
        RText{
            id:value
            anchors{fill:parent;margins:4}
            verticalAlignment:Text.AlignVCenter
            horizontalAlignment:Text.AlignLeft            
        }
    }
}