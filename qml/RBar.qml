import QtQuick
Rectangle{
    height:40
    border{width:1;color:Qt.rgba(0,0,0,0.5)}
    gradient:Gradient{
        GradientStop{position:0;color:Qt.rgba(1,1,1,0.2)}
        GradientStop{position:0.5;color:Qt.rgba(0,0,0,0)}
        GradientStop{position:1;color:Qt.rgba(0,0,0,0.2)}
    }
}