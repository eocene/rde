import QtQuick
import QtQuick.Controls
TextField{
    color:Qt.rgba(1,1,1,1)
    renderType:TextInput.CurveRendering
    background:Rectangle{
        border{width:1;color:Qt.rgba(0,0,0,0.5)}
        radius:8
        gradient:Gradient{
            GradientStop{position:0;color:Qt.rgba(0.3,0.3,0.3,0.8)}
            GradientStop{position:1;color:Qt.rgba(0.7,0.7,0.7,0.8)}
        }
    }
}