import QtQuick
import QtQuick.Controls
import QtQuick.Effects
Dial{
    id:dial
    anchors.centerIn:parent
    width:40
    height:width
    snapMode:Dial.SnapAlways
    layer.enabled:enabled
    layer.smooth:true
    layer.effect:MultiEffect{
        shadowEnabled:true
        shadowHorizontalOffset:3
        shadowVerticalOffset:3
    }
    MouseArea{
        anchors.fill:parent
        acceptedButtons:Qt.RightButton
        onClicked:(mouse)=>{rightClick()}
    }
    signal rightClick()
}