import QtQuick
import QtQuick.Controls
ListView{
    id:view
    clip:true
    focus:true
    boundsBehavior:Flickable.StopAtBounds
    highlight:RHighlight{}
    highlightFollowsCurrentItem:true
    highlightMoveDuration:0
    snapMode:ListView.SnapToItem
    delegate:RListDelegate{z:0}
    headerPositioning:ListView.OverlayHeader
    //    interactive:false
    //    MouseArea{
    //        anchors.fill:parent
    //        onWheel:(event)=>{
    //                    if((event.angleDelta.y>0 && !view.atYBeginning) || (event.angleDelta.y<0 && !view.atYEnd))
    //                    view.contentY=view.contentY-event.angleDelta.y
    //                }
    //    }
    ScrollBar.vertical:ScrollBar{}
    // section.delegate:RBarRaised{
    //     anchors{left:parent.left;right:parent.right}
    //     RText{
    //         anchors.fill:parent
    //         verticalAlignment:Text.AlignVCenter
    //         horizontalAlignment:Text.AlignHCenter
    //     }
    // }
    section.criteria:ViewSection.FullString
}