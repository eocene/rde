import QtQuick
Item{
    RIcon{
        id:icon
        anchors{left:parent.left;top:parent.top;bottom:parent.bottom}
        source:parent.parent.icon.source
    }
    RText{
        anchors{left:icon.right;right:parent.right;top:parent.top;bottom:parent.bottom;margins:4}
        text:parent.parent.text
    }
}