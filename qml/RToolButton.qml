import QtQuick
import QtQuick.Controls
import QtQuick.Effects
ToolButton{
    id:button
    height:40
    width:height
    background:Rectangle{
        anchors{fill:parent;margins:4}
        color:'blue'
        visible:button.checked
    }
    Behavior on scale{NumberAnimation{duration:50}}
    scale:down?0.9:1
    display:AbstractButton.IconOnly
    icon{
        width:32
        height:32
    }
    opacity:enabled?1:0.5
    Behavior on opacity{NumberAnimation{duration:300}}
    layer{
        enabled:enabled
        smooth:true
        effect:MultiEffect{
            shadowEnabled:true
            shadowHorizontalOffset:3
            shadowVerticalOffset:3
        }
    }
}