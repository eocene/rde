import QtQuick
Rectangle{    
    height:40
    border{width:1;color:Qt.rgba(0,0,0,0.5)}
    gradient:Gradient{
        GradientStop{position:0;color:Qt.rgba(0.7,0.7,0.7,0.8)}
        GradientStop{position:1;color:Qt.rgba(0.3,0.3,0.3,0.8)}
    }
}