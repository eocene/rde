import QtQuick
import QtQuick.Controls
Item{
    readonly property real radius:height/5
    antialiasing:true
    clip:true
    Canvas{
        anchors.fill:parent
        onPaint:{
            var ctx=getContext('2d');
            ctx.reset();
            ctx.beginPath();
            ctx.lineWidth=height*0.1;
            ctx.roundedRect(ctx.lineWidth/2,ctx.lineWidth/2,width-ctx.lineWidth,height-ctx.lineWidth,parent.radius,parent.radius);
            ctx.strokeStyle='grey';
            ctx.stroke();
            var gradient=ctx.createLinearGradient(0,0,0,height);
            gradient.addColorStop(0,'#cc4e4e4e');
            gradient.addColorStop(1,'#cc252525');
            ctx.fillStyle=gradient;
            ctx.fill();
        }
    }
    Canvas{
        anchors.fill:parent
        z:1
        onPaint:{
            var ctx=getContext('2d');
            ctx.reset();
            ctx.beginPath();
            ctx.lineWidth=height*0.1;
            ctx.roundedRect(ctx.lineWidth/2,ctx.lineWidth/2,width-ctx.lineWidth,height-ctx.lineWidth,parent.radius,parent.radius);
            ctx.moveTo(0,height*0.4);
            ctx.bezierCurveTo(width*0.25,height*0.6,width*0.75,height*0.6,width,height*0.4);
            ctx.lineTo(width,height);
            ctx.lineTo(0,height);
            ctx.lineTo(0,height*0.4);
            ctx.clip();
            ctx.beginPath();
            ctx.roundedRect(ctx.lineWidth/2,ctx.lineWidth/2,width-ctx.lineWidth,height-ctx.lineWidth,parent.radius,parent.radius);
            var gradient=ctx.createLinearGradient(0,0,0,height);
            gradient.addColorStop(0,'#88ffffff');
            gradient.addColorStop(0.6,'#00ffffff');
            ctx.fillStyle=gradient;
            ctx.fill();
        }
    }
}