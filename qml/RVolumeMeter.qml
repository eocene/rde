/*
import QtQuick


//import QtQuick.Controls
/*
Control{
    id:control

    background:Rectangle{
        radius:4
        border{width:1;color:Qt.rgba(0,0,0,0.5)}
        color:Qt.rgba(0,0,0,0.3)
    }
    contentItem:Rectangle{
        id:content
        implicitHeight:40
        clip:true
        Rectangle{
//            anchors.fill:parent
            width:parent.width
            height:control.height/2
            gradient:Gradient{
                orientation:Gradient.Vertical
                GradientStop{position:0;color:'red'}
                GradientStop{position:0.3;color:'yellow'}
                GradientStop{position:1;color:'green'}
            }
        }
    }

}

Rectangle{
    id:root
    radius:4
    border{width:1;color:Qt.rgba(0,0,0,0.5)}
    color:Qt.rgba(0,0,0,0.3)
    property real level
//    property real max

    clip:true


    Rectangle{
        id:bar

        anchors{left:parent.left;right:parent.right;bottom:parent.bottom}
        height:100

        color:'green'
    }

    onLevelChanged:{

//        console.log(root.height/level)
//        var gl=level*100

//        console.log(1.0/gl)

//        console.log("LEVEL:"+level*100)
//        bar.height=level*100


    }

    Component.onCompleted:{
//        max=root.height*1.0

//        console.log("MAX:"+max)
    }


//    Item{
//        anchors{left:parent.left;right:parent.right;bottom:parent.bottom}
//        height:50

//        clip:true
//        Rectangle{
//            anchors{left:parent.left;right:parent.right;margins:1}
//            height:root.height
//            radius:4
////            anchors.fill:parent


////            height:control.visualPosition*parent.height
////            width:parent.height
//            gradient:Gradient{
//                orientation:Gradient.Vertical
//                GradientStop{position:0;color:'red'}
//                GradientStop{position:0.3;color:'yellow'}
//                GradientStop{position:1;color:'green'}
//            }
//        }
//    }

//    onLevelChanged:console.log("Level:"+level)
}





//Rectangle { // background
//    id: root

//// public
//    property double maximum: 10
//    property double value:   0
//    property double minimum: 0

//// private
//    width: 500;  height: 100 // default size

//    border.width: 0.05 * root.height
//    radius: 0.5 * height

//    Rectangle { // foreground
//        visible: value > minimum
//        x: 0.1 * root.height;  y: 0.1 * root.height
//        width: Math.max(height,
//               Math.min((value - minimum) / (maximum - minimum) * (parent.width - 0.2 * root.height),
//                        parent.width - 0.2 * root.height)) // clip
//        height: 0.8 * root.height
//        color: 'black'
//        radius: parent.radius
//    }
//}
*/
import QtQuick
import QtQuick.Controls
import Qt5Compat.GraphicalEffects
ProgressBar{
    id:control
    padding:2
    rotation:180
    background:Rectangle{
        radius:4
        border{width:1;color:Qt.rgba(0,0,0,0.5)}
        color:Qt.rgba(0,0,0,0.3)
    }
    //    contentItem:Rectangle{
    //        height:control.visualPosition*parent.height
    //        width:parent.height
    //        gradient:Gradient{
    //            //            start:Qt.point(0,0)
    //            //            end:Qt.point(control.height,0)
    //            GradientStop{position:0;color:'green'}
    //            GradientStop{position:0.7;color:'yellow'}
    //            GradientStop{position:1;color:'red'}
    //        }
    //    }



    //    contentItem:Rectangle{
    //        height:control.visualPosition*parent.height
    //        width:parent.height
    //        gradient:Gradient{
    //            orientation:Gradient.Vertical
    //            GradientStop{position:0;color:'green'}
    //            GradientStop{position:0.7;color:'yellow'}
    //            GradientStop{position:1;color:'red'}
    //        }
    //    }

    contentItem:LinearGradient{
        height:control.visualPosition*parent.height
        width:parent.height
        start:Qt.point(0,0)
        end:Qt.point(0,control.height)
        gradient:Gradient{
            GradientStop{position:0;color:'green'}
            GradientStop{position:0.7;color:'yellow'}
            GradientStop{position:1;color:'red'}
        }
    }
}