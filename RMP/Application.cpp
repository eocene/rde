#include "Application.h"
Application::Application(int &argc,char **argv):QGuiApplication(argc,argv)
{
    setOrganizationName("rde");
    setApplicationName("rmp");
    setApplicationDisplayName("RMP");


    m_player=new QMediaPlayer(this);
    m_player->setAudioOutput(new QAudioOutput);


    m_adaptor=new Adaptor(m_player);

    if(QDBusConnection::sessionBus().registerObject("/org/mpris/MediaPlayer2",m_player)){        
    }

    connect(m_player,&QMediaPlayer::hasVideoChanged,[=](bool available){
        if(available){
            m_engine=new QQmlApplicationEngine(this);
            m_engine->load("qrc:/VideoWindow.qml");
            m_player->setVideoOutput(m_engine->rootObjects().first()->children().at(1));
        }else{
            m_player->setVideoOutput(nullptr);
            m_engine->deleteLater();
        }
    });

    QStringList args=arguments();
    args.removeFirst();
    if(!args.isEmpty())
        open(args.first());
}
void Application::open(const QString &url)
{
    QUrl u(url,QUrl::TolerantMode);
    if(u.isValid()){
        emit m_player->setSource(u);
    }
    else
        qWarning() << "RMP: Invalid URL" << url;
}
// void Application::quit()
// {
//     qGuiApp->quit();
// }
// Application::~Application()
// {
// }