#ifndef APPLICATION_H
#define APPLICATION_H
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "Adaptor.h"
//#include <QDBusConnection>
class Application:public QGuiApplication
{
    Q_OBJECT
public:
    explicit Application(int &argc,char **argv);
    //~Application();
public slots:
    Q_SCRIPTABLE void open(const QString& url);
private:
    QQmlApplicationEngine* m_engine=nullptr;
    QMediaPlayer* m_player=nullptr;
    Adaptor* m_adaptor=nullptr;
};
#endif