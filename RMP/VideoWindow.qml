import QtQuick
import QtMultimedia
Window{
    id:window
    visible:true
    color:'black'

    //VideoMenu{id:menu}
    VideoOutput{
        anchors.fill:parent
        MouseArea{
            anchors.fill:parent
            acceptedButtons:Qt.LeftButton|Qt.RightButton
            onClicked:(event)=>{
                          if(event.button===Qt.RightButton){
                              menu.popup()
                          }
                      }
            onDoubleClicked:{
                if(window.visibility===Window.FullScreen)
                    window.visibility=Window.Windowed
                else
                    window.visibility=Window.FullScreen
            }
        }
    }

}