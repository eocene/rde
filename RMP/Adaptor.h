#ifndef ADAPTOR_H
#define ADAPTOR_H
#include <QAudioOutput>
#include <QDBusAbstractAdaptor>
#include <QDBusObjectPath>
#include <QDBusConnection>
#include <QDBusMessage>
#include <QMediaPlayer>
#include <QMediaMetaData>
#include <QMetaClassInfo>
#include <QDebug>
class Adaptor:public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface","org.mpris.MediaPlayer2.Player")
    Q_PROPERTY(QString PlaybackStatus READ PlaybackStatus CONSTANT)
    Q_PROPERTY(QString LoopStatus READ LoopStatus WRITE setLoopStatus)
    Q_PROPERTY(bool Shuffle READ Shuffle WRITE setShuffle)
    Q_PROPERTY(bool CanPause READ CanPause CONSTANT)
    Q_PROPERTY(bool CanPlay READ CanPlay CONSTANT)
    Q_PROPERTY(bool CanGoNext READ CanGoNext CONSTANT)
    Q_PROPERTY(bool CanGoPrevious READ CanGoPrevious CONSTANT)
    Q_PROPERTY(bool CanControl READ CanControl CONSTANT)
    Q_PROPERTY(bool CanSeek READ CanSeek)
    Q_PROPERTY(QVariantMap Metadata READ Metadata CONSTANT)
    Q_PROPERTY(double Volume READ Volume WRITE setVolume CONSTANT)
    Q_PROPERTY(double Rate READ Rate WRITE setRate CONSTANT)
    Q_PROPERTY(double MinimumRate READ MinimumRate CONSTANT)
    Q_PROPERTY(double MaximumRate READ MaximumRate CONSTANT)
    Q_PROPERTY(qlonglong Position READ Position)
public:
    Adaptor(QMediaPlayer* player):QDBusAbstractAdaptor(player){
        connect(player,&QMediaPlayer::durationChanged,[=](qint64 duration){
            metaData.insert("mpris:length",duration);
            m_updatedProperties.insert("Metadata",metaData);
            signalPropertyChange("Metadata");
        });
        connect(player,&QMediaPlayer::seekableChanged,[=](const bool& seekable){
            m_updatedProperties.insert("CanSeek",seekable);
            signalPropertyChange("CanSeek");
        });
        connect(player,&QMediaPlayer::playbackStateChanged,[=](QMediaPlayer::PlaybackState state){
            m_updatedProperties.insert("PlaybackStatus",PlaybackStatus());
            signalPropertyChange("PlaybackStatus");
        });
        connect(player,&QMediaPlayer::mediaStatusChanged,[=](QMediaPlayer::MediaStatus status){
            switch(status){
            case QMediaPlayer::NoMedia:{
                m_updatedProperties.insert("CanPlay",false);
                signalPropertyChange("CanPlay");
                break;
            }
            case QMediaPlayer::LoadingMedia:{
                m_updatedProperties.insert("CanPlay",false);
                signalPropertyChange("CanPlay");
                break;
            }
            case QMediaPlayer::LoadedMedia:{
                m_updatedProperties.insert("CanPlay",true);
                signalPropertyChange("CanPlay");
                break;
            }
            case QMediaPlayer::StalledMedia:{
                break;
            }
            case QMediaPlayer::BufferingMedia:{
                break;
            }
            case QMediaPlayer::BufferedMedia:{
                break;
            }
            case QMediaPlayer::EndOfMedia:{
                break;
            }
            case QMediaPlayer::InvalidMedia:{
                m_updatedProperties.insert("CanPlay",false);
                signalPropertyChange("CanPlay");
                break;
            }
            }
        });
        connect(player,&QMediaPlayer::playingChanged,[=](const bool& playing){
            m_updatedProperties.insert("CanPause",playing);
            signalPropertyChange("CanPause");
        });
        connect(player,&QMediaPlayer::metaDataChanged,[=](){
            //metaData.clear();
            QMediaMetaData data=m_player->metaData();
            QList<QMediaMetaData::Key> keys=data.keys();
            if(keys.contains(QMediaMetaData::ContributingArtist))
                metaData.insert("xesam:artist",data.value(QMediaMetaData::ContributingArtist));
            if(keys.contains(QMediaMetaData::Title))
                metaData.insert("xesam:title",data.value(QMediaMetaData::Title));
            if(keys.contains(QMediaMetaData::AlbumTitle))
                metaData.insert("xesam:album",data.value(QMediaMetaData::AlbumTitle));

            m_updatedProperties.insert("Metadata",metaData);
            signalPropertyChange("Metadata");
        });
        connect(player,&QMediaPlayer::errorOccurred,[=](QMediaPlayer::Error error,const QString& errorString){
            switch(error){
            case QMediaPlayer::NoError:break;
            case QMediaPlayer::ResourceError:break;
            case QMediaPlayer::FormatError:break;
            case QMediaPlayer::NetworkError:break;
            case QMediaPlayer::AccessDeniedError:break;
            }
        });
        connect(player,&QMediaPlayer::sourceChanged,[=](const QUrl& media){
            player->play();
            metaData.insert("xesam:url",media.toString());
            m_updatedProperties.insert("Metadata",metaData);
            signalPropertyChange("Metadata");

        });


        /*
        connect(player,&QMediaPlayer::activeTracksChanged,[=](){
        });
        connect(player,&QMediaPlayer::tracksChanged,[=](){
            m_audioTracks.clear();
            m_videoTracks.clear();
            m_subtitleTracks.clear();
            foreach(QMediaMetaData meta,m_player->audioTracks()){
                if(meta.keys().contains(QMediaMetaData::Language))
                    m_audioTracks << meta.stringValue(QMediaMetaData::Language);
            }
            foreach(QMediaMetaData meta,m_player->videoTracks()){
                if(meta.keys().contains(QMediaMetaData::Language))
                    m_videoTracks << meta.stringValue(QMediaMetaData::Language);
            }
            foreach(QMediaMetaData meta,m_player->subtitleTracks()){
                if(meta.keys().contains(QMediaMetaData::Language))
                    m_subtitleTracks << meta.stringValue(QMediaMetaData::Language);
            }
            emit audioTracksChanged();
            emit videoTracksChanged();
            emit subtitleTracksChanged();
        });
*/

        m_player=player;
    }
    bool CanGoNext()const{return false;}
    bool CanGoPrevious()const{return false;}
    bool CanPause()const{return m_player->isPlaying();}
    bool CanPlay()const{return true;}
    bool CanControl()const{return true;}
    bool CanSeek()const{return m_player->isSeekable();}
    QVariantMap Metadata()const{return metaData;}
    QString PlaybackStatus()const
    {
        switch(m_player->playbackState()){
        case QMediaPlayer::PlayingState:return "Playing";
        case QMediaPlayer::PausedState:return "Paused";
        case QMediaPlayer::StoppedState:return "Stopped";
        }
    }
    QString LoopStatus()const
    {
        return m_loopStatus;
    }
    void setLoopStatus(const QString& loopStatus)
    {
        // if(loopStatus=="None");
        // else if(loopStatus=="Playlist");
        // else if(loopStatus=="Track");
        m_loopStatus=loopStatus;
        m_updatedProperties.insert("LoopStatus",m_loopStatus);
        signalPropertyChange("LoopStatus");
    }
    bool Shuffle()const
    {
        return m_shuffle;
    }
    void setShuffle(bool shuffle)
    {
        m_shuffle=shuffle;
        m_updatedProperties.insert("Shuffle",m_shuffle);
        signalPropertyChange("Shuffle");
    }
    double Volume()const{return static_cast<double>(m_player->audioOutput()->volume());}
    void setVolume(const double& volume)
    {

        qDebug() << "Setting vol to" << volume;


        m_player->audioOutput()->setVolume(volume);
        m_updatedProperties.insert("Volume",volume);
        signalPropertyChange("Volume");
    }
    double Rate()const{return m_player->playbackRate();}
    void setRate(const double& rate)
    {
        m_player->setPlaybackRate(rate);
    }

    double MinimumRate()const{return 0.0;}
    double MaximumRate()const{return 2.0;}
    qlonglong Position() const{return m_player->position();}
public Q_SLOTS:
    void Next()const{}
    void Previous()const{}
    void Play()const{m_player->play();}
    void Pause()const{m_player->pause();}
    void PlayPause()const{if(m_player->isPlaying())m_player->pause();else m_player->play();}
    void Stop()const{m_player->stop();}
    void Seek(qlonglong offset)const{
        //m_player->setPosition(offset);
        m_player->setPosition(m_player->position()+offset);
        //emit Seeked(m_player->position());
    }
    void SetPosition(const QDBusObjectPath& TrackId,qlonglong Position)const
    {
        qDebug() << "SET POS TO" << Position;
    }
    void OpenUri(const QString& uri)const
    {
        QUrl u(uri,QUrl::TolerantMode);
        if(u.isValid()){
            emit m_player->setSource(u);
        }
        else
            qWarning() << "RMP: Invalid URI" << uri;
    }
private:
    QMediaPlayer* m_player=nullptr;
    QVariantMap metaData;
    QStringList m_invalidatedProperties;
    QVariantMap m_updatedProperties;
    QString m_loopStatus;
    bool m_shuffle;
    void signalPropertyChange(const QString& property)
    {
        QMetaObject::invokeMethod( this,"_m_emitPropertiesChanged",Qt::QueuedConnection);
    }
    void signalPropertyChange(const QString& property,const QVariant& value)
    {
        QMetaObject::invokeMethod(this,"_m_emitPropertiesChanged",Qt::QueuedConnection);
        // if ( m_updatedProperties.isEmpty() && m_invalidatedProperties.isEmpty() ) {
        //     QMetaObject::invokeMethod( this, "_m_emitPropertiesChanged", Qt::QueuedConnection );
        //     debug() << "MPRIS2: Queueing up a PropertiesChanged signal";
        // }

        // m_updatedProperties[property] = value;
    }
private Q_SLOTS:
    void _m_emitPropertiesChanged()
    {
        if(m_updatedProperties.isEmpty() && m_invalidatedProperties.isEmpty()){
            return;
        }

        int index=metaObject()->indexOfClassInfo("D-Bus Interface");
        QDBusMessage signal=QDBusMessage::createSignal("/org/mpris/MediaPlayer2","org.freedesktop.DBus.Properties","PropertiesChanged");
        signal << QLatin1String(metaObject()->classInfo(index).value());
        signal << m_updatedProperties;
        signal << m_invalidatedProperties;

        if(QDBusConnection::sessionBus().send(signal)){
        }

        m_updatedProperties.clear();
        m_invalidatedProperties.clear();
    }
Q_SIGNALS:
    void Seeked(qlonglong Position) const;
};
#endif