#define Service "org.mpris.MediaPlayer2.rmp"
#define Path "/org/mpris/MediaPlayer2"
#include "Application.h"
#include <QDBusInterface>
int main(int argc,char *argv[])
{
    if(QDBusConnection::systemBus().isConnected()){
        if(QDBusConnection::sessionBus().registerService(Service)){
            Application app=Application(argc,argv);
            return app.exec();
        }else{
            QDBusInterface i(Service,Path,"",QDBusConnection::sessionBus());
            if(i.isValid()){
                QString path;
                for(int i=1;i<argc;i++)
                    path.append(argv[i]);
                i.call("OpenUri",path);
            }
            return 2;

        }return 1;
    }
}