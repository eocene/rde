import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.rde.ui
import org.rde.rweb
Pane{
    padding:0
    background:Item{}
    contentItem:RList{
        id:view
        anchors.fill:parent
        header:RBarRaised{
            width:view.width
            RText2{
                anchors{fill:parent;margins:4}
                text:'History'
                verticalAlignment:Text.AlignVCenter
                horizontalAlignment:Text.AlignHCenter
            }
        }
        model:HistoryModel{}
    }
    //look up QWebEngineHistory
}