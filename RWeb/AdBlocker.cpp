#include "AdBlocker.h"
AdBlocker::AdBlocker(QObject* parent):QWebEngineUrlRequestInterceptor(parent)
{
    // m_network=new QNetworkAccessManager(this);
    // QNetworkReply* reply=m_network->get(QNetworkRequest(QUrl("https://easylist.to/easylist/easylist.txt")));
    // connect(reply,&QNetworkReply::finished,[=](){
    //     if(!reply->error())
    //         m_data=reply->readAll();
    //     else
    //         qWarning() << "EasyList Error" << reply->errorString();
    //     reply->deleteLater();
    //     m_network->deleteLater();
    //     m_network=nullptr;
    // });
}
void AdBlocker::interceptRequest(QWebEngineUrlRequestInfo& info)
{
    QString u=info.requestUrl().toString();
    if(
        u.contains("doubleclick.net")
        ||
        u.contains("https://www.youtube.com/ads/")
        ||
        u.contains("https://www.linkedin.com/ads/")
        ||
        u.contains("https://adfox.yandex.ru/promo")
        ||
        u.contains("googlesyndication.com/pagead/")
    )
    {
        info.block(true);
    }
}