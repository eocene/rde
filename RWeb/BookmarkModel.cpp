#include "BookmarkModel.h"
BookmarkModel::BookmarkModel()
{
    m_file=QFileInfo(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation)+"/bookmarks");
    if(m_file.isReadable()){
        m_watcher=new QFutureWatcher<QList<QStringList>>(this);
        connect(m_watcher,&QFutureWatcherBase::finished,[=](){
            beginResetModel();
            m_bookmarks=m_watcher->result();
            endResetModel();
            m_watcher->deleteLater();
        });
        m_watcher->setFuture(QtConcurrent::run(&BookmarkModel::readFile,this));
    }
}
extern QList<QStringList> BookmarkModel::readFile()
{
    QList<QStringList> l;
    QFile data(m_file.absoluteFilePath());
    if(data.open(QIODevice::ReadOnly)){
        QString s=data.readAll().trimmed();
        foreach(QString line,s.split("\n"))
            l << line.split(",");
        data.close();
    }
    return l;
}
void BookmarkModel::addBookmark(const QString& title,const QString& url,const QString& icon)
{
    // bool check=false;
    // foreach(QStringList l,m_bookmarks){
    //     if(url==l.at(1)){
    //         break;
    //     }
    // }

    //if(check){
    int size=m_bookmarks.size();
    QModelIndex i;
    beginInsertRows(i,size,size);
    QStringList l;
    l << title << url << icon;
    m_bookmarks.append(l);
    endInsertRows();
    //}
}
QVariant BookmarkModel::data(const QModelIndex &index,int role) const
{    
    switch(role){
    case Qt::DisplayRole:return QVariant(m_bookmarks.at(index.row()).first());
        //case Qt::DecorationRole:return QVariant("image://FavIcon/"+m_bookmarks.at(index.row()).last());
    case Qt::DecorationRole:return QVariant(m_bookmarks.at(index.row()).last());
    case Qt::UserRole:return QVariant(m_bookmarks.at(index.row()).at(1));
    }
}
BookmarkModel::~BookmarkModel()
{
    QFile data(m_file.absoluteFilePath());
    if(data.open(QIODevice::WriteOnly)){
        QTextStream out(&data);
        foreach(QStringList l,m_bookmarks)
            out << l.join(",")+"\n";
        data.close();
    }
}