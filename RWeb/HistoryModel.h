#ifndef HISTORYMODEL_H
#define HISTORYMODEL_H
#include <QQmlEngine>
#include <QAbstractListModel>
class HistoryModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit HistoryModel();
private:
    int rowCount(const QModelIndex &parent)const{Q_UNUSED(parent);return 4;}
    QVariant data(const QModelIndex &index,int role) const override;
    QHash<int,QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QAbstractListModel::roleNames();
        h[Qt::UserRole]="URL";
        return h;
    }
};
#endif