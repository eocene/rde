#include "DownloadModel.h"
DownloadModel::DownloadModel()
{
    m_file=QFileInfo(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation)+"/downloads");
    if(m_file.isReadable()){
        m_watcher=new QFutureWatcher<QStringList>(this);
        connect(m_watcher,&QFutureWatcherBase::finished,[=](){
            beginResetModel();
            m_downloads=m_watcher->result();
            endResetModel();
            m_watcher->deleteLater();
        });
        m_watcher->setFuture(QtConcurrent::run(&DownloadModel::readFile,this));
    }
}
extern QStringList DownloadModel::readFile()
{
    QStringList l;
    QFile data(m_file.absoluteFilePath());
    if (data.open(QIODevice::ReadOnly)){
        QString s=data.readAll().trimmed();
        l=s.split("\n");
        data.close();
    }
    return l;
}
void DownloadModel::addDownload(const QString& file)
{
    QModelIndex i;
    beginInsertRows(i,0,0);
    m_downloads.prepend(file);
    endInsertRows();

    QByteArray buffer;
    QFile f(m_file.absoluteFilePath());
    if(f.exists()){
        if(f.open(QFile::ReadOnly|QFile::Text)){
            buffer=f.readAll();
            f.close();
        }
    }
    if(f.open(QFile::WriteOnly|QFile::Text)){
        QTextStream out(&f);
        out << file << "\n";
        if(!buffer.isEmpty())
            out << buffer;
        f.close();
    }
}
void DownloadModel::clearDownloads()
{
    QFile file(m_file.absoluteFilePath());
    if(file.remove()){
        beginResetModel();
        m_downloads.clear();
        endResetModel();
    }
    else
        qWarning() << "Failed to delete history file!";
}
QVariant DownloadModel::data(const QModelIndex &index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return QVariant(m_downloads.at(index.row()));
    case Qt::DecorationRole:{
        QMimeDatabase db;
        QMimeType type=db.mimeTypeForFile(m_downloads.at(index.row()));
        return QVariant(type.iconName());
    }
    }
}