#include "Profile.h"
Profile::Profile()
{
    setStorageName(qEnvironmentVariable("USER"));
    setCachePath(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));

    connect(this,&QQuickWebEngineProfile::downloadPathChanged,[=](){
        qDebug() << "Download path changed to" << downloadPath();
    });
    connect(this,&QQuickWebEngineProfile::cachePathChanged,[=](){
        qDebug() << "Cache path changed to" << cachePath();
    });

    setPersistentCookiesPolicy(QQuickWebEngineProfile::ForcePersistentCookies);
    setUrlRequestInterceptor(new AdBlocker(this));
    setOffTheRecord(false);
    setHttpCacheType(DiskHttpCache);
    setHttpCacheMaximumSize(5000);
}