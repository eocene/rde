#ifndef BOOKMARKMODEL_H
#define BOOKMARKMODEL_H
#include <QtConcurrentRun>
#include <QAbstractListModel>
#include <QFileInfo>
#include <QFutureWatcher>
#include <QQmlEngine>
#include <QStandardPaths>
class BookmarkModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit BookmarkModel();
    ~BookmarkModel();
    Q_INVOKABLE void addBookmark(const QString& title,const QString& url,const QString& icon);
private:
    int rowCount(const QModelIndex &parent)const{Q_UNUSED(parent);return m_bookmarks.size();}
    QVariant data(const QModelIndex &index,int role) const override;
    QHash<int,QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QAbstractListModel::roleNames();
        h[Qt::UserRole]="URL";
        return h;
    }
    QList<QStringList> m_bookmarks;
    QList<QStringList> readFile();
    QFutureWatcher<QList<QStringList>>* m_watcher=nullptr;
    QFileInfo m_file;
//private slots:
    //void onFileChanged(const QString& path);
};
#endif