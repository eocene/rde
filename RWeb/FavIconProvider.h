#ifndef FAVICONPROVIDER_H
#define FAVICONPROVIDER_H
#include <QIcon>
#include <QQuickImageProvider>
class FavIconProvider:public QQuickImageProvider
{
public:
    FavIconProvider():QQuickImageProvider(Pixmap,ForceAsynchronousImageLoading){}
    QPixmap requestPixmap(const QString &id,QSize *size,const QSize &requestedSize)
    {
        Q_UNUSED(size);
        if(!id.isEmpty()){
            QPixmap p;
            if(p.load(id))
                return p.scaled(requestedSize,Qt::KeepAspectRatio,Qt::SmoothTransformation);
            else
                qWarning() << "Failed to load icon" << id;

        }
        return QIcon::fromTheme("unknown").pixmap(requestedSize);
    }
};
#endif