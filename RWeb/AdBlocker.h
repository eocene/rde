#ifndef ADBLOCKER_H
#define ADBLOCKER_H
#include <QWebEngineUrlRequestInterceptor>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDebug>
class AdBlocker:public QWebEngineUrlRequestInterceptor
{
    Q_OBJECT
public:
    explicit AdBlocker(QObject* parent);
    void interceptRequest(QWebEngineUrlRequestInfo& info) override;
    QNetworkAccessManager* m_network=nullptr;
private:
    QByteArray m_data;
};
#endif