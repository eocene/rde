import QtQuick
import QtQuick.Controls
import org.rde.ui
import org.rde.rweb
Pane{    
    property alias view:view
    padding:0    
    background:Item{}
    contentItem:RList{
        id:view
        anchors.fill:parent
        header:RBarRaised{
            width:view.width
            RText2{                
                anchors{fill:parent;margins:4}
                text:'Bookmarks'
                verticalAlignment:Text.AlignVCenter
                horizontalAlignment:Text.AlignHCenter
            }
        }
        model:BookmarkModel{}
        delegate:RListDelegate{
            onClicked:addTab(URL)
        }
        MouseArea{
            anchors.fill:parent
            acceptedButtons:Qt.RightButton
            onPressed:(mouse)=>{
                          menu=Qt.createComponent('BookmarkMenu.qml').createObject(view)
                          menu.bookmark=view.itemAt(mouse.x,mouse.y).text
                          menu.popup()
                      }
        }
    }
}