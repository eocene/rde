#ifndef DOWNLOADMODEL_H
#define DOWNLOADMODEL_H
#include <QtConcurrentRun>
#include <QAbstractListModel>
#include <QFileInfo>
#include <QFutureWatcher>
#include <QQmlEngine>
#include <QStandardPaths>
#include <QMimeDatabase>
class DownloadModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit DownloadModel();
    Q_INVOKABLE void addDownload(const QString& file);
    Q_INVOKABLE void clearDownloads();
private:
    int rowCount(const QModelIndex &parent)const{Q_UNUSED(parent);return m_downloads.size();}
    QVariant data(const QModelIndex &index,int role) const override;
    // QHash<int,QByteArray> roleNames() const override
    // {
    //     QHash<int,QByteArray> h=QAbstractListModel::roleNames();
    //     return h;
    // }
    QStringList m_downloads;
    QFileInfo m_file;
    QFutureWatcher<QStringList>* m_watcher=nullptr;
    QStringList readFile();
};
#endif