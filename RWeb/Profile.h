#ifndef PROFILE_H
#define PROFILE_H
#include <QQmlEngine>
#include <QQuickWebEngineProfile>
//#include <QWebEngineDownloadRequest>
#include <QStandardPaths>
#include "AdBlocker.h"
class Profile:public QQuickWebEngineProfile
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit Profile();
};
#endif