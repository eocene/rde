#include "Application.h"
Application::Application(int &argc,char **argv):QGuiApplication(argc,argv)
{
    setOrganizationName("rde");
    setApplicationName("rweb");
    setApplicationDisplayName("RWeb");

    QSettings s;
    m_bookmarks=s.value("bookmarks",false).toBool();
    m_downloads=s.value("downloads",false).toBool();
    m_history=s.value("history",false).toBool();
    m_homePage=s.value("homepage","about:blank").toString();

    m_engine=new QQmlApplicationEngine(this);
    m_engine->rootContext()->setContextProperty("RWeb",this);
    m_engine->addImageProvider("FavIcon",new FavIconProvider);
    m_engine->load("qrc:/Window.qml");
}
void Application::openFile(const QString& file)
{
    // QFileInfo i(downloadPath()+"/"+file);;
    // if(i.exists()){
    //     if(!QDesktopServices::openUrl(QUrl::fromLocalFile(i.absoluteFilePath()))){
    //         qWarning() << "RWeb: Failed to open" << i.absoluteFilePath();
    //     }
    // }
}
QString Application::processUrl(QString url)
{
    QUrl q=QUrl::fromUserInput(url);
    if(!q.isValid())
        return "about:blank";
    url=q.toString();
    return url;
}
void Application::quit()
{
    qGuiApp->quit();
}
Application::~Application()
{
    QSettings s;
    s.setValue("bookmarks",m_bookmarks);
    s.setValue("downloads",m_downloads);
    s.setValue("history",m_history);
    s.setValue("homepage",m_homePage);
}