#ifndef APPLICATION_H
#define APPLICATION_H
#include <QDesktopServices>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSettings>
#include "FavIconProvider.h"
class Application:public QGuiApplication
{
    Q_OBJECT
    Q_PROPERTY(bool bookmarks READ bookmarks WRITE setBookmarks NOTIFY bookmarksChanged)
    Q_PROPERTY(bool downloads READ downloads WRITE setDownloads NOTIFY downloadsChanged)
    Q_PROPERTY(bool history READ history WRITE setHistory NOTIFY historyChanged)
    Q_PROPERTY(QString homePage READ homePage WRITE setHomePage NOTIFY homePageChanged)
public:    
    explicit Application(int &argc,char **argv);
    ~Application();
    inline bool bookmarks(){return m_bookmarks;}
    inline void setBookmarks(bool on){m_bookmarks=on;emit bookmarksChanged();}
    inline bool downloads(){return m_downloads;}
    inline void setDownloads(bool on){m_downloads=on;emit downloadsChanged();}
    inline bool history(){return m_history;}
    inline void setHistory(bool on){m_history=on;emit historyChanged();}
    inline QString homePage(){return m_homePage;}
    void setHomePage(const QString& url){m_homePage=url;}
    Q_INVOKABLE void openFile(const QString& file);
    Q_INVOKABLE QString processUrl(QString url);
public slots:
    Q_SCRIPTABLE void quit();
private:
    QQmlApplicationEngine* m_engine=nullptr;
    bool m_bookmarks=false;
    bool m_downloads=false;
    bool m_history=false;
    QString m_homePage;
signals:
    void bookmarksChanged();
    void downloadsChanged();
    void historyChanged();
    void homePageChanged();
    void toggleWindow();
};
#endif