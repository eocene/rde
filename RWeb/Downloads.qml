import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.rde.ui
import org.rde.rweb
Pane{
    id:pane
    property DownloadMenu menu
    property alias view:view
    padding:0
    background:Item{}
    contentItem:ColumnLayout{
        spacing:0
        RBarRaised{
            Layout.fillWidth:true
            RText2{
                anchors{fill:parent;margins:4}
                text:'Downloads'
                verticalAlignment:Text.AlignVCenter
                horizontalAlignment:Text.AlignHCenter
            }
        }
        RToolBar{
            Layout.fillWidth:true
            Row{
                anchors.fill:parent
                spacing:0
                RToolButton{action:clearDownloads}
                RToolButton{action:openFolder}
            }
        }
        RList{
            id:view
            Layout.fillWidth:true
            Layout.fillHeight:true
            model:DownloadModel{}
            delegate:RListDelegate{
                onDoubleClicked:RWeb.openFile(model.display)

                //     background:ProgressBar{
                //         from:0
                //         to:1
                //         value:0.5
                //         background:RBar{}
                //     }
            }
            // MouseArea{
            //     anchors.fill:parent
            //     acceptedButtons:Qt.RightButton
            //     onPressed:(mouse)=>{
            //                   console.log("Clicked on index "+view.itemAt(mouse.x,mouse.y));
            //                   menu=Qt.createComponent('DownloadMenu.qml').createObject(pane)
            //                   //                                  menu.file=row.childAt(mouse.x+strip.contentX,mouse.y).source
            //                   menu.popup()
            //               }
            // }
        }
    }
    // RBarShadow{
    //     parent:view
    //     anchors{left:parent.left;right:parent.right;top:parent.top}
    // }
    Action{
        id:clearDownloads
        text:'Clear downloads'
        icon.name:'edit-clear-list'
        enabled:view.count>0
        onTriggered:{
            view.model.clearDownloads();
            //RWeb.downloads=false
        }
    }
    Action{
        id:openFolder
        text:'Open downloads folder'
        icon.name:'document-open-remote'
        //onTriggered:view.model.openFolder()
        onTriggered:RWeb.openFile("/data/")
    }
    Connections{
        target:userProfile
        function onDownloadRequested(download){
            //RWeb.downloads=true
            view.model.addDownload(download.suggestedFileName)
            download.accept()
        }
        function onDownloadFinished(download){
            //RWeb.downloads=false
        }
    }
}