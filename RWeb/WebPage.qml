import QtQuick
import QtQuick.Controls
import QtWebEngine
import org.rde.ui
Page{
    id:page
    property alias url:view.url
    property alias view:view
    property alias findBar:findBar
    property alias findText:findText
    title:view.title
    header:RToolBar{
        id:pageToolBar
        Row{
            anchors.fill:parent
            spacing:0
            RToolButton{
                action:goBack
                enabled:view.canGoBack
            }
            RToolButton{
                action:goForward
                enabled:view.canGoForward
            }
            RToolButton{
                action:reload
            }
            Item{
                id:addressBar
                anchors{top:parent.top;bottom:parent.bottom;margins:4}
                width:800
                RTextField{
                    id:addressField
                    anchors.fill:parent
                    text:view.url
                    onAccepted:view.url=RWeb.processUrl(text)
                    inputMethodHints:Qt.ImhUrlCharactersOnly
                    background:ProgressBar{
                        from:0
                        to:100
                        value:0
                        // value:view.loadProgress
                        // contentItem:Rectangle{
                        //     color:'darkblue'
                        // }
                        background:Rectangle{
                            border{width:1;color:Qt.rgba(0,0,0,0.5)}
                            radius:8
                            gradient:Gradient{
                                GradientStop{position:0;color:Qt.rgba(0.3,0.3,0.3,0.8)}
                                GradientStop{position:1;color:Qt.rgba(0.7,0.7,0.7,0.8)}
                            }
                        }
                    }
                }
            }
        }
    }
    contentItem:WebEngineView{
        id:view
        profile:userProfile
        //settings:engineSettings
        onFindTextFinished:{
            if(!result.activeMatch)
                findText.remove(findText.length-1,findText.length)
            matchLabel.text=result.numberOfMatches
        }
        onFullScreenRequested:{
            toolBar.visible=!request.toggleOn
            pageToolBar.visible=!request.toggleOn
            tabBar.visible=!request.toggleOn
            addressBar.visible=!request.toggleOn

            if(request.toggleOn){
                window.showFullScreen()
            }
            else{
                window.showNormal()
            }
            request.accept()
        }
        onNewWindowRequested:{
            if(request.userInitiated){
                window.addTab(request.requestedUrl)
                //                switch(request.destination){
                //                case WebEngineNewWindowRequest.InNewWindow:console.log("new window");break;	//In a separate window.
                //                case WebEngineNewWindowRequest.InNewTab:console.log("new tab");break;	//In a tab of the same window.
                //                case WebEngineNewWindowRequest.InNewDialog:window.addTab(request.requestedUrl);break;//	In a window without a tab bar, toolbar, or URL bar.
                //                case WebEngineNewWindowRequest.InNewBackgroundTab:window.addTab(request.requestedUrl);break;
                //                }
            }
            else{
                console.log("Page tried to open a popup!")
            }
        }
        //        function onLoadingChanged:
    }
    footer:RToolBar{
        id:findBar
        visible:false
        Row{
            anchors.fill:parent
            spacing:0
            RTextField{
                id:findText
                anchors{top:parent.top;bottom:parent.bottom;margins:4}
                width:1000
                onTextEdited:view.findText(text)
                Keys.onEscapePressed:find.trigger()
            }
            RText2{
                id:matchLabel
                anchors{top:parent.top;bottom:parent.bottom;margins:4}
            }
        }
    }
}