import QtQuick
import org.rde.ui
RTabButton{
    id:tab
    property WebPage page
    text:page?page.view.title:''
    icon.source:page?page.view.icon:''    
    width:implicitWidth+40

    // RIconButton{
    //     anchors{top:parent.top;bottom:parent.bottom;right:parent.right}
    //     width:height
    //     source:'image://Icon/dialog-close'
    //     onClick:tabBar.removeItem(tab)
    //     visible:tabBar.count>1
    // }

    Component.onDestruction:page.destroy()
}