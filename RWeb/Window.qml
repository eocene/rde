import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.rde.ui
import org.rde.rweb
ApplicationWindow{
    id:window
    color:Qt.rgba(0,0,0,0.5)
    visible:true    
    visibility:toggleFullScreen.checked?Window.FullScreen:Window.Maximized
    Profile{id:userProfile}
    Action{
        id:toggleFullScreen
        shortcut:'F11'
        checkable:true
    }
    Action{
        id:toggleBookmarks
        text:'Toggle bookmarks'
        icon.name:'bookmarks'
        shortcut:'Ctrl+B'
        checkable:true        
    }
    Action{
        id:toggleDownloads
        text:'Toggle Downloads'
        icon.name:'download'
        shortcut:'Ctrl+Shift+Y'
        checkable:true        
    }
    Action{
        id:toggleHistory
        text:'Toggle History'
        icon.name:'view-history'
        shortcut:'Ctrl+H'
        checkable:true        
    }
    Action{
        id:addBookmark
        text:'Add Bookmark'
        icon.name:'bookmark-new'
        shortcut:'Ctrl+D'
        onTriggered:bookmarks.view.model.addBookmark(currentPage.title,currentPage.url,currentPage.view.icon)
    }
    Action{
        id:newTab
        text:'New tab'
        icon.name:'window-new'
        shortcut:'Ctrl+T'
        onTriggered:addTab(RWeb.homePage)
    }
    Action{
        id:closeTab
        text:'Close tab'
        icon.name:'dialog-close'
        shortcut:'Ctrl+W'
        enabled:tabBar.count>1
        onTriggered:tabBar.removeItem(tabBar.currentItem)
    }
    Action{
        id:goBack
        text:'Go back'
        icon.name:'go-previous'
        shortcut:'Alt+Left'
        onTriggered:currentPage.view.goBack()
    }
    Action{
        id:goForward
        text:'Go forward'
        icon.name:'go-next'
        shortcut:'Alt+Right'
        onTriggered:currentPage.view.goForward()
    }
    Action{
        id:reload
        text:'Reload'
        icon.name:'view-refresh'
        shortcut:'F5'
        onTriggered:currentPage.view.reload()
    }
    Action{
        id:reloadOverrideCache
        text:'Reload Full'
        // icon.name:'view-refresh'
        shortcut:'Ctrl+F5'
        onTriggered:currentPage.view.reloadAndBypassCache()
    }
    Action{
        id:find
        text:'Find'
        icon.name:'edit-find'
        shortcut:'Ctrl+F'
        onTriggered:{
            if(currentPage.findBar.visible){
                currentPage.findBar.visible=false;
                currentPage.findText.undo()
            }
            else{
                currentPage.findBar.visible=true;
                currentPage.findText.forceActiveFocus()
            }
        }
    }
    Action{
        id:toggleAdblock
        text:'Toggle Adblock'
        icon.name:'security-high'
        //shortcut:'Ctrl+F'
        onTriggered:{
        }
    }
    header:RToolBar{
        id:toolBar
        Row{
            anchors.fill:parent
            spacing:0
            RToolButton{action:toggleBookmarks}
            RToolButton{action:toggleHistory}
            RToolButton{action:toggleDownloads}
            RToolButton{action:addBookmark}
            RToolButton{action:newTab}
            RToolButton{action:toggleAdblock}
        }
    }
    RowLayout{
        anchors.fill:parent
        spacing:0
        Bookmarks{
            id:bookmarks
            Layout.preferredWidth:300
            Layout.fillHeight:true            
            visible:toggleBookmarks.checked
        }
        History{
            id:history
            Layout.preferredWidth:300
            Layout.fillHeight:true            
            visible:toggleHistory.checked
        }
        Item{
            Layout.fillHeight:true
            Layout.fillWidth:true
            ColumnLayout{
                anchors.fill:parent
                spacing:0
                RTabBar{
                    id:tabBar
                    Layout.fillWidth:true
                    onCurrentItemChanged:currentPage=currentItem.page
                }
                StackLayout{
                    id:stack
                    Layout.fillWidth:true
                    Layout.fillHeight:true
                    currentIndex:tabBar.currentIndex
                }
            }
        }
        Downloads{
            id:downloads
            Layout.preferredWidth:300
            Layout.fillHeight:true
            //visible:rweb.downloads
            visible:toggleDownloads.checked

        }
    }
    function addTab(url){
        console.log("
")
        var page=Qt.createComponent('WebPage.qml').createObject(stack)
        page.url=url
        var tab=Qt.createComponent('Tab.qml').createObject(tabBar)
        tab.page=page
        tabBar.currentIndex=tabBar.count-1
        currentPage=page
    }
    property list<url> openUrls
    property WebPage currentPage
    Component.onCompleted:{

        // restore open urls

        //openUrls=RWeb.restoreUrls()
        //if(openUrls.length===0)newTab.trigger()
        //else for(var i=0;i<openUrls.length;i++)addTab(openUrls[i])
        //openUrls=[]
    }
    Component.onDestruction:{
        //property list<url> openUrls
        //for(var child in tabBar.contentChildren)
        //console.log(tabBar.contentChildren[child].page.url)
        //openUrls.append(tabBar.contentChildren[child].page.url)
        //console.log(openUrls)
    }
}