#define Service "org.rde.rway"
#define Path "/RWay"
#include "Application.h"
#include <QDBusInterface>
int main(int argc,char *argv[])
{
    if(QDBusConnection::systemBus().isConnected()){
        if(QDBusConnection::sessionBus().registerService(Service)){
            QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts,true);
            //qputenv("QT_WAYLAND_HARDWARE_INTEGRATION","");
            //qputenv("QT_WAYLAND_CLIENT_BUFFER_INTEGRATION","wayland-egl");
            //qputenv("QT_WAYLAND_SERVER_BUFFER_INTEGRATION","wayland-egl");
            //qputenv("QT_WAYLAND_SHELL_INTEGRATION","layer-shell");
            QByteArray wayland=qgetenv("WAYLAND_DISPLAY");
            if(wayland.isEmpty()){
                qputenv("QT_QPA_PLATFORM","eglfs");
                qputenv("QT_QPA_EGLFS_INTEGRATION","eglfs_kms");
                qputenv("QT_QPA_EGLFS_KMS_ATOMIC","1");
                qputenv("QT_QPA_EGLFS_KMS_CONFIG","/data/rde/kms.conf");
                //qputenv("QT_QPA_ENABLE_TERMINAL_KEYBOARD","1");
                //qputenv("QT_QPA_EVDEV_KEYBOARD_PARAMETERS","grab=1");
                //qputenv("QT_QPA_EVDEV_MOUSE_PARAMETERS","grab=1");
                //qputenv("QT_QPA_EGLFS_CURSOR","");
                qputenv("XCURSOR_THEME","NeoAlien-dark");
                qputenv("DISPLAY",":1");
            }
            Application app=Application(argc,argv);
            if(QDBusConnection::sessionBus().registerObject(Path,&app,QDBusConnection::ExportScriptableSlots))
                return app.exec();
            else return 3;
        }else{
            QDBusInterface i(Service,Path,"",QDBusConnection::sessionBus());
            if(i.isValid())
                i.call("quit");
            return 2;
        }
    }else return 1;
}