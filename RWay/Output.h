#ifndef OUTPUT_H
#define OUTPUT_H
#include <QWaylandOutput>
class Output:public QWaylandOutput
{
    Q_OBJECT
public:
    explicit Output(QWaylandCompositor* compositor,QWindow* window);
};
#endif