#include "Compositor.h"
Compositor::Compositor(QObject* parent):QWaylandCompositor(parent)
{
    setSocketName("rway");
    setUseHardwareIntegrationExtension(true);
}
void Compositor::create()
{
    QWaylandCompositor::create();
    setDefaultOutput(Outputs.first());
    createXdgShell();
    createLayerShell();
}
void Compositor::createLayerShell()
{
    m_layerShell=new LayerShell(this);
    m_layerShell->initialize();
    connect(m_layerShell,&LayerShell::surfaceCreated,[=](LayerShellSurface* surface){
        connect(surface->surface(),&QWaylandSurface::redraw,defaultOutput(),&QWaylandOutput::update);

        // connect(surface->surface(),&QWaylandSurface::redraw,[=](){
        //     qDebug() << "REDRAW LAYER SURFACE!";
        // });

        LayerShellView* view=new LayerShellView("org.rde.rshow"); //temp
        view->setSurface(surface->surface());
        view->setOutput(Outputs.first());
        m_views << view;
    });
}
void Compositor::createXdgShell()
{
    m_xdgShell=new QWaylandXdgShell(this);
    m_xdgShell->initialize();
    connect(m_xdgShell,&QWaylandXdgShell::toplevelCreated,[=](QWaylandXdgToplevel* toplevel,QWaylandXdgSurface* xdgSurface){
        connect(toplevel,&QWaylandXdgToplevel::activatedChanged,[=](){
            qDebug() << toplevel->title() << "activated";
        });
        connect(toplevel,&QWaylandXdgToplevel::appIdChanged,[=](){
            qDebug() << toplevel->title() << "appId:" << toplevel->appId();
        });
        connect(toplevel,&QWaylandXdgToplevel::decorationModeChanged,[=](){
            qDebug() << toplevel->title() << "decoration mode:" << toplevel->decorationMode();
        });
        connect(toplevel,&QWaylandXdgToplevel::fullscreenChanged,[=](){
            qDebug() << toplevel->title() << "fullscreen:" << toplevel->fullscreen();
        });
        connect(toplevel,&QWaylandXdgToplevel::maximizedChanged,[=](){
            qDebug() << toplevel->title() << "maximized:" << toplevel->maximized();
        });
        connect(toplevel,&QWaylandXdgToplevel::minSizeChanged,[=](){
            qDebug() << toplevel->title() << "minsize:" << toplevel->minSize();
        });
        connect(toplevel,&QWaylandXdgToplevel::maxSizeChanged,[=](){
            qDebug() << toplevel->title() << "max size:" << toplevel->maxSize();
        });
        connect(toplevel,&QWaylandXdgToplevel::resizingChanged,[=](){
            qDebug() << toplevel->title() << "resizing changed";
        });
        connect(toplevel,&QWaylandXdgToplevel::statesChanged,[=](){
            qDebug() << toplevel->title() << "states changed" << toplevel->states();
        });
        connect(toplevel,&QWaylandXdgToplevel::modalChanged,[=](){
            qDebug() << toplevel->title() << "modal changed" << toplevel->isModal();
        });
        connect(xdgSurface->surface(),&QWaylandSurface::redraw,defaultOutput(),&QWaylandOutput::update);
        ToplevelView* view=new ToplevelView(toplevel->appId());
        view->setSurface(xdgSurface->surface());
        view->setOutput(Outputs.first());
        m_views << view;

        connect(view,&QWaylandView::surfaceDestroyed,[=](){
            m_views.removeAll(view);
            delete view;
            defaultOutput()->update();
            emit windowUnmapped();
        });
        emit windowMapped();

        toplevel->sendMaximized(QSize(1680,1080));
    });
    connect(m_xdgShell,&QWaylandXdgShell::popupCreated,[=](QWaylandXdgPopup* popup,QWaylandXdgSurface* xdgSurface){
    });
}
View* Compositor::viewAt(const QPoint& position)
{
    for(auto it=m_views.crbegin();it!=m_views.crend();++it){
        View* view=*it;
        if(view->globalGeometry().contains(position))
            return view;
    }
    return nullptr;
}
void Compositor::raise(View* view)
{
    m_views.removeAll(view);
    m_views.append(view);
    defaultSeat()->setKeyboardFocus(view->surface());
    defaultOutput()->update();
}
static inline QPoint mapToView(const View* view,const QPoint& position)
{
    return view?view->mapToLocal(position):position;
}
void Compositor::handleMousePress(const QPoint& position,Qt::MouseButton button)
{
    if(!m_mouseView){
        if((m_mouseView=viewAt(position)))
            raise(m_mouseView);
    }
    auto* seat=defaultSeat();
    seat->sendMouseMoveEvent(m_mouseView,mapToView(m_mouseView,position));
    seat->sendMousePressEvent(button);
}
void Compositor::handleMouseRelease(const QPoint& position,Qt::MouseButton button,Qt::MouseButtons buttons)
{
    auto* seat=defaultSeat();
    seat->sendMouseMoveEvent(m_mouseView,mapToView(m_mouseView,position));
    seat->sendMouseReleaseEvent(button);
    if(buttons==Qt::NoButton){
        View* newView=viewAt(position);
        if(newView!=m_mouseView)
            seat->sendMouseMoveEvent(newView,mapToView(newView,position));
        m_mouseView=nullptr;
    }
}
void Compositor::handleMouseMove(const QPoint& position)
{
    View* view=m_mouseView?m_mouseView.data():viewAt(position);
    defaultSeat()->sendMouseMoveEvent(view,mapToView(view,position));
}
void Compositor::handleMouseWheel(const QPoint& angleDelta)
{
    // TODO: fix this to send a single event,when diagonal scrolling is supported
    if(angleDelta.x()!=0)
        defaultSeat()->sendMouseWheelEvent(Qt::Horizontal,angleDelta.x());
    if(angleDelta.y()!=0)
        defaultSeat()->sendMouseWheelEvent(Qt::Vertical,angleDelta.y());
}
void Compositor::handleKeyPress(quint32 nativeScanCode)
{
    defaultSeat()->sendKeyPressEvent(nativeScanCode);
}
void Compositor::handleKeyRelease(quint32 nativeScanCode)
{
    defaultSeat()->sendKeyReleaseEvent(nativeScanCode);
}
void Compositor::startRender()
{
    QWaylandOutput* out=defaultOutput();
    if(out)
        out->frameStarted();
}
void Compositor::endRender()
{
    QWaylandOutput* out=defaultOutput();
    if(out)
        out->sendFrameCallbacks();
}
QList<Output*> Compositor::Outputs;