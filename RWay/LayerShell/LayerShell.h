#ifndef LAYERSHELL_H
#define LAYERSHELL_H
#include <QWaylandShellTemplate>
#include <qwayland-server-wlr-layer-shell-unstable-v1.h>
#include "LayerShellSurface.h"
class LayerShellSurface;
class Q_WAYLANDCOMPOSITOR_EXPORT LayerShell:public QWaylandShellTemplate<LayerShell>,QtWaylandServer::zwlr_layer_shell_v1
{
    Q_OBJECT
    //Q_DECLARE_PRIVATE(LayerShell)
public:
    LayerShell(QWaylandCompositor* compositor);
    void initialize() override;
    using QtWaylandServer::zwlr_layer_shell_v1::interface;
    using QtWaylandServer::zwlr_layer_shell_v1::interfaceName;
protected:
    void zwlr_layer_shell_v1_get_layer_surface(Resource* resource,uint32_t id,wl_resource* surface,wl_resource* output,uint32_t layer,const QString& layer_namespace) override;
    void zwlr_layer_shell_v1_destroy_resource(Resource* resource) override;    
signals:
    void surfaceCreated(LayerShellSurface* layerShellSurface);
private:
    QWaylandCompositor* m_compositor=nullptr;
};
#endif