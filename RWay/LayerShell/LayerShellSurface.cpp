#include "LayerShellSurface.h"
LayerShellSurface::LayerShellSurface(LayerShell* shell,QWaylandSurface* surface,const QWaylandResource& resource)
{
    m_shell=shell;
    m_surface=surface;
    init(resource.resource());
    setExtensionContainer(surface);
    QWaylandShellSurface::initialize();
}
void LayerShellSurface::zwlr_layer_surface_v1_destroy_resource(Resource* resource)
{
    qDebug() << "LayerShellSurface: Destroy resource";
}
void LayerShellSurface::zwlr_layer_surface_v1_set_size(Resource* resource,uint32_t width,uint32_t height)
{
    qDebug() << "LayerShellSurface: Set size" << width << height;
}
void LayerShellSurface::zwlr_layer_surface_v1_set_anchor(Resource* resource,uint32_t anchor)
{
    qDebug() << "LayerShellSurface: Set anchor" << anchor;


     const uint32_t anchorMask=anchor_top|anchor_left|anchor_right|anchor_bottom;
     if (anchor > anchorMask) {
         wl_resource_post_error(resource->handle, error_invalid_anchor, "invalid anchor %d", anchor);
         return;
     }

     qDebug() << anchorMask;

     // pending.anchor = Qt::Edges();
     // if (anchor & anchor_top) {
     //     *pending.anchor |= Qt::TopEdge;
     // }
     // if (anchor & anchor_right) {
     //     *pending.anchor |= Qt::RightEdge;
     // }
     // if (anchor & anchor_bottom) {
     //     *pending.anchor |= Qt::BottomEdge;
     // }
     // if (anchor & anchor_left) {
     //     *pending.anchor |= Qt::LeftEdge;
     // }
}
void LayerShellSurface::zwlr_layer_surface_v1_set_exclusive_edge(Resource* resource,uint32_t edge)
{
    qDebug() << "LayerShellSurface: Set exclusive edge" << edge;



    //KWIN
    // if (!edge) {
    //     pending.exclusiveEdge = Qt::Edge();
    // } else if (edge == anchor_top) {
    //     pending.exclusiveEdge = Qt::TopEdge;
    // } else if (edge == anchor_right) {
    //     pending.exclusiveEdge = Qt::RightEdge;
    // } else if (edge == anchor_bottom) {
    //     pending.exclusiveEdge = Qt::BottomEdge;
    // } else if (edge == anchor_left) {
    //     pending.exclusiveEdge = Qt::LeftEdge;
    // } else {
    //     wl_resource_post_error(resource->handle, error_invalid_exclusive_edge, "Invalid exclusive edge: %d", edge);
    // }
}
void LayerShellSurface::zwlr_layer_surface_v1_set_exclusive_zone(Resource* resource,int32_t zone)
{
    qDebug() << "LayerShellSurface: Set exclusive zone" << zone;

    //KWIN
    //pending.exclusiveZone=zone;
}
void LayerShellSurface::zwlr_layer_surface_v1_set_margin(Resource* resource,int32_t top,int32_t right,int32_t bottom,int32_t left)
{
    qDebug() << "LayerShellSurface: Set margin" << top << right << bottom << left;

    //KWIN
    //pending.margins=QMargins(left,top,right,bottom);
}
void LayerShellSurface::zwlr_layer_surface_v1_set_keyboard_interactivity(Resource* resource,uint32_t keyboard_interactivity)
{
    qDebug() << "LayerShellSurface: Set keyboard interactivity" << keyboard_interactivity;


    if(keyboard_interactivity==0)
        m_acceptsFocus=false;
    else if(keyboard_interactivity==1)
        m_acceptsFocus=true;
}
/*
void LayerShellSurface::zwlr_layer_surface_v1_get_popup(Resource *resource, struct ::wl_resource *popup)
{
    XdgPopupInterface* popup=XdgPopupInterface::get(popup_resource);
    XdgPopupInterfacePrivate* popupPrivate=XdgPopupInterfacePrivate::get(popup);
    if (popup->isConfigured()) {
        wl_resource_post_error(resource->handle, error_invalid_surface_state, "xdg_popup surface is already configured");
        return;
    }
    popupPrivate->parentSurface = surface;
}
*/
void LayerShellSurface::zwlr_layer_surface_v1_ack_configure(Resource* resource,uint32_t serial)
{
    qDebug() << "LayerShellSurface: ack configure" << serial;

    //KWIN
    // if (!state.serials.contains(serial)) {
    //     wl_resource_post_error(resource->handle, error_invalid_surface_state, "invalid configure serial %d", serial);
    //     return;
    // }
    // while(!state.serials.isEmpty()){
    //     const quint32 head=state.serials.takeFirst();
    //     if (head==serial){
    //         break;
    //     }
    // }
    // if(!state.closed) {
    //     pending.acknowledgedConfigure=serial;
    // }
}
void LayerShellSurface::zwlr_layer_surface_v1_destroy(Resource* resource)
{
    wl_resource_destroy(resource->handle);
}
void LayerShellSurface::zwlr_layer_surface_v1_set_layer(Resource* resource,uint32_t layer)
{
    qDebug() << "LayerShellSurface: Setting layer to" << layer;

    //KWIN
    // if (Q_UNLIKELY(layer > LayerShellV1InterfacePrivate::layer_overlay)) {
    //     wl_resource_post_error(resource->handle, LayerShellV1InterfacePrivate::error_invalid_layer, "invalid layer %d", layer);
    //     return;
    // }
    // pending.layer=LayerSurfaceV1Interface::Layer(layer);
}
QWaylandSurfaceRole LayerShellSurface::s_role("zwlr_layer_surface_v1");
QWaylandSurfaceRole* LayerShellSurface::role()
{
    return &s_role;
}
LayerShellSurface* LayerShellSurface::fromResource(wl_resource *resource)
{
    auto* res=Resource::fromResource(resource);
    return static_cast<LayerShellSurface*>(res->object());
}