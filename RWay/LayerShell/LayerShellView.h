#ifndef LAYERSHELLVIEW_H
#define LAYERSHELLVIEW_H
#include "../View/View.h"
#include <QDebug>
class LayerShellView:public View
{
    Q_OBJECT
public:
    explicit LayerShellView(QString appId);
    void initPosition(const QSize& screenSize,const QSize& surfaceSize) override;
};
#endif