#ifndef LAYERSHELLSURFACE_H
#define LAYERSHELLSURFACE_H
#include <QWaylandShellSurfaceTemplate>
#include <QWaylandResource>
#include <QWaylandSurface>
#include <qwayland-server-wlr-layer-shell-unstable-v1.h>
#include "LayerShell.h"
class LayerShell;
class Q_WAYLANDCOMPOSITOR_EXPORT LayerShellSurface:public QWaylandShellSurfaceTemplate<LayerShellSurface>,public QtWaylandServer::zwlr_layer_surface_v1
{
    Q_OBJECT
    //Q_DECLARE_PRIVATE(LayerShellSurface)
public:
    explicit LayerShellSurface(LayerShell* shell,QWaylandSurface* surface,const QWaylandResource& resource);
    using QtWaylandServer::zwlr_layer_surface_v1::interface;
    using QtWaylandServer::zwlr_layer_surface_v1::interfaceName;
    static QWaylandSurfaceRole* role();
    static LayerShellSurface* fromResource(::wl_resource* resource);
    QWaylandSurface* surface()const{return m_surface;}
    inline Qt::Edges anchor()const{return m_anchors;}
    inline QMargins margins()const{return m_margins;}
    inline bool acceptsFocus()const{return m_acceptsFocus;}
    inline int leftMargin()const{return m_leftMargin;}
    inline int rightMargin()const{return m_rightMargin;}
    inline int topMargin()const{return m_topMargin;}
    inline int bottomMargin()const{return m_bottomMargin;}
    inline int exclusiveZone()const{return m_exclusiveZone;}
    inline Qt::Edge exclusiveEdge()const{return m_exclusiveEdge;}
    inline QString scope()const{return m_scope;}

#if QT_CONFIG(wayland_compositor_quick)
    QWaylandQuickShellIntegration* createIntegration(QWaylandQuickShellSurfaceItem* item) override {return NULL;}
#endif

private:
    static QWaylandSurfaceRole s_role;
    QWaylandSurface* m_surface=nullptr;
    LayerShell* m_shell=nullptr;
    bool m_acceptsFocus;
    int m_rightMargin;
    int m_leftMargin;
    int m_topMargin;
    int m_bottomMargin;
    QString m_scope;
    Qt::Edge m_exclusiveEdge;
    Qt::Edges m_anchors;
    QMargins m_margins;
    int m_exclusiveZone;
protected:
    void zwlr_layer_surface_v1_destroy_resource(Resource* resource) override;
    void zwlr_layer_surface_v1_set_size(Resource* resource,uint32_t width,uint32_t height) override;
    void zwlr_layer_surface_v1_set_anchor(Resource* resource,uint32_t anchor) override;
    void zwlr_layer_surface_v1_set_exclusive_edge(Resource* resource,uint32_t edge) override;
    void zwlr_layer_surface_v1_set_exclusive_zone(Resource* resource,int32_t zone) override;
    void zwlr_layer_surface_v1_set_margin(Resource* resource,int32_t top,int32_t right,int32_t bottom,int32_t left) override;
    void zwlr_layer_surface_v1_set_keyboard_interactivity(Resource* resource,uint32_t keyboard_interactivity) override;
    //void zwlr_layer_surface_v1_get_popup(Resource* resource,struct ::wl_resource* popup) override;
    void zwlr_layer_surface_v1_ack_configure(Resource* resource,uint32_t serial) override;
    void zwlr_layer_surface_v1_destroy(Resource* resource) override;
    void zwlr_layer_surface_v1_set_layer(Resource* resource,uint32_t layer) override;
};
#endif