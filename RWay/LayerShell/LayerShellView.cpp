#include "LayerShellView.h"
LayerShellView::LayerShellView(QString appId):View(appId)
{
}
void LayerShellView::initPosition(const QSize& screenSize,const QSize& surfaceSize)
{
    qDebug() << "Init pos Layer" << screenSize.width() << surfaceSize.width();


    if(m_positionSet)
        return;
    setGlobalPosition(QPoint(0,0));
}