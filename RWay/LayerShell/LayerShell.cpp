#include "LayerShell.h"
LayerShell::LayerShell(QWaylandCompositor* compositor):QWaylandShellTemplate<LayerShell>(compositor)
{
    m_compositor=compositor;
}
void LayerShell::initialize()
{
    QWaylandShellTemplate::initialize();
    QWaylandCompositor* compositor=static_cast<QWaylandCompositor*>(extensionContainer());
    if(!compositor){
        qWarning() << "Could not find QWaylandCompositor when initializing LayerShell!";
        return;
    }
    init(compositor->display(),1);
}
void LayerShell::zwlr_layer_shell_v1_get_layer_surface(Resource* resource,uint32_t id,wl_resource* surface,wl_resource* output,uint32_t layer,const QString& layer_namespace)
{
    QWaylandSurface* s=QWaylandSurface::fromResource(surface);
    if(!s->setRole(LayerShellSurface::role(),resource->handle,ZWLR_LAYER_SHELL_V1_ERROR_ROLE))
        return;
    QWaylandResource r(wl_resource_create(resource->client(),&zwlr_layer_surface_v1_interface,wl_resource_get_version(resource->handle),id));
    auto* l=new LayerShellSurface(this,s,r);
    emit surfaceCreated(l);
}
void LayerShell::zwlr_layer_shell_v1_destroy_resource(Resource* resource)
{
    Q_UNUSED(resource);
    delete this;
}