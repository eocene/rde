#ifndef WINDOW_H
#define WINDOW_H
#include "Compositor.h"
#include <QOpenGLWindow>
#include <QOpenGLTextureBlitter>
class Window:public QOpenGLWindow
{
    Q_OBJECT
public:
    Window(){}
    void setCompositor(Compositor* comp);
signals:
    void glReady();
protected:
    void initializeGL() override;
    void paintGL() override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;
    void keyReleaseEvent(QKeyEvent* event) override;
private:
    QOpenGLTextureBlitter m_textureBlitter;
    Compositor* m_compositor=nullptr;
};
#endif