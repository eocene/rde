#ifndef APPLICATION_H
#define APPLICATION_H
#include <QGuiApplication>
#include <QScreen>
#include "Compositor.h"
#include "EventServer.h"
#include "Output.h"
#include "Window.h"
#include "Shell.h"
class Application:public QGuiApplication
{
    Q_OBJECT
public:
    explicit Application(int &argc,char **argv);
private:    
    Compositor* m_compositor=nullptr;
    EventServer* m_eventServer=nullptr;
    Window* m_window=nullptr;
    Shell* m_shell=nullptr;
};
#endif