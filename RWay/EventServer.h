#ifndef EVENTSERVER_H
#define EVENTSERVER_H
#include <QLocalServer>
#include <QLocalSocket>
#include <QDebug>
class EventServer:public QLocalServer
{
    Q_OBJECT
public:
    EventServer(QObject* parent);
    ~EventServer();
public slots:
    void windowMapped();
    void windowUnmapped();
private:
    void sendMessage(const QByteArray& message);
    QList<QLocalSocket*> m_clients;
};
#endif