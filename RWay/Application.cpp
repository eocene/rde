#include "Application.h"
Application::Application(int &argc,char **argv):QGuiApplication(argc,argv)
{
    setOrganizationName("rde");
    setApplicationName("rway");
    setApplicationDisplayName("RWay");

    m_compositor=new Compositor(this);
    m_eventServer=new EventServer(this);

    connect(m_compositor,&Compositor::windowMapped,m_eventServer,&EventServer::windowMapped);
    connect(m_compositor,&Compositor::windowUnmapped,m_eventServer,&EventServer::windowUnmapped);

    m_window=new Window;
    m_window->resize(1920,1080);
    //m_window->resize(5760,1080);
    m_window->setCompositor(m_compositor);

    // connect(this,&QGuiApplication::screenAdded,[=](QScreen* screen){
    //     qDebug() << "RWAY: Added screen" << screen->name();
    // });
    // connect(this,&QGuiApplication::screenRemoved,[=](QScreen* screen){
    //     qDebug() << "RWAY: Removed screen" << screen->name();
    // });

    QWaylandOutputMode mode(QSize(1920,1080),60000);
    foreach(QScreen* screen,screens()){
        Output* output=new Output(m_compositor,m_window);
        output->addMode(mode,true);
        output->setCurrentMode(mode);
        Compositor::Outputs << output;
    }
    m_window->showFullScreen();

    qputenv("WAYLAND_DISPLAY",m_compositor->socketName());
    qputenv("QT_QPA_PLATFORM","wayland");
    m_shell=new Shell(this);
}