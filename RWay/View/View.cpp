#include "View.h"
QOpenGLTexture* View::getTexture()
{
    if(advance())
        m_texture=currentBuffer().toOpenGLTexture();
    return m_texture;
}
QPoint View::mapToLocal(const QPoint& globalPos) const
{
    return globalPos-globalPosition();
}
// void View::initPosition(const QSize &screenSize,const QSize &surfaceSize)
// {
//     if(m_positionSet)
//         return;


//     //QRandomGenerator rand(400);
//     //int xrange = qMax(screenSize.width()-surfaceSize.width(),1);
//     //int yrange = qMax(screenSize.height()-surfaceSize.height(),1);
//     //setGlobalPosition(QPoint(rand.bounded(xrange),rand.bounded(yrange)));
//     //setGlobalPosition(QPoint(100,100));

//     //setGlobalPosition(QPoint(output()->window()->width()/8,0));
//     setGlobalPosition(QPoint(0,0));
// }