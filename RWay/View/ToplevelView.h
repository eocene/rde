#ifndef TOPLEVELVIEW_H
#define TOPLEVELVIEW_H
#include "View.h"
#include <QRandomGenerator>
class ToplevelView:public View
{
    Q_OBJECT
public:
    explicit ToplevelView(QString appId);
    void initPosition(const QSize &screenSize,const QSize &surfaceSize) override;
};
#endif