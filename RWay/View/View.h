#ifndef VIEW_H
#define VIEW_H
#include <QWaylandView>
class View:public QWaylandView
{
    Q_OBJECT
public:
    explicit View(QString appId):m_appId(appId){}
    QOpenGLTexture* getTexture();
    QString appId()const{return m_appId;}
    QRect globalGeometry()const{return QRect(globalPosition(),surface()->destinationSize());}
    void setGlobalPosition(const QPoint &globalPos){m_pos=globalPos;m_positionSet=true;}
    QPoint globalPosition()const{return m_pos;}
    QPoint mapToLocal(const QPoint &globalPos) const;
    QSize size() const {return surface()?surface()->destinationSize():QSize();}
    virtual void initPosition(const QSize &screenSize,const QSize &surfaceSize)=0;
protected:
    bool m_positionSet=false;
private:
    QOpenGLTexture* m_texture=nullptr;
    QPoint m_pos;
    QString m_appId;
};
#endif