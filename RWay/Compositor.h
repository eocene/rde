#ifndef COMPOSITOR_H
#define COMPOSITOR_H
#include <QWaylandCompositor>
#include <QWaylandSeat>
#include <QWaylandXdgShell>
#include "View/ToplevelView.h"
#include "Output.h"
#include "LayerShell/LayerShell.h"
#include "LayerShell/LayerShellView.h"
class Compositor:public QWaylandCompositor
{
    Q_OBJECT
public:
    Compositor(QObject* parent);
    void create() override;
    QList<View*> views()const{return m_views;}
    View* viewAt(const QPoint &position);
    void raise(View *view);
    void handleMousePress(const QPoint& position,Qt::MouseButton button);
    void handleMouseRelease(const QPoint& position,Qt::MouseButton button,Qt::MouseButtons buttons);
    void handleMouseMove(const QPoint& position);
    void handleMouseWheel(const QPoint& angleDelta);
    void handleKeyPress(quint32 nativeScanCode);
    void handleKeyRelease(quint32 nativeScanCode);
    void startRender();
    void endRender();
    static QList<Output*> Outputs;
private:
    LayerShell* m_layerShell=nullptr;
    QWaylandXdgShell* m_xdgShell=nullptr;
    QList<View*> m_views;
    QPointer<View> m_mouseView;
    void createXdgShell();
    void createLayerShell();
signals:
    void windowMapped();
    void windowUnmapped();
};
#endif