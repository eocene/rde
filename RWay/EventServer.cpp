#include "EventServer.h"
EventServer::EventServer(QObject* parent):QLocalServer(parent)
{
    if(listen(qgetenv("RWAY_SOCKET"))){
        connect(this,&EventServer::newConnection,[=](){
            m_clients << nextPendingConnection();
        });
    }else{
        qWarning() << "RWay event server not listening?";
    }
}
void EventServer::windowMapped()
{
    sendMessage("window mapped");
}
void EventServer::windowUnmapped()
{
    sendMessage("window unmapped");
}
void EventServer::sendMessage(const QByteArray& message)
{
    if(isListening()){
        QByteArray data;
        QDataStream out(&data,QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_6_5);
        out << message;
        foreach(QLocalSocket* client,m_clients){
            client->write(data);
            client->flush();
        }
    }
}
EventServer::~EventServer()
{
    if(isListening())
        close();
}