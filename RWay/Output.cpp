#include "Output.h"
Output::Output(QWaylandCompositor* compositor,QWindow* window):QWaylandOutput(compositor,window)
{
    setSizeFollowsWindow(true);
}