#ifndef MUSICMODEL_H
#define MUSICMODEL_H
#include "Model.h"
#include "taglib/fileref.h"
class MusicModel:public Model
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit MusicModel():Model("music","audio","audio-x-generic",QStandardPaths::MusicLocation,QStringList() << "title" << "artist" << "album" << "genre" << "year"){}
    QString metaData(const QString &file)
    {
        TagLib::FileRef f(file.toLocal8Bit());
        const TagLib::Tag *t=f.tag();
        QString title=t->title().toCString(true);
        QString artist=t->artist().toCString(true);
        QString album=t->album().toCString(true);
        QString genre=t->genre().toCString(true);
        QString year=QString::number(f.tag()->year());
        return "(\""+file+"\",\""+title+"\",\""+artist+"\",\""+album+"\",\""+genre+"\",\""+year+"\")";
    }
};
#endif