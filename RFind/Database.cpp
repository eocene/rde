#include "Database.h"
Database::Database()
{
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));
    if(dir.mkpath(dir.absolutePath())){
        QSqlDatabase db=QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(dir.absolutePath()+"/database");
        db.setConnectOptions("QSQLITE_ENABLE_SHARED_CACHE");


        // if(db.open())

            // qDebug() << "Creating database...";
            // else{
            //     qWarning() << "Could not open database!";
            // }
    }
}
void Database::addTable(const QString &name,const QStringList& columns)
{
    qDebug() << "Adding db table" << name;
    qDebug() << columns;
}
Database::~Database()
{
    qDebug() << "Deleting database!";
}
Database* Database::instance(){if(!s_instance)s_instance=new Database;return s_instance;}
Database* Database::s_instance;