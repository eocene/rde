#include "Application.h"
Application::Application(int &argc,char **argv):QGuiApplication(argc,argv)
{
    setOrganizationName("rde");
    setApplicationName("rfind");
    setApplicationDisplayName("RFind");





    QSettings settings;
    m_filter=settings.value("filter").toString();
    m_tab=settings.value("tab",0).toInt();

    m_engine=new QQmlApplicationEngine(this);

    m_engine->rootContext()->setContextProperty("RFind",this);
    m_engine->rootContext()->setContextProperty("Database",Database::instance());



    m_engine->load("qrc:/Window.qml");

    connect(this,&QGuiApplication::aboutToQuit,this,[=](){
        {
            QSqlDatabase db=QSqlDatabase::database("rfind");
            if(db.isOpen()){
                db.close();
            }
        }
        QSqlDatabase::removeDatabase("rfind"); // still in use?
    });
}
void Application::quit()
{
    qGuiApp->quit();
}
void Application::setFilter(const QString& filter)
{
    m_filter=filter;
    foreach(Model* model,Model::s_models)
        model->search(filter);
}
Application::~Application()
{
    // qDebug() << "DESTRUCT RFIND";

    // {
    //     QSqlDatabase db=QSqlDatabase::database("rfind");
    //     if(db.isOpen())
    //         db.close();
    // }
    //QSqlDatabase::removeDatabase("rfind");
    QSettings settings;
    settings.setValue("filter",m_filter);
    settings.setValue("tab",m_tab);
}