#include "Model.h"
Model::Model(const QString& table,const QString& mime,const QString& icon,QStandardPaths::StandardLocation location,const QStringList& columns):QSqlTableModel(nullptr,QSqlDatabase::database("rfind"))
{
    m_mime=mime;
    m_icon=icon;
    m_location=location;

    //qDebug() << "Creating model for table" << table;

    Database::instance()->addTable(table,columns);


    // QSqlDatabase db=QSqlDatabase::database("rfind");
    // if(db.open()){
    //     if(db.tables().contains(table))
    //         setTable(table);
    //     else{
    //         QSqlQuery q(db);
    //         if(q.exec("CREATE TABLE "+table+"(file,"+columns.join(",")+",PRIMARY KEY(file))"))
    //             import(table);
    //         q.finish();
    //     }
    // }
    // else
    //     qDebug() << "Failed to open " << table;
    s_models.append(this);
}
void Model::import(const QString& table)
{
    m_watcher=new QFutureWatcher<QStringList>(this);
    connect(m_watcher,&QFutureWatcherBase::finished,[=](){
        QSqlDatabase db=QSqlDatabase::database("rfind");
        if(db.transaction()){
            QSqlQuery q(db);
            q.setForwardOnly(true);
            foreach(QString line,m_watcher->result()){
                if(q.prepare("INSERT INTO "+table+" VALUES"+line)){
                    if(q.exec()){
                    }
                }
            }
            if(db.commit()){
                setTable(table);
                select();
            }
            q.finish();
        }
        m_watcher->deleteLater();
    });
    m_watcher->setFuture(QtConcurrent::run(&Model::importThread,this));
}
extern QStringList Model::importThread()
{
    QStringList l,ext;
    QMimeDatabase mdb;

    foreach(QMimeType type,mdb.allMimeTypes()){
        if(type.name().startsWith(m_mime+"/")){
            foreach(QString ss,type.suffixes()){
                if(!ss.isEmpty() && !ext.contains(ss)){
                    ext << ss;
                }
            }
        }
    }
    foreach(QString path,QStandardPaths::standardLocations(m_location)){
        QDir dir(path,"*."+ext.join(" *."),QDir::Unsorted,QDir::Files);
        if(dir.isReadable()){
            QDirIterator iterator(dir,QDirIterator::Subdirectories);
            while(iterator.hasNext())
                l << metaData(iterator.next());
        }
        else{
            qWarning() << dir.absolutePath() << "not readable!";
        }
    }
    return l;
}
void Model::search(const QString& text)
{
    if(text.isEmpty())setFilter("");
    else setFilter("file LIKE \"%"+text+"%\"");
    if(rowCount()==0)
        select();
}
const bool Model::open()
{
    QFileInfo i(data(index(m_currentRow,0),Qt::DisplayRole).toString());
    if(i.isReadable() && QDesktopServices::openUrl(QUrl::fromLocalFile(i.absoluteFilePath()))){
        return true;
    }
    return false;
}
QVariant Model::data(const QModelIndex& index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return QSqlQueryModel::data(index,role);
    case Qt::TextAlignmentRole:{if(index.column()==5)return Qt::AlignHCenter;else return Qt::AlignLeft;}
    case Qt::DecorationRole:return "image://Icon/"+m_icon;
    case Qt::UserRole:{if(index.column()==1)return "IconText";else return "Text";}
    }
}
QList<Model*> Model::s_models;