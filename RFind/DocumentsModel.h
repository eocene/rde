#ifndef DOCUMENTSMODEL_H
#define DOCUMENTSMODEL_H
#include "Model.h"
class DocumentsModel:public Model
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit DocumentsModel():Model("documents","text","x-office-document",QStandardPaths::DocumentsLocation,QStringList() << "name"){}
    QString metaData(const QString& file)
    {
        QFileInfo info(file);
        QString name=info.baseName();
        return "(\""+file+"\",\""+name+"\")";
    }
};
#endif