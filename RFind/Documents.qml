import org.rde.ui
import org.rde.rfind
Panel{
    RTable{
        anchors.fill:parent
        onActivated:function(row){documentsModel.open()}
        table{
            model:DocumentsModel{id:documentsModel}
            columnWidthProvider:function(column){
                if(column===0)
                    return 0;
                else return 400;
            }
        }
    }
}