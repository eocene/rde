#ifndef MOVIESMODEL_H
#define MOVIESMODEL_H
#include "Model.h"
#include <QDebug>
class MoviesModel:public Model
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit MoviesModel():Model("movies","video","video-x-generic",QStandardPaths::MoviesLocation,QStringList() << "title" << "genre" << "year")
    {
    }
    QString metaData(const QString& file)
    {
        QFileInfo info(file);
        QString title=info.baseName();
        QString genre;
        QString year;
        return "(\""+file+"\",\""+title+"\",\""+genre+"\",\""+year+"\")";
    }
};
#endif