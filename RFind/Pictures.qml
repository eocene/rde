import org.rde.ui
import org.rde.rfind
Panel{
    RTable{
        anchors.fill:parent
        onActivated:function(row){picturesModel.open()}
        table{
            model:PicturesModel{id:picturesModel}
            columnWidthProvider:function(column){
                if(column===0)
                    return 0;
                else return 400;
            }
        }
    }
}