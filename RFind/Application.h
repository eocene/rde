#ifndef APPLICATION_H
#define APPLICATION_H
#include <QFileInfo>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSettings>
#include "Database.h"
#include "Model.h"
class Application:public QGuiApplication
{
    Q_OBJECT
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY filterChanged)
    Q_PROPERTY(int tab READ tab WRITE setTab NOTIFY tabChanged)
public:
    explicit Application(int &argc,char **argv);
    ~Application();
    inline int tab(){return m_tab;}
    inline void setTab(int tab){m_tab=tab;}
public slots:
    Q_SCRIPTABLE void quit();
private:
    QQmlApplicationEngine* m_engine=nullptr;
    QString m_filter;
    QString filter(){return m_filter;}
    void setFilter(const QString& filter);
    int m_tab;
signals:
    void focusSearch();
    void filterChanged();
    void tabChanged();
};
#endif