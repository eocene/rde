import org.rde.ui
import org.rde.rfind
Panel{
    RTable{
        anchors.fill:parent
        onActivated:function(row){moviesModel.open()}
        table{
            model:MoviesModel{id:moviesModel}
            columnWidthProvider:function(column){
                if(column===0)
                    return 0;
                else return 400;
            }
        }
    }
}