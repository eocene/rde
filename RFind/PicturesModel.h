#ifndef PICTURESMODEL_H
#define PICTURESMODEL_H
#include "Model.h"
class PicturesModel:public Model
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit PicturesModel():Model("pictures","image","image-x-generic",QStandardPaths::PicturesLocation,QStringList() << "name"){}
    QString metaData(const QString &file)
    {
        QFileInfo info(file);
        QString name=info.baseName();
        return "(\""+file+"\",\""+name+"\")";
    }
};
#endif