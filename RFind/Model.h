#ifndef MODEL_H
#define MODEL_H
#include <QDesktopServices>
#include <QDir>
#include <QDirIterator>
#include <QFutureWatcher>
#include <QMimeDatabase>
#include <QQmlEngine>
#include <QSqlError>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QtConcurrentRun>
#include <QUrl>

#include "Database.h"


class Model:public QSqlTableModel
{
    Q_OBJECT
    Q_PROPERTY(int currentRow READ currentRow WRITE setCurrentRow NOTIFY currentRowChanged)
public:
    explicit Model(const QString& table,const QString& mime,const QString& icon,QStandardPaths::StandardLocation location,const QStringList& columns);
    void import(const QString& table);    
    static QList<Model*> s_models;
    int currentRow(){return m_currentRow;}
    void setCurrentRow(const int& row){m_currentRow=row;}
    Q_INVOKABLE void search(const QString& text);
    Q_INVOKABLE const bool open();
private:
    QHash<int,QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QSqlTableModel::roleNames();
        //h[Qt::DecorationRole]="decoration";
        h[Qt::TextAlignmentRole]="textAlignment";
        h[Qt::UserRole]="type";
        return h;
    }
    QVariant data(const QModelIndex &index,int role) const override;
    QStandardPaths::StandardLocation m_location;
    QFutureWatcher<QStringList>* m_watcher=nullptr;
    QStringList importThread();
    QString m_mime;
    QString m_icon;
    virtual QString metaData(const QString& file)=0;
    int m_currentRow;
signals:
    void currentRowChanged();
};
#endif