import org.rde.ui
import org.rde.rfind
Panel{
    RTable{
        anchors.fill:parent
        onActivated:function(row){musicModel.open()}
        table{
            model:MusicModel{id:musicModel}
            columnWidthProvider:function(column){
                if(column===0)
                    return 0;
                else return 400;
            }
        }
    }
}