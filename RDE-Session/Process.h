#ifndef PROCESS_H
#define PROCESS_H
#include <QProcess>
#include <QStandardPaths>
#include <QDebug>
class Process:public QProcess
{
    Q_OBJECT
public:
    explicit Process(const QString& program,QObject* parent);
};
#endif