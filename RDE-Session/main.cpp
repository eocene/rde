#include <QDBusConnection>
#include <QCoreApplication>
#include <QDateTime>
#include <QFile>
#include "Application.h"
// #define ApplicationName "rde-session"
// #define ApplicationVersion "1.0"
// #define OrganizationName "rde"
// #define OrganizationDomain "org.rde"
void logger(QtMsgType type,const QMessageLogContext &,const QString & msg)
{
    QString txt;
    switch(type){
    case QtDebugMsg:
        txt=QString("Debug: %1").arg(msg);
        break;
    case QtInfoMsg:
        //        txt=QString("Info: %1").arg(msg);
        txt=QString("%1").arg(msg);
        break;
    case QtWarningMsg:
        txt=QString("Warning: %1").arg(msg);
        break;
    case QtCriticalMsg:
        txt=QString("Critical: %1").arg(msg);
        break;
    case QtFatalMsg:
        txt=QString("Fatal: %1").arg(msg);
        abort();
    }
    QFile outFile("/data/log/rde-session.log"); //unhardcode
    outFile.open(QIODevice::WriteOnly|QIODevice::Append);
    QTextStream ts(&outFile);
    ts << QDateTime::currentDateTime().toString("dd.MM.yyyy/hh:mm:ss")+":"+txt << Qt::endl;
}
int main(int argc, char *argv[])
{
    //qInstallMessageHandler(logger);
    if(QDBusConnection::sessionBus().registerService("org.rde.session")){
        // QCoreApplication::setOrganizationName(OrganizationName);
        // QCoreApplication::setOrganizationDomain(OrganizationDomain);
        // QCoreApplication::setApplicationName(ApplicationName);
        // QCoreApplication::setApplicationVersion(ApplicationVersion);
        QCoreApplication a(argc,argv);
        Application app(argc,argv);
        if(QDBusConnection::sessionBus().registerObject("/session",&app,QDBusConnection::ExportScriptableSlots)){
            return app.exec();
        }
        //return session->exec();
    }else return 1;
}