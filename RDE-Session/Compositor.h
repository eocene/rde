#ifndef COMPOSITOR_H
#define COMPOSITOR_H
#include "Process.h"
#include <QSettings>
class Compositor:public Process
{
    Q_OBJECT
public:
    explicit Compositor(const QString& compositor,QObject* parent);
};
#endif