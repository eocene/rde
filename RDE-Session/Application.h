#ifndef APPLICATION_H
#define APPLICATION_H
#include <QCoreApplication>
#include <QDBusConnection>

//#include <QStandardPaths>
//#include <QSettings>

#include "AI.h"
#include "RShell.h"
#include "RShow.h"

#include <DFL/DF6/DFLogin1.hpp>
#include "Compositor.h"
class Application:public QCoreApplication
{
    Q_OBJECT
public:
    explicit Application(int &argc,char **argv);
public slots:
    Q_SCRIPTABLE void compositorReady();
private:
    DFL::Login1* m_login=nullptr;
    Compositor* m_compositor=nullptr;
    AI* m_ai=nullptr;
    RShell* m_rshell=nullptr;
    RShow* m_rshow=nullptr;
private slots:
    void session(const QString& command);
};
#endif