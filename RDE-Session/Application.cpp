#include "Application.h"
Application::Application(int &argc,char **argv):QCoreApplication(argc,argv)
{
    setOrganizationName("rde");
    setApplicationName("session");

    m_login=new DFL::Login1(this);
    QDBusConnection::sessionBus().connect(QString(),QString(),"org.rde.rshell","Session",this,SLOT(session(QString)));

    qputenv("QT_QPA_PLATFORM","wayland");
    qputenv("QT_QPA_PLATFORMTHEME","rde");
    qputenv("XDG_CURRENT_DESKTOP","Wayfire");

    m_ai=new AI(this);
    QSettings settings;
    m_compositor=new Compositor(settings.value("compositor","rway").toString(),this);
}
void Application::compositorReady()
{
    qputenv("DISPLAY",":0");
    qputenv("WAYLAND_DISPLAY","wayland-1");

    m_rshell=new RShell(this);
    m_rshow=new RShow(this);
}
void Application::session(const QString &command)
{
    if(command=="hibernate"){
        m_login->request("Hibernate");
    }
    else if(command=="lockscreen"){
    }
    else if(command=="logout"){
    }
    else if(command=="reboot"){
        m_login->schedulePowerEventIn("reboot",1*1000*1000);
    }
    else if(command=="shutdown"){
        m_login->schedulePowerEventIn("poweroff",1*1000*1000);
    }
    else if(command=="suspend"){
        m_login->request("Suspend");
    }
    else if(command=="switchuser"){
    }
}