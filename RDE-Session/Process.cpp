#include "Process.h"
Process::Process(const QString& program,QObject* parent):QProcess(parent)
{
    QString path=QStandardPaths::findExecutable(program);
    if(!path.isEmpty()){
        setProgram(path);
        connect(this,&QProcess::stateChanged,[=](ProcessState state){
            switch(state){
            case NotRunning:break;
            case Starting:break;
            case Running:break;
            }
        });
        connect(this,&QProcess::errorOccurred,[=](ProcessError error){
            switch(error){
            case FailedToStart:break;
            case Crashed:break;
            case Timedout:break;
            case ReadError:break;
            case WriteError:break;
            case UnknownError:break;
            }
        });
        connect(this,&QProcess::started,[=](){
        });
        connect(this,&QProcess::finished,[=](int exitCode,QProcess::ExitStatus status){
            switch(status){
            case QProcess::NormalExit:{

                qDebug() << program << "exited with code" << exitCode;

                if(exitCode==255){
                    //if(isOpen())close();
                    //if(open(QIODevice::ReadOnly));
                }
                break;
            }
            case QProcess::CrashExit:
                qWarning() << program << "crashed with code" << exitCode;
                //if(isOpen())close();
                //if(open(QIODevice::ReadOnly));
                break;
            }
        });
        if(open(QIODevice::ReadOnly)){
        }
    }
}