#define Service "org.rde.settings"
#define Path "/org/rde/settings"
#include <QDBusInterface>
#include "Application.h"
int main(int argc,char *argv[])
{
    if(QDBusConnection::systemBus().isConnected()){
        if(QDBusConnection::sessionBus().registerService(Service)){
            Application app(argc,argv);            
            return app.exec();
        }else return 2;
    }else return 1;
}