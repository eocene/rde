#ifndef JOURNALMODEL_H
#define JOURNALMODEL_H
#include <QAbstractTableModel>
#include <QQmlEngine>
#include <QProcess>
#include <QStandardPaths>
class JournalModel:public QAbstractTableModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit JournalModel();
private:
    int rowCount(const QModelIndex & = QModelIndex()) const override{return m_data.size();}
    int columnCount(const QModelIndex & = QModelIndex()) const override{return m_columns.size();}
    QVariant headerData(int section,Qt::Orientation orientation,int role=Qt::DisplayRole) const override;
    QVariant data(const QModelIndex &index,int role) const override;
    QHash<int, QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QAbstractTableModel::roleNames();
        h[Qt::TextAlignmentRole]="textAlignment";
        h[Qt::UserRole]="type";
        return h;
    }
    QStringList m_columns;
    QProcess* m_process=nullptr;
    QList<QStringList> m_data;
};
#endif