#include "JournalModel.h"
JournalModel::JournalModel()
{
    m_columns << "Date" << "Time" << "System" << "Source" << "Message";
    QString journalctl=QStandardPaths::findExecutable("journalctl");
    if(!journalctl.isEmpty()){
        m_process=new QProcess(this);
        m_process->setProgram(journalctl);
        m_process->setArguments(QStringList("--no-pager"));
        connect(m_process,&QProcess::readyReadStandardOutput,[=](){
            while(m_process->canReadLine()){
                QString line=m_process->readLine();
                if(!line.isEmpty()){                    
                    QStringList l=line.split(" ");
                    if(l.size()==6){
                        QStringList entry;
                        entry << l.first()+" "+l.at(1);
                        entry << l.at(2);
                        entry << l.at(3);
                        entry << l.at(4);
                        entry << l.at(5);
                        m_data << entry;
                    }
                }
            }
        });
        connect(m_process,&QProcess::started,[=](){
            beginResetModel();
        });
        connect(m_process,&QProcess::finished,[=](int exitCode,QProcess::ExitStatus status){
            endResetModel();
        });
        if(m_process->open(QIODevice::ReadOnly)){
        }
    }
}
QVariant JournalModel::headerData(int section,Qt::Orientation orientation,int role) const
{
    return m_columns.at(section);
}
QVariant JournalModel::data(const QModelIndex &index,int role) const
{
    switch(role){
    case Qt::DisplayRole:{return QVariant(m_data.at(index.row()).at(index.column()));}
    //case Qt::DecorationRole:return QVariant("image://Icon/data-error");
    case Qt::DecorationRole:return QVariant("data-error");
    case Qt::TextAlignmentRole:{
            if(index.column()==0){return Qt::AlignHCenter;}
            else if(index.column()==1){return Qt::AlignHCenter;}
            else if(index.column()==2){return Qt::AlignHCenter;}
    }
    case Qt::UserRole:{
        return "Text";
    }
    }
}