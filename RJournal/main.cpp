#include <QDBusInterface>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
int main(int argc,char *argv[])
{
    if(QDBusConnection::systemBus().isConnected()){
        if(QDBusConnection::sessionBus().registerService("org.rde.rjournal")){
            QGuiApplication app(argc, argv);
            QQmlApplicationEngine engine("qrc:/rjournal/Window.qml");
            return app.exec();
        } else return 2;
    } else return 1;
}