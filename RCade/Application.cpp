#include "Application.h"
Application::Application(int &argc,char **argv):QGuiApplication(argc,argv)
{
    setOrganizationName("rde");
    setApplicationName("rcade");
    setApplicationDisplayName("RCade");

    m_engine=new QQmlApplicationEngine(this);    
    m_engine->addImageProvider("GameLogo",new GameLogo);
    m_engine->addImageProvider("GameIcon",new GameIcon);
    m_engine->addImageProvider("GameArt",new GameArt);
    m_engine->addImageProvider("GameSnap",new GameSnap);
    m_engine->rootContext()->setContextProperty("GameModel",GameModel::instance(this));
    m_engine->load("qrc:/Window.qml");

    Plugin::loadPlugins();

    QSettings settings;
    GameModel::instance()->setFilter(settings.value("filter").toString());
}
bool Application::setCurrentGame(const QString& id)
{
    if(!id.isEmpty()){
        if(m_currentGame && m_currentGame->id()==id)
            return false;
        m_currentGame=new Game(id);
        emit currentGameChanged();
        return true;
    }
    return false;
}
void Application::quit()
{
    qGuiApp->quit();
}
Application::~Application()
{
    QSettings s;
    s.setValue("filter",GameModel::instance()->filter());
}