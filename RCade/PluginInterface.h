#ifndef PLUGININTERFACE_H
#define PLUGININTERFACE_H
#include <QtPlugin>
class PluginInterface
{
public:
    virtual QStringList import(const QString& id)=0;
protected:
    QString stripTitle(QString title)
    {
        if(title.contains("\""))
            title=title.remove("\"");
        if(title.contains(" ("))
            title.truncate(title.indexOf(" ("));
        return title;
    }
};
Q_DECLARE_INTERFACE(PluginInterface,"org.rde.rcade.plugin")
#endif