#ifndef SYSTEM_H
#define SYSTEM_H
#include <QMap>
#include "PluginInterface.h"
class System:public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id CONSTANT)
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString icon READ icon CONSTANT)
    Q_PROPERTY(PluginInterface* interface READ interface CONSTANT)
public:
    System(PluginInterface* interface,const QString& id,const QString& name,const QString& icon);
    PluginInterface* interface(){return m_interface;}
    QString id(){return m_id;}
    QString name(){return m_name;}
    QString icon(){return m_icon;}
    static QMap<QString,System*> s_systems;
private:
    PluginInterface* m_interface=nullptr;
    QString m_id;
    QString m_name;
    QString m_icon;    
};
#endif