import org.rde.ui
RList{
    id:gameList
    anchors.fill:parent
    model:GameModel
    currentIndex:GameModel.currentIndex
    onCurrentIndexChanged:GameModel.currentIndex=currentIndex
    delegate:RListDelegate{
        width:gameList.width
        icon.source:'image://GameIcon/'+decoration
        onPressed:gameList.currentIndex=index
    }
}