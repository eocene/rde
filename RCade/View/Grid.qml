import QtQuick
GridView{
    id:grid
    model:GameModel
    cellWidth:parent.width/12
    cellHeight:parent.height/4
    delegate:delegate
    boundsBehavior:Flickable.StopAtBounds
    clip:true
    currentIndex:GameModel.currentIndex
    onCurrentIndexChanged:GameModel.currentIndex=currentIndex
    Component{
        id:delegate
        Image{
            width:grid.cellWidth
            height:grid.cellHeight
            //anchors.fill:parent
            fillMode:Image.PreserveAspectFit
            source:'image://GameArt/'+decoration
            sourceSize{width:width;height:height}
            asynchronous:true
            mipmap:true

            MouseArea{
                width:parent.paintedWidth
                height:parent.paintedHeight
                onPressed:grid.currentIndex=index
            }
        }

        // Rectangle{
        //     color:'green'
        //     border{width:1;color:'yellow'}
        //     width:grid.cellWidth
        //     height:grid.cellHeight

        //     Image{
        //         anchors.fill:parent
        //         fillMode:Image.PreserveAspectFit
        //         source:'image://GameLogo/'+decoration
        //         sourceSize{width:width;height:height}
        //         asynchronous:true
        //         mipmap:true
        //     }
        // }
    }
}