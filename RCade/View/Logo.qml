import QtQuick
import QtQuick.Effects
Item{
    ListView{
        id:gameList
        //anchors{left:parent.left;top:parent.top;bottom:parent.bottom;right:parent.horizontalCenter}
        anchors{left:parent.left;top:parent.top;bottom:parent.bottom}
        width:parent.width/4
        //width:300
        transform:Rotation{origin.x:gameList.width;origin.y:gameList.height/2;axis{x:0;y:1;z:0}angle:30}
        model:GameModel
        delegate:delegate
        clip:true
        layer.enabled:enabled
        layer.smooth:true
        layer.effect:MultiEffect{
            maskEnabled:true
            maskSpreadAtMin:0.3
            maskSpreadAtMax:1
            maskThresholdMin:0.3
            maskThresholdMax:1
            maskSource:listMask
        }
        currentIndex:GameModel.currentIndex
        onCurrentIndexChanged:GameModel.currentIndex=currentIndex
        snapMode:ListView.SnapToItem
        boundsBehavior:Flickable.StopAtBounds
        highlightFollowsCurrentItem:true
        highlightRangeMode:ListView.StrictlyEnforceRange
        highlightMoveDuration:300
        preferredHighlightBegin:height/5*2
        preferredHighlightEnd:height/5*3
        flickDeceleration:5000
        maximumFlickVelocity:5000
        onCurrentItemChanged:{
            // if(currentItem && currentGame!==currentItem.game){
            //     gameArt.opacity=0
            //     currentGame=currentItem.game
            //     timer.restart()
            //     mediaPanel.clear()
            // }
        }

        Rectangle{
            id:listMask
            layer.enabled:true
            anchors.fill:parent
            border{width:1;color:Qt.rgba(0,0,0,0)}
            visible:false
            gradient:Gradient{
                GradientStop{position:0;color:Qt.rgba(0,0,0,0.1)}
                GradientStop{position:0.4;color:Qt.rgba(0,0,0,0.2)}
                GradientStop{position:0.5;color:Qt.rgba(0,0,0,1)}
                GradientStop{position:0.6;color:Qt.rgba(0,0,0,0.2)}
                GradientStop{position:1;color:Qt.rgba(0,0,0,0.1)}
            }
        }

        Component{
            id:delegate
            Image{
                width:gameList.width
                height:gameList.height/5
                fillMode:Image.PreserveAspectFit
                source:'image://GameLogo/'+decoration
                sourceSize{width:width;height:height}
                asynchronous:true
                mipmap:true
            }
        }
    }
}