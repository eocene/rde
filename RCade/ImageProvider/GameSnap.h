#ifndef GAMESNAP_H
#define GAMESNAP_H
#include <QQuickImageProvider>
//#include "platform/Platform.h"
class GameSnap:public QQuickImageProvider{
public:
    GameSnap():QQuickImageProvider(Pixmap,ForceAsynchronousImageLoading){}
    QPixmap requestPixmap(const QString &id,QSize *size,const QSize &requestedSize) override
    {
        Q_UNUSED(size);
        // QStringList l=id.split("/");
        // QPixmap p(Platform::gamePath+"/"+l.first()+"/snap/"+l.last()+".png","png");
        // if(!p.isNull())
        //     return p.scaled(requestedSize,Qt::KeepAspectRatio,Qt::SmoothTransformation);
        // else
        //     return QIcon::fromTheme("input-gaming").pixmap(requestedSize);

        return QIcon::fromTheme("input-gaming").pixmap(requestedSize);
    }
};
#endif