#ifndef GAMEART_H
#define GAMEART_H
#include <QQuickImageProvider>
#include <QIcon>
//#include "platform/Platform.h"
class GameArt:public QQuickImageProvider{
public:
    GameArt():QQuickImageProvider(Pixmap,ForceAsynchronousImageLoading){}
    QPixmap requestPixmap(const QString& id,QSize* size,const QSize& requestedSize) override
    {
        Q_UNUSED(size);
        QStringList l=id.split("/");
        //QPixmap p(Platform::gamePath+"/"+l.first()+"/art/"+l.last());
        QPixmap p("/data/games/"+l.first()+"/art/"+l.last());
        if(!p.isNull())
            return p.scaled(requestedSize,Qt::KeepAspectRatio,Qt::SmoothTransformation);
        else
            return QIcon::fromTheme("input-gaming").pixmap(requestedSize);
    }
};
#endif