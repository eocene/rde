#ifndef GAMEICON_H
#define GAMEICON_H
#include <QQuickImageProvider>
#include <QIcon>
//#include "platform/Platform.h"
class GameIcon:public QQuickImageProvider{
public:
    GameIcon():QQuickImageProvider(Pixmap,ForceAsynchronousImageLoading){}
    QPixmap requestPixmap(const QString& id,QSize* size,const QSize& requestedSize) override
    {
        Q_UNUSED(size);
        QStringList l=id.split("/");
        //QPixmap p(Platform::gamePath+"/"+l.first()+"/icon/"+l.last());
        QPixmap p("/data/games/"+l.first()+"/icon/"+l.last());
        if(!p.isNull())
            return p.scaled(requestedSize,Qt::KeepAspectRatio,Qt::SmoothTransformation);
        else
            return QIcon::fromTheme("input-gaming").pixmap(requestedSize);
    }
};
#endif