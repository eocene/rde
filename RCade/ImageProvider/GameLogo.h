#ifndef GAMELOGO_H
#define GAMELOGO_H
#include <QQuickImageProvider>
#include <QIcon>
//#include "platform/Platform.h"
class GameLogo:public QQuickImageProvider{
public:
    GameLogo():QQuickImageProvider(Pixmap,ForceAsynchronousImageLoading){}
    QPixmap requestPixmap(const QString& id,QSize* size,const QSize& requestedSize) override
    {
        Q_UNUSED(size);
        QStringList l=id.split("/");
        //// QPixmap p(Platform::gamePath+"/"+l.first()+"/logo/"+l.last()+".png","png");
        QPixmap p("/data/games/"+l.first()+"/logo/"+l.last()+".png","png");
        if(!p.isNull())
            return p.scaled(requestedSize,Qt::KeepAspectRatio,Qt::SmoothTransformation);
        else
            return QIcon::fromTheme("input-gaming").pixmap(requestedSize);

        //return QIcon::fromTheme("input-gaming").pixmap(requestedSize);
    }
};
#endif