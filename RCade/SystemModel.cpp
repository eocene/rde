#include "SystemModel.h"
SystemModel::SystemModel()
{
    qRegisterMetaType<System*>("System");
}
QVariant SystemModel::data(const QModelIndex& index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return System::s_systems.values().at(index.row())->name();
    case Qt::DecorationRole:return System::s_systems.values().at(index.row())->icon();
    case Qt::UserRole:return QVariant::fromValue(System::s_systems.values().at(index.row()));
    }
}