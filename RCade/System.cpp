#include "System.h"
System::System(PluginInterface* interface,const QString& id,const QString& name,const QString& icon)
{
    m_interface=interface;
    m_id=id;
    m_name=name;
    m_icon=icon;
}
QMap<QString,System*> System::s_systems;