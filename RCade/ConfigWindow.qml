import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.rde.ui
import org.rde.rcade
Window{
    color:Qt.rgba(0,0,0,0.5)
    width:640
    height:480
    visible:true
    modality:Qt.WindowModal
    ColumnLayout{
        anchors.fill:parent
        spacing:0
        RList{
            id:list
            Layout.fillWidth:true
            Layout.fillHeight:true
            model:SystemModel{}
            delegate:delegate
            Component{
                id:delegate
                RListDelegate{
                    width:list.width
                    onPressed:list.currentIndex=index
                    CheckBox{
                        anchors{right:parent.right;verticalCenter:parent.verticalCenter}
                        onCheckedChanged:{
                            if(checked)
                                GameModel.import(System.interface,System.id)
                            else
                                GameModel.remove(System.id)
                        }
                    }
                }
            }
        }
        RBarRaised{
            Layout.fillWidth:true
            RProgressBar{
                id:progressBar
                anchors{fill:parent;margins:4}
                indeterminate:true
                visible:GameModel.busy
                //visible:false
                // Connections{
                //     target:GameModel
                //     function onImportStarted(){
                //         progressBar.
                //         progressBar.visible=true
                //     }
                //     function onImportFinished(){
                //         progressBar.visible=false
                //     }
                // }
            }
        }
    }
}