#include "Plugin.h"
Plugin::Plugin(const QString& file):QPluginLoader(file)
{
    QJsonObject meta=metaData();
    m_iid=meta.value("IID").toString();
    QJsonObject data=meta.value("MetaData").toObject();
    if(!data.keys().contains("program") || (!QStandardPaths::findExecutable(data.value("program").toString()).isEmpty())){
        if(load()){
            m_interface=qobject_cast<PluginInterface*>(instance());
            QJsonObject system=data.value("system").toObject();
            foreach(QString key,system.keys()){
                QJsonObject d=system.value(key).toObject();
                System::s_systems.insert(key,new System(m_interface,key,d.value("name").toString(),d.value("icon").toString()));
            }
        }else{
            qWarning() << "Failed to load" << m_iid << errorString();
        }
    }else{
        qDebug() << "Not loading" << data.value("program").toString();
    }
}
void Plugin::loadPlugins()
{
    QDir dir("/usr/lib/rde/rcade","*.so");
    if(dir.isReadable()){
        foreach(QFileInfo info,dir.entryInfoList()){
            Plugin* plugin=new Plugin(info.absoluteFilePath());
            if(plugin->iid().startsWith("org.rde.rcade."))
                s_plugins.append(plugin);
            else
                plugin->deleteLater();
        }
    }
}
Plugin::~Plugin()
{
    delete m_interface;
}
QList<Plugin*> Plugin::s_plugins;