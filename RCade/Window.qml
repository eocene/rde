import QtQuick
import QtQuick.Controls
import QtQuick.Effects
import org.rde.ui
import 'View'
ApplicationWindow{
    id:window
    color:Qt.rgba(0,0,0,0.5)
    visible:true
    visibility:toggleFullScreen.checked?Window.FullScreen:Window.Maximized
    header:RToolBar{
        id:toolBar
        Row{
            anchors.fill:parent
            spacing:0
            RToolButton{action:importSystems}
            RToolButton{action:logoView;autoExclusive:true}
            RToolButton{action:listView;autoExclusive:true}
            RToolButton{action:gridView;autoExclusive:true}
            RSearchBox{
                anchors{top:parent.top;bottom:parent.bottom;margins:4}
                width:400
                input{
                    text:GameModel.filter
                    onAccepted:GameModel.filter=input.text
                }
            }
        }
    }
    Loader{
        id:loader
        anchors.fill:parent
    }
    Action{
        id:toggleFullScreen
        //text:'Toggle Full Screen'
        //icon.name:'view-list-icons'
        shortcut:'F11'
        checkable:true
    }
    Action{
        id:importSystems
        text:'Import systems'
        icon.name:'bookmarks'
        shortcut:'F5'
        onTriggered:{
            configWindow=Qt.createComponent("ConfigWindow.qml").createObject(window)
        }
    }
    Action{
        id:logoView
        text:'Logo View'
        icon.name:'view-list-details'
        //shortcut:'CTRL+L'
        checkable:true
        onTriggered:loader.source='View/Logo.qml'
    }
    Action{
        id:listView
        text:'List View'
        icon.name:'view-list-text'
        //shortcut:'CTRL+B'
        checkable:true
        onTriggered:loader.source='View/List.qml'
    }
    Action{
        id:gridView
        text:'Grid View'
        icon.name:'view-list-icons'
        //shortcut:'CTRL+B'
        checkable:true
        onTriggered:loader.source='View/Grid.qml'
    }
    property ConfigWindow configWindow;
}