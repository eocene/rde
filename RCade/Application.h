#ifndef APPLICATION_H
#define APPLICATION_H
#include "Game.h"
#include <QDir>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSettings>
#include <QSqlDatabase>
#include <QStandardPaths>
#include "Plugin.h"
#include "GameModel.h"
#include "ImageProvider/GameArt.h"
#include "ImageProvider/GameIcon.h"
#include "ImageProvider/GameLogo.h"
#include "ImageProvider/GameSnap.h"
class Application:public QGuiApplication
{
    Q_OBJECT
public:
    explicit Application(int &argc,char **argv);
    ~Application();
    bool setCurrentGame(const QString& id);
public slots:
    Q_SCRIPTABLE void quit();
private:
    Game* m_currentGame=nullptr;
    QQmlApplicationEngine* m_engine=nullptr;
signals:
    void currentGameChanged();
};
#endif