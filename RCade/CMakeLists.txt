cmake_minimum_required(VERSION 3.14)
project(rcade LANGUAGES CXX)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY $ENV{RDE_DIR})
find_package(Qt6 REQUIRED COMPONENTS Gui Qml Quick Concurrent DBus Sql)
add_executable(rcade
    main.cpp
    Application.h Application.cpp
    Game.h Game.cpp
    Plugin.h Plugin.cpp
    PluginInterface.h
    System.h System.cpp
    SystemModel.h SystemModel.cpp
    GameModel.h GameModel.cpp
    ImageProvider/GameArt.h
    ImageProvider/GameIcon.h
    ImageProvider/GameLogo.h
    ImageProvider/GameSnap.h
)
qt_add_resources(rcade "qml"
    FILES
    Window.qml
    ConfigWindow.qml
    View/Grid.qml
    View/List.qml
    View/Logo.qml
)
qt6_add_qml_module(rcade
    URI org.rde.rcade
    VERSION 1.0
)
target_link_libraries(rcade PRIVATE Qt6::Gui Qt6::Qml Qt6::Quick Qt6::Concurrent Qt6::DBus Qt6::Sql)
add_subdirectory(Plugin/mame)
add_subdirectory(Plugin/pc)
add_subdirectory(Plugin/pcsx2)
add_subdirectory(Plugin/retroarch)
add_subdirectory(Plugin/rpcs3)
add_subdirectory(Plugin/steam)
add_subdirectory(Plugin/xemu)