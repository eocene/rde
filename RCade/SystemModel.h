#ifndef SYSTEMMODEL_H
#define SYSTEMMODEL_H
#include "System.h"
#include <QAbstractListModel>
#include <QQmlEngine>
class SystemModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    SystemModel();    
private:
    int rowCount(const QModelIndex &parent)const{Q_UNUSED(parent);return System::s_systems.size();}
    QHash<int,QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QAbstractListModel::roleNames();
        h[Qt::UserRole]="System";
        return h;
    }
    QVariant data(const QModelIndex& index,int role) const override;
};
#endif