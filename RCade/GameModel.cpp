#include "GameModel.h"
GameModel::GameModel(QObject* parent):QAbstractListModel(parent)
{
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));
    if(dir.mkpath(dir.absolutePath())){
        m_db=QSqlDatabase::addDatabase("QSQLITE");
        m_db.setDatabaseName(dir.absolutePath()+"/database");
        m_db.setConnectOptions("QSQLITE_ENABLE_SHARED_CACHE");
        if(m_db.open()){
        }
    }

    m_filterWatcher=new QFutureWatcher<QMap<QString,QString>>(this);
    connect(m_filterWatcher,&QFutureWatcherBase::finished,[=](){
        beginResetModel();
        m_games=m_filterWatcher->result();
        endResetModel();
    });
}
void GameModel::setFilter(const QString& filter)
{
    if(filter!=m_filter && !m_filterWatcher->isRunning()){
        m_filter=filter;
        m_filterWatcher->setFuture(QtConcurrent::run(&GameModel::filterThread,this,filter));
    }
}
extern QMap<QString,QString> GameModel::filterThread(const QString& filter)
{
    QMap<QString,QString> map;
    QString qu;
    foreach(QString table,m_db.tables())qu.append("SELECT \""+table+"\" as \"platform\",name,title FROM "+table+" WHERE title LIKE \"%"+filter+"%\" UNION ");
    qu.truncate(qu.lastIndexOf(" UNION"));
    qu.append(" ORDER BY title COLLATE NOCASE ASC");
    QSqlQuery q(m_db);
    q.setForwardOnly(true);
    if(q.prepare(qu)){
        if(q.exec(qu)){
            while(q.next())
                map.insert(q.value(0).toString()+"/"+q.value(1).toString(),q.value(2).toString());
        }
    }
    q.finish();
    return map;
}
void GameModel::import(PluginInterface* interface,const QString& id)
{
    m_importWatcher=new QFutureWatcher<QStringList>(this);
    connect(m_importWatcher,&QFutureWatcher<QStringList>::started,this,&GameModel::busyChanged);
    connect(m_importWatcher,&QFutureWatcherBase::finished,[=](){
        QStringList result=m_importWatcher->result();
        if(!result.isEmpty()){
            if(m_db.transaction()){
                QSqlQuery q(m_db);
                q.setForwardOnly(true);
                if(q.exec("CREATE TABLE "+id+"(name,title,year,publisher,PRIMARY KEY(name))")){
                    foreach(QString line,result){
                        if(q.exec("INSERT INTO "+id+" VALUES"+line)){}
                    }
                    if(m_db.commit());
                }
                q.finish();
            }
        }
        m_importWatcher->deleteLater();
        m_busy=false;
        emit busyChanged();
    });
    m_busy=true;
    m_importWatcher->setFuture(QtConcurrent::run(&GameModel::importThread,this,interface,id));
}
extern QStringList GameModel::importThread(PluginInterface* interface,const QString& id)
{
    return interface->import(id);
}
void GameModel::remove(const QString& id)
{
    QSqlQuery q(m_db);
    if(m_db.transaction()){
        if(q.exec("DROP TABLE "+id)){
            if(m_db.commit());
        }
    }
    q.finish();
}
QVariant GameModel::data(const QModelIndex& index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return QVariant(m_games.values().at(index.row()));
    case Qt::DecorationRole:return QVariant(m_games.keys().at(index.row()));
    }
}
GameModel* GameModel::instance(QObject* parent){if(!s_instance)s_instance=new GameModel(parent);return s_instance;}
GameModel* GameModel::s_instance;