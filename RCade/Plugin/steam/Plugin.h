#ifndef PLUGIN_H
#define PLUGIN_H
#define GETOWNEDGAMES "https://api.steampowered.com/IPlayerService/GetOwnedGames/v1/"
#define DETAILSURL "https://store.steampowered.com/api/appdetails"
#include "../../PluginInterface.h"
#include <QEventLoop>
#include <QFileInfo>
#include <QDir>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonArray>
#include <QUrlQuery>
#include <QPixmap>
#include <QProcess>
#include <QSettings>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rcade.steam" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
public:
    QStringList import(const QString& id) override
    {
        Q_UNUSED(id)
        QNetworkAccessManager* network=new QNetworkAccessManager;
        QStringList l;
        QSettings settings;
        settings.beginGroup("Steam");
        QString key=settings.value("key").toString();
        QString aid=settings.value("id").toString();
        settings.endGroup();
        if(!key.isEmpty()){
            if(!aid.isEmpty()){
                QUrlQuery query;
                QUrl url(GETOWNEDGAMES);
                query.addQueryItem("key",key);
                query.addQueryItem("steamid",aid);
                query.addQueryItem("include_appinfo","1");
                query.addQueryItem("include_played_free_games","1");
                query.addQueryItem("skip_unvetted_apps","false");
                url.setQuery(query);
                network->setTransferTimeout(2000);
                QNetworkReply* reply=network->get(QNetworkRequest(url));
                QEventLoop loop;
                QObject::connect(reply,&QNetworkReply::finished,&loop,&QEventLoop::quit);
                loop.exec();
                if(!reply->error()){
                    QJsonDocument doc=QJsonDocument::fromJson(reply->readAll());
                    if(doc.isObject()){
                        QJsonObject obj=doc.object();
                        if(!obj.isEmpty()){
                            QJsonValue response=obj.value("response");
                            if(response.isObject()){
                                QJsonArray array=response.toObject().value("games").toArray();
                                QJsonArray::const_iterator begin=array.constBegin();
                                QJsonArray::const_iterator end=array.constEnd();
                                do{
                                    QJsonValue v=begin.operator *();
                                    if(v.isObject()){
                                        QJsonObject w=v.toObject();
                                        QString i;
                                        QString id=i.setNum(w.value("appid").toInt());
                                        getIcon(network,id,w.value("img_icon_url").toString());
                                        getLogo(network,id);
                                        l << "(\""+id+"\",\""+getTitle(id,w.value("name").toString())+"\",\"\",\"\")";
                                    }
                                    begin++;
                                }
                                while(begin!=end);
                            }
                        }
                    }
                }
                else{
                    qWarning() << "RGame Error:" << reply->errorString();
                }
                reply->deleteLater();
            }else qWarning() << "RGame: invalid steam id";
        }else qWarning() << "RGame: invalid steam key";
        return l;

    }
private:
    inline QString getTitle(const QString& id,QString title)
    {
        QSettings settings("/data/games/steam/data.cfg",QSettings::NativeFormat);
        settings.beginGroup("Title");

        return settings.value(id,title).toString();

        //if(settings.contains(id)){
        //return settings.value(id).toString();
        //}
        //else return title;

        //title=title.remove("™");
        //title=title.remove("®");
        //title.remove("©");
        //return title;
    }
    inline void getIcon(QNetworkAccessManager* network,const QString& id,const QString& icon)
    {
        QFileInfo i("/data/games/steam/icon/"+id+".jpg");
        if(!i.exists()){
            QDir dir;
            if(dir.mkpath("/data/games/steam/icon")){
                QNetworkReply* r=network->get(QNetworkRequest(QUrl("http://media.steampowered.com/steamcommunity/public/images/apps/"+id+"/"+icon+".jpg")));
                QEventLoop l;
                QObject::connect(r,&QNetworkReply::finished,&l,&QEventLoop::quit);
                l.exec();
                if(!r->error()){
                    QPixmap p;
                    p.loadFromData(r->readAll());
                    r->deleteLater();
                    if(p.save(i.absoluteFilePath(),"jpg")){
                    }
                }else qWarning() << "ERROR:" << r->errorString();
            }
        }
    }
    inline void getLogo(QNetworkAccessManager* network,const QString& id)
    {
        QFileInfo i("/data/games/steam/logo/"+id+".png");
        if(!i.exists()){
            QDir dir;
            if(dir.mkpath("/data/games/steam/logo")){
                QNetworkReply* r=network->get(QNetworkRequest(QUrl("https://steamcdn-a.akamaihd.net/steam/apps/"+id+"/logo.png")));
                QEventLoop l;
                QObject::connect(r,&QNetworkReply::finished,&l,&QEventLoop::quit);
                l.exec();
                if(!r->error()){
                    QPixmap p;
                    p.loadFromData(r->readAll());
                    r->deleteLater();
                    if(p.save(i.absoluteFilePath(),"png"))
                        QProcess::execute("mogrify",QStringList("-trim") << i.absoluteFilePath());
                }else qWarning() << "RGame: Error Saving logo:" << r->errorString();
            }
        }
    }
};
#endif