#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
#include <QProcess>
#include <QXmlStreamReader>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rcade.mame" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
public:
    QStringList import(const QString& id) override
    {
        QStringList l,v;
        if(id=="arcade"){
            QProcess m;
            m.setProgram("mame");
            m.setReadChannel(QProcess::StandardOutput);
            m.setArguments(QStringList("-verifyroms"));
            if(m.open(QIODevice::ReadOnly|QIODevice::Text)){
                m.waitForFinished();
                QString output=m.readAllStandardOutput();
                m.close();
                foreach(QString line,output.split("\n")){
                    if(line.endsWith("is good") || line.endsWith("available")){
                        line.remove(0,7);
                        line.truncate(line.indexOf(" "));
                        v << line;
                    }
                }
                if(!v.isEmpty()){
                    QStringList filter;
                    filter << "32x";
                    filter << "32xe";
                    filter << "32xj";
                    filter << "a1200";
                    filter << "a1200n";
                    filter << "a2600";
                    filter << "a2600p";
                    filter << "a500";
                    filter << "a500n";
                    filter << "c64";
                    filter << "c64p";
                    filter << "cd32";
                    filter << "cd32n";
                    filter << "cpc464";
                    filter << "cpc464p";
                    filter << "cpc6128";
                    filter << "cpc6128p";
                    filter << "dendy";
                    filter << "dendy2";
                    filter << "edu64";
                    filter << "famicom";
                    filter << "famicomo";
                    filter << "fds";
                    filter << "fm7";
                    filter << "fm77av";
                    filter << "fm7740sx";
                    filter << "fmtmarty";
                    filter << "fmtowns";
                    filter << "gameboy";
                    filter << "gamegear";
                    filter << "gamegeaj";
                    filter << "gba";
                    filter << "gchinatv";
                    filter << "gbcolor";
                    filter << "genesis";
                    filter << "gen_nomd";
                    filter << "gx4000";
                    filter << "iq501";
                    filter << "iq502";
                    filter << "jaguar";
                    filter << "lynx";
                    filter << "megadrij";
                    filter << "megadriv";
                    filter << "megaduck";
                    filter << "megajet";
                    filter << "megast";
                    filter << "megast_uk";
                    filter << "megacdj";
                    filter << "n64";
                    filter << "nes";
                    filter << "nespal";
                    filter << "ngpc";
                    filter << "pc8801mk2sr";
                    filter << "pc9821ce2";
                    filter << "pce";
                    filter << "psa";
                    filter << "psj";
                    filter << "psu";
                    filter << "saturn";
                    filter << "saturnjp";
                    filter << "sc3000";
                    filter << "sc3000h";
                    filter << "segacd";
                    filter << "sg1000";
                    filter << "sg1000m2";
                    filter << "sg1000m3";
                    filter << "sgx";
                    filter << "sms";
                    filter << "sms2";
                    filter << "snes";
                    filter << "snespal";
                    filter << "sb486";
                    filter << "sc3000";
                    filter << "tg16";
                    filter << "vboy";
                    filter << "x1";
                    filter << "x68000";
                    filter << "x68030";
                    filter << "x68ksupr";
                    filter << "x68kxvi";
                    filter << "ng_mv1";
                    filter << "ng_mv1f";
                    filter << "ng_mv2f";
                    filter << "ng_mv1fz";
                    filter << "ng_mv4f";

                    // filter on device type?

                    QStringListIterator i(filter);
                    while(i.hasNext())
                        v.removeAll(i.next());

                    m.setArguments(QStringList("-lx") << v);
                    if(m.open(QIODevice::ReadOnly|QIODevice::Text)){
                        m.waitForFinished();
                        QXmlStreamReader xml(m.readAllStandardOutput());
                        m.close();
                        if(xml.readNextStartElement()){
                            QString name,title,year,publisher;
                            while(!xml.atEnd()){
                                if(xml.readNextStartElement()){
                                    if(xml.name().toLocal8Bit()=="machine"){
                                        QXmlStreamAttributes attr=xml.attributes();
                                        if(attr.value("ismechanical").toString()=="yes" || attr.value("runnable").toString()=="no" || attr.value("isbios").toString()=="yes" || attr.value("isdevice").toString()=="yes")
                                            xml.skipCurrentElement();
                                        else{
                                            name=attr.value("name").toLocal8Bit();
                                            xml.readNextStartElement();
                                            title=stripTitle(xml.readElementText());
                                            xml.readNextStartElement();
                                            year=xml.readElementText();
                                            xml.readNextStartElement();
                                            publisher=xml.readElementText();
                                            l << "(\""+name+"\",\""+title+"\",\""+year+"\",\""+publisher+"\")";
                                            xml.skipCurrentElement();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return l;
    }
};
#endif