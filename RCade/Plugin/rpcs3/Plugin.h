#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
#include <QDir>
#include <QFile>
#include <QStandardPaths>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rcade.rpcs3" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
public:
    QStringList import(const QString& id) override
    {
        QStringList list;
        QFile file(QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).first()+"/rpcs3/games.yml");
        if(file.open(QIODevice::ReadOnly)){
            QString data=file.readAll();
            file.close();
            foreach(QString line,data.split("\n")){
                QStringList ll=line.split(": ");
                QString id=ll.first();
                QString path=ll.last();
                if(!path.endsWith("PS3_GAME"))
                    path.append("PS3_GAME");
                QDir dir(path);
                if(dir.isReadable()){
                    QFile sfo(dir.absoluteFilePath("PARAM.SFO"));
                    if(sfo.open(QIODevice::ReadOnly)){
                        QByteArray data=sfo.readAll();
                        sfo.close();

                        QByteArray header=data.left(20);
                        int keyTableLoc=header.at(8); //8
                        qDebug() << "KEYTABLE:" << keyTableLoc;
                        //title max length=128
                    }
                }
            }
        }
        return list;
    }
};
#endif