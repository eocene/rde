#ifndef GAMEMODEL_H
#define GAMEMODEL_H
#include <QAbstractListModel>
#include <QDir>
#include <QFutureWatcher>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QtConcurrentRun>
#include "PluginInterface.h"
class GameModel:public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY filterChanged)
    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY currentIndexChanged)
    Q_PROPERTY(bool busy READ busy NOTIFY busyChanged)
public:
    static GameModel* instance(QObject* parent=nullptr);
    inline QString filter()const{return m_filter;}
    void setFilter(const QString& filter);
    inline int currentIndex()const{return m_currentIndex;}
    void setCurrentIndex(const int& index){m_currentIndex=index;}
    Q_INVOKABLE void import(PluginInterface* interface,const QString& id);
    Q_INVOKABLE void remove(const QString& id);
    //bool busy(){return m_filterWatcher->isRunning();}
    bool busy(){return m_busy;}
private:
    GameModel(QObject* parent=nullptr);
    int rowCount(const QModelIndex &parent)const{Q_UNUSED(parent);return m_games.size();}
    // QHash<int,QByteArray> roleNames() const override
    // {
    //     QHash<int,QByteArray> h=QAbstractListModel::roleNames();
    //     h[Qt::UserRole]="System";
    //     return h;
    // }
    QVariant data(const QModelIndex& index,int role) const override;
    QString m_filter;
    QFutureWatcher<QMap<QString,QString>>* m_filterWatcher=nullptr;
    QMap<QString,QString> filterThread(const QString& filter);
    QMap<QString,QString> m_games;
    QFutureWatcher<QStringList>* m_importWatcher=nullptr;
    QStringList importThread(PluginInterface* interface,const QString& id);
    QSqlDatabase m_db;
    static GameModel* s_instance;
    bool m_busy=false;
    int m_currentIndex=0;
signals:
    void filterChanged();
    void busyChanged();
    void currentIndexChanged();
};
#endif