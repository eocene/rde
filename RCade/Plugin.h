#ifndef PLUGIN_H
#define PLUGIN_H
#include <QDir>
#include <QPluginLoader>
#include <QStandardPaths>
#include "PluginInterface.h"
#include "System.h"
class Plugin:public QPluginLoader
{
    Q_OBJECT
public:
    Plugin(const QString &file);
    ~Plugin();
    QString iid()const{return m_iid;}
    QString description()const{return m_description;}    
    QString icon()const{return m_icon;}
    static QList<Plugin*> s_plugins;
    static void loadPlugins();
private:
    PluginInterface* m_interface=nullptr;
    QString m_iid;    
    QString m_description;
    QString m_icon;
};
#endif