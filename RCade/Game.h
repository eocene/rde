#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QDebug>

//#include "platform/PlatformFactory.h"
//class Game:public QObject
class Game: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id CONSTANT)
    //Q_PROPERTY(QString title READ title CONSTANT)
    //Q_PROPERTY(QString year READ year CONSTANT)
    //Q_PROPERTY(QString publisher READ publisher CONSTANT)
    //Q_PROPERTY(QString platformIcon READ platformIcon CONSTANT)
    //Q_PROPERTY(QString art READ art CONSTANT)
    //Q_PROPERTY(QString icon READ icon CONSTANT)
    //Q_PROPERTY(QString logo READ logo CONSTANT)
    //Q_PROPERTY(QString snap READ snap CONSTANT)
    //Q_PROPERTY(QString review READ review WRITE setReview NOTIFY reviewChanged)
    //Q_PROPERTY(QString video READ video CONSTANT)
public:
    Game(const QString& id);
    inline QString id(){return m_id;}
    //inline QString title(){return m_title;}
    //inline QString year(){return m_year;}
    //inline QString publisher(){return m_publisher;}
    //inline Platform* platform(){return m_platform;}
    //inline QString platformIcon(){if(m_platform)return m_platform->name();return "Unknown";}
    //inline QString icon(){return "image://GameIcon/"+m_id;}
    //inline QString art(){return "image://GameArt/"+m_id;}
    //inline QString logo(){return "image://GameLogo/"+m_id;}
    //inline QString snap(){return "image://GameSnap/"+m_id;}
    //     inline QString video(){
    //         if(m_platform)
    //             return m_platform->video(m_name);
    //         return 0;
    //     }
    //     inline QString review(){
    //         if(m_platform){
    //             QFile fp(Platform::gamePath+"/"+m_platform->name()+"/review/"+m_name+".txt");
    //             if(fp.exists() && fp.open(QIODevice::ReadOnly|QIODevice::Text)){
    //                 QString r=fp.readAll();
    //                 fp.close();
    //                 return r;
    //             }
    //         }
    //         return 0;
    //     }
    //     inline void setReview(const QString& text){
    //         if(m_platform && !text.isEmpty()){
    //             QFile fp(Platform::gamePath+"/"+m_platform->name()+"/review/"+m_name+".txt");
    //             if(fp.open(QIODevice::WriteOnly|QIODevice::Text)){
    //                 fp.write(text.trimmed().toUtf8());
    //                 fp.close();
    //             }
    //         }
    //     }
    //     inline Session* session(){
    //         if(m_platform)
    //             return m_platform->session(m_name);
    //         return nullptr;
    //     }
    //     void close();
private:
    QString m_id;
    //     QString m_name;
    //     QString m_title;
    //     QString m_year;
    //     QString m_publisher;
    //     Platform* m_platform=nullptr;
    // signals:
    //     void reviewChanged();
};
#endif