#ifndef PLUGIN_H
#define PLUGIN_H
#include <QDir>
#include <QPluginLoader>
#include <QResource>
#include "PluginInterface.h"
#include "Panel.h"
class Plugin:public QPluginLoader
{
    Q_OBJECT
public:
    Plugin(const QString& file);
    ~Plugin();
    QString iid()const{return m_iid;}
    QString name()const{return m_name;}
    QString description()const{return m_description;}    
    QString icon()const{return m_icon;}
    void enable();
    void disable();
    void save();
    static void loadPlugins();
    static QList<Plugin*> s_plugins;
private:
    QObject* m_instance=nullptr;
    PluginInterface* m_interface=nullptr;
    QString m_iid;
    QString m_name;
    QString m_description;
    QString m_icon;
};
#endif