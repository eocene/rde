import QtQuick
import org.rde.ui
RShellPanel{    
    property Session session
    Row{
        anchors.fill:parent
        RButton{
            icon.name:'system-shutdown'
            checkable:true
            onToggled:{
                if(checked)
                    session=Qt.createComponent('Session.qml').createObject()
                else
                    session.close()
            }
        }
        RButton{
            icon.name:'configure'
            checkable:true
            onToggled:RShell.openRConf(checked)
        }
        RButton{
            icon.name:'preferences-desktop-screensaver'
            checkable:true
            onToggled:RShell.openRShow(checked)
        }
        RButton{
            icon.name:'utilities-log-viewer'
            checkable:true
            onToggled:RShell.openRJournal(checked)
        }
    }
}