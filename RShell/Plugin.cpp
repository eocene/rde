#include "Plugin.h"
Plugin::Plugin(const QString& file):QPluginLoader(file)
{
    QJsonObject meta=metaData();
    m_iid=meta.value("IID").toString();
    QJsonObject data=meta.value("MetaData").toObject();
    m_name=data.value("Name").toString();
    m_description=data.value("Description").toString();
    m_icon=data.value("Icon").toString();
}
void Plugin::enable()
{
    if(load()){
        m_instance=instance();
        if(m_instance){
            m_instance->setParent(this);
            if(QDBusConnection::sessionBus().registerObject("/"+m_name,m_instance,QDBusConnection::ExportScriptableSlots)){
                m_interface=qobject_cast<PluginInterface*>(m_instance);
                if(m_interface){
                    m_interface->setContext(new QQmlContext(Panel::instance()->engine()->rootContext(),m_instance));                    
                    m_interface->context()->setContextProperty("Plugin",m_instance);
                    QSettings settings;
                    settings.beginGroup(m_iid);
                    m_interface->load(&settings);
                    QResource panel("/"+m_iid+"/Panel.qml");
                    if(panel.isValid())
                        Panel::instance()->addItem("qrc"+panel.absoluteFilePath(),m_interface->context());
                }else qWarning() << "RShell: Failed to create plugin" << m_iid;
            }else qWarning() << "RShell: Failed to register plugin" << m_iid << "to DBUS";
        }else qWarning() << "RShell: Failed to create object for plugin" << m_iid;
    }else qWarning() << "RShell: Failed to load plugin" << m_iid << ":" << errorString();
}
void Plugin::disable()
{
    // Panel::instance()->removeItem(m_iid);
    // save();
    // if(unload()){
    //     m_interface=nullptr;
    //     m_instance=nullptr;
    //     QDBusConnection::sessionBus().unregisterObject("/"+m_iid);
    // }
}
void Plugin::save()
{
    QSettings settings;
    settings.beginGroup(m_iid);
    m_interface->save(&settings);
}
Plugin::~Plugin()
{
}
void Plugin::loadPlugins()
{
    //qRegisterMetaType<PluginInterface*>("PluginInterface");
    QSettings settings;
    settings.beginGroup("Core");
    QDir dir("/usr/lib/rde/rshell","*.so");
    if(dir.isReadable()){
        QStringList list=settings.value("plugins").toStringList();
        foreach(QFileInfo info,dir.entryInfoList()){
            Plugin* plugin=new Plugin(info.absoluteFilePath());
            if(plugin->iid().startsWith("org.rde.rshell.")){
                Plugin::s_plugins.append(plugin);
                if(list.contains(plugin->name()))
                    plugin->enable();
            }else
                plugin->deleteLater();
        }
    } else qCritical() << "Plugin directory" << dir.absolutePath() << "is not readable!";
}
QList<Plugin*> Plugin::s_plugins;