#include "Idle.h"
Idle::Idle(QObject *parent):QObject{parent}
{
    m_timeOut=3600000;

    m_idleNotifier=Window::registry->idleNotifier();
    m_idleNotifier->setParent(this);
    if(Window::registry->waitForInterface(WQt::Registry::IdleInterface)){
        m_idleNotification=m_idleNotifier->createNotification(m_timeOut,Window::registry->waylandSeat());
        m_idleNotification->setParent(this);
        connect(m_idleNotification,&WQt::IdleNotification::idled,[=](){
            QDBusMessage m=QDBusMessage::createSignal("/","org.rde.rshell","Idle");
            //m << message;
            QDBusConnection::sessionBus().send(m);
        });
        m_idleNotification->setup();
    }
}