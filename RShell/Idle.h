#ifndef IDLE_H
#define IDLE_H
#include <QDBusConnection>
#include <QDBusMessage>
#include <wayqt/Idle.hpp>
//#include "Window.h"
class Idle:public QObject
{
    Q_OBJECT
public:
    explicit Idle(QObject* parent);
private:
    WQt::IdleNotifier* m_idleNotifier=nullptr;
    WQt::IdleNotification* m_idleNotification=nullptr;
    int m_timeOut=0;
};
#endif