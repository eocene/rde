#ifndef PANEL_H
#define PANEL_H
#include "RShellPanelItem.h"
#include <QQuickWindow>
#include <QQmlEngine>
#include <LayerShellQt/Shell>
#include <LayerShellQt/Window>
#include <QDebug>
class Panel:public QQuickWindow
{
    Q_OBJECT
public:
    static Panel* instance();
    QQmlEngine* engine(){return m_engine;}
    void addItem(const QString& iid,QQmlContext* context);
    void removeItem(const QString& iid);
private:
    Panel();
    static Panel* s_instance;
    QQmlEngine* m_engine=nullptr;
    QString m_iid;
    QQmlContext* m_context=nullptr;
    QMap<QString,RShellPanelItem*> m_items;
    QQmlComponent* m_component=nullptr;
    LayerShellQt::Window* m_layer=nullptr;
};
#endif