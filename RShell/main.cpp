#define Service "org.rde.rshell"
#include "Application.h"
#include <QDBusInterface>
void logger(QtMsgType type,const QMessageLogContext &,const QString & msg)
{
    QString txt;
    switch(type){
    case QtDebugMsg:
        txt=QString("Debug: %1").arg(msg);
        break;
    case QtWarningMsg:
        txt=QString("Warning: %1").arg(msg);
        break;
    case QtCriticalMsg:
        txt=QString("Critical: %1").arg(msg);
        break;
    case QtFatalMsg:
        txt=QString("Fatal: %1").arg(msg);
        abort();
    }
    QFile outFile("/data/log/rshell-outs.log");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << txt << Qt::endl;
}
int main(int argc,char *argv[])
{
    //qInstallMessageHandler(logger);
    if(QDBusConnection::systemBus().isConnected()){
        if(QDBusConnection::sessionBus().registerService(Service)){
            Application app=Application(argc,argv);
            if(QDBusConnection::sessionBus().registerObject("/RShell",&app,QDBusConnection::ExportScriptableSlots)){
                return app.exec();
            }else return 3;
        }else{
            QDBusInterface i(Service,"/RShell","",QDBusConnection::sessionBus());
            if(i.isValid())
                i.call("quit");
            return 2;
        }
    }else return 1;
}