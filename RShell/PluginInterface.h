#ifndef PLUGININTERFACE_H
#define PLUGININTERFACE_H
#include <QtPlugin>
#include <QQmlContext>
#include <QSettings>
#include <QDBusInterface>
class PluginInterface
{
public:
    virtual void load(QSettings* settings){}
    virtual void save(QSettings* settings){}
    QQmlContext* context(){return m_context;}
    void setContext(QQmlContext* context){m_context=context;}
    static void say(const QString& text){
        if(!text.isEmpty()){
            QDBusInterface tts("org.rde.ai","/org/rde/ai","org.rde.ai.tts",QDBusConnection::sessionBus());
            if(tts.isValid()){
                tts.asyncCall("speak",text);
            }
        }
    }
private:
    QQmlContext* m_context=nullptr;
};
Q_DECLARE_INTERFACE(PluginInterface,"org.rde.rshell.plugin")
#endif