#include "Application.h"
Application::Application(int &argc,char **argv):QGuiApplication(argc,argv)
{
    setOrganizationName("rde");
    setApplicationName("rshell");
    setApplicationDisplayName("RShell");

    //         QDBusInterface iface(ServiceName,"/rshell","",QDBusConnection::sessionBus());
    //         if(iface.isValid()){
    //             //QCoreApplication::setAttribute(Qt::AA_DisableHighDpiScaling); //no effect :(
    //             QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
    //             QDir dir;
    //             if(dir.mkpath(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation))){
    //                 //qputenv("QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT",QByteArrayLiteral("1"));


    //Panel::engine->rootContext()->setContextProperty("RShell",this);



    //m_idle=new Idle(this);

    Panel::instance();

    qunsetenv("QT_WAYLAND_SHELL_INTEGRATION");

    //Panel::engine->rootContext()->setContextProperty("MainPanel",m_panel);
    //Panel::engine->addImageProvider("Icon",new IconProvider);

    Plugin::loadPlugins();
}
void Application::saveSettings()
{
    // QStringList v;
    // foreach(PluginLoader* loader,PluginLoader::pluginLoaders){
    //     if(loader->isLoaded()){
    //         loader->save();
    //         v.append(loader->iid());
    //     }
    // }
    // settings.setValue("plugins",v);
}
void Application::session(const QString& command)
{
    // first, need to close all open windows except rshell ones
    saveSettings();
    // close all open windows
    // QDBusInterface interface("org.DesQ.Wayfire","/org/DesQ/Wayfire","wayland.compositor.output",QDBusConnection::sessionBus());
    // if(interface.isValid()){
    //     QDBusReply<QUintList> outputs=interface.call("QueryOutputIds");
    //     foreach(uint output,outputs.value()){
    //         QDBusReply<QUintList> views=interface.call("QueryOutputViews",output);
    //         foreach(uint view,views.value()){
    //             QDBusInterface i("org.DesQ.Wayfire","/org/DesQ/Wayfire","wayland.compositor.views",QDBusConnection::sessionBus());
    //             QDBusMessage result=i.call("CloseView",view);
    //         }
    //     }
    // }
    QDBusMessage m=QDBusMessage::createSignal("/","org.rde.rshell","Session");
    m << command;
    QDBusConnection::sessionBus().send(m);
}
Application::~Application()
{
}