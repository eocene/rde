#include "Panel.h"
Panel::Panel()
{
    setWidth(screen()->size().width()/8);
    setHeight(screen()->size().height());
    setColor(QColor(0,0,0,128));

    m_layer=LayerShellQt::Window::get(this);
    m_layer->setLayer(LayerShellQt::Window::LayerBottom);
    m_layer->setAnchors(LayerShellQt::Window::AnchorLeft);
    m_layer->setKeyboardInteractivity(LayerShellQt::Window::KeyboardInteractivityNone);
    m_layer->setExclusiveEdge(LayerShellQt::Window::AnchorLeft);
    m_layer->setExclusiveZone(width());
    //m_layer->setScreenConfiguration(LayerShellQt::Window::ScreenFromCompositor); //?
    m_layer->setScreenConfiguration(LayerShellQt::Window::ScreenFromQWindow); //?
    m_layer->setScope("shell"); //?

    m_engine=new QQmlEngine(this);

    show();




    // QScreen* s=nullptr;
    // foreach(QScreen* screen,qGuiApp->screens()){
    //     if(screen->name()=="DP-2")
    //         s=screen;
    // }
    // if(!s)
    //     s=qGuiApp->primaryScreen();

    // setScreen(s);

    m_component=new QQmlComponent(m_engine,this);
    connect(m_component,&QQmlComponent::statusChanged,[=](QQmlComponent::Status status){
        switch(status){
        case QQmlComponent::Null:break;
        case QQmlComponent::Loading:break;
        case QQmlComponent::Ready:{
            RShellPanelItem* i=qobject_cast<RShellPanelItem*>(m_component->create(m_context));
            if(i){
                i->setParent(this);
                i->setWidth(width());
                if(!contentItem()->childItems().isEmpty()){
                    QQuickItem* prevItem=contentItem()->childItems().last();
                    i->setParentItem(contentItem());
                    qvariant_cast<QObject*>(i->property("anchors"))->setProperty("top",prevItem->property("bottom"));
                }else{
                    i->setParentItem(contentItem());
                }
                m_items.insert(m_iid,i);
            }
            break;
        }
        case QQmlComponent::Error:{
            qWarning() << m_component->errors();
            break;
        }
        }
    });
    //addItem("rshell",engine->rootContext());
}
void Panel::addItem(const QString& iid,QQmlContext* context)
{
    // m_iid=iid;
    m_context=context;
    //m_component->loadUrl(QUrl("qrc:/"+iid+"/Panel.qml"),QQmlComponent::PreferSynchronous);
    m_component->loadUrl(QUrl(iid),QQmlComponent::PreferSynchronous);
}
void Panel::removeItem(const QString &iid)
{
    if(!iid.isEmpty() && m_items.contains(iid)){
        delete m_items.value(iid);
        m_items.remove(iid);
    }
}
Panel* Panel::s_instance;
Panel* Panel::instance(){if(!s_instance){LayerShellQt::Shell::useLayerShell();s_instance=new Panel;}return s_instance;}