#ifndef PROGRAM_H
#define PROGRAM_H
#include <QProcess>
#include <QStandardPaths>
class Program:public QProcess
{
    Q_OBJECT
public:
    explicit Program(const QString& program,QObject* parent);
};
#endif