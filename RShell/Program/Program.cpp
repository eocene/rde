#include "Program.h"
Program::Program(const QString& program,QObject* parent):QProcess(parent)
{
    QString p=QStandardPaths::findExecutable(program);
    if(!p.isEmpty()){
        setProgram(p);
        connect(this,&QProcess::stateChanged,[=](QProcess::ProcessState state){
            switch(state){
            case NotRunning:break;
            case Starting:break;
            case Running:break;
            }
        });
    }
}