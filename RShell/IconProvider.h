#ifndef ICONPROVIDER
#define ICONPROVIDER
#include <QQuickImageProvider>
#include <QIcon>
class IconProvider:public QQuickImageProvider
{
public:
    IconProvider():QQuickImageProvider(Pixmap,ForceAsynchronousImageLoading){}
    QPixmap requestPixmap(const QString &id,QSize *size,const QSize &requestedSize)
    {
        Q_UNUSED(size);
        if(!id.isEmpty() && QIcon::hasThemeIcon(id))
            return QIcon::fromTheme(id).pixmap(requestedSize);
        QPixmap p(id);
        if(p.isNull()) return QIcon::fromTheme("system-help").pixmap(requestedSize);
        else return p;
    }
};
#endif