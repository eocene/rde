#ifndef RSHELL_H
#define RSHELL_H
#include "Plugin.h"
#include "IconProvider.h"
//#include "Idle.h"
#include <QDir>
#include <QGuiApplication>
class Application:public QGuiApplication
{
    Q_OBJECT
public:
    explicit Application(int &argc,char **argv);
    ~Application();
    Q_INVOKABLE void session(const QString& command);
private:
    //Idle* m_idle=nullptr;
    QSettings settings;
    Panel* m_panel=nullptr;
    void saveSettings();
};
#endif