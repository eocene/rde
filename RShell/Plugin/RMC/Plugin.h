#ifndef PLUGIN_H
#define PLUGIN_H
#define Path "/org/mpris/MediaPlayer2"
#include "../../PluginInterface.h"
//#include <QDBusConnectionInterface>
#include <QDBusServiceWatcher>
#include <QDBusArgument>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rshell.rmc" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)

    Q_PROPERTY(bool active READ active NOTIFY activeChanged)
    Q_PROPERTY(bool playing READ playing NOTIFY playingChanged)
    Q_PROPERTY(bool canSeek READ canSeek NOTIFY canSeekChanged)
    Q_PROPERTY(bool canPause READ canPause NOTIFY canPauseChanged)
    Q_PROPERTY(bool canPlay READ canPlay NOTIFY canPlayChanged)
    Q_PROPERTY(bool canGoNext READ canGoNext NOTIFY canGoNextChanged)
    Q_PROPERTY(bool canGoPrevious READ canGoPrevious NOTIFY canGoPreviousChanged)
    Q_PROPERTY(bool shuffle READ shuffle NOTIFY shuffleChanged)
    Q_PROPERTY(double volume READ volume WRITE setVolume NOTIFY volumeChanged)
    Q_PROPERTY(double minRate READ minRate NOTIFY minRateChanged)
    Q_PROPERTY(double maxRate READ maxRate NOTIFY maxRateChanged)
    Q_PROPERTY(double rate READ rate WRITE setRate NOTIFY rateChanged)
    Q_PROPERTY(QString artist READ artist NOTIFY artistChanged)
    Q_PROPERTY(QString title READ title NOTIFY titleChanged)
    Q_PROPERTY(QString album READ album NOTIFY albumChanged)
    Q_PROPERTY(QString genre READ genre NOTIFY genreChanged)
    Q_PROPERTY(qlonglong duration READ duration NOTIFY durationChanged)
    Q_PROPERTY(qlonglong position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(QString url READ url NOTIFY urlChanged)
    Q_PROPERTY(bool announce READ announce WRITE setAnnounce NOTIFY announceChanged)
public:
    Plugin();
    ~Plugin();
    void load(QSettings* settings) override;
    void save(QSettings* settings) override;
    Q_INVOKABLE inline void command(const QString& command){m_player->asyncCall(command);}

    double volume()const{return m_volume;}
    void setVolume(const double& volume);
    double rate()const{return m_rate;}
    double minRate()const{return m_minRate;}
    double maxRate()const{return m_maxRate;}
    void setRate(const double& rate);

    bool playing()const{return m_playing;}
    bool canSeek()const{return m_canSeek;}
    bool canPause()const{return m_canPause;}
    bool canPlay()const{return m_canPlay;}
    bool canGoNext()const{return m_canGoNext;}
    bool canGoPrevious()const{return m_canGoPrevious;}
    bool active()const{return(m_player && m_player->isValid());}
    bool shuffle()const{return m_shuffle;}
    void setShuffle(const bool& shuffle){m_shuffle=shuffle;}

    QString url(){return m_url;}
    QString title(){return m_title;}
    QString artist(){return m_artist;}
    QString album(){return m_album;}
    QString genre(){return m_genre;}
    qlonglong position(){return m_position;}
    void setPosition(const qlonglong& position);
    qlonglong duration(){return m_duration;}    
    bool announce(){return m_announce;}
    void setAnnounce(bool announce){m_announce=announce;}
private:
    QDBusServiceWatcher* m_watcher=nullptr;
    QDBusInterface* m_player=nullptr;
    QMap<QString,QDBusInterface*> m_players;

    bool m_shuffle=false;
    bool m_playing=false;
    bool m_canPause=false;
    bool m_canPlay=false;
    bool m_canSeek=false;
    bool m_announce=false;
    bool m_canGoNext=false;
    bool m_canGoPrevious=false;

    QString m_url;
    QString m_title;
    QString m_artist;
    QString m_album;
    QString m_genre;
    double m_volume=0;
    double m_minRate=0;
    double m_maxRate=0;
    double m_rate=0;
    qlonglong m_duration=0;
    qlonglong m_position=0;

private slots:
    void propertiesChanged(const QString& interface,const QVariantMap& properties,QStringList c);
    void seeked(qlonglong position);
signals:
    void activeChanged();
    void playingChanged();
    void volumeChanged();
    void canSeekChanged();
    void canPauseChanged();
    void canPlayChanged();
    void canGoNextChanged();
    void canGoPreviousChanged();
    void artistChanged();
    void titleChanged();
    void albumChanged();
    void genreChanged();
    void durationChanged();
    void positionChanged();
    void minRateChanged();
    void maxRateChanged();
    void rateChanged();
    void announceChanged();
    void urlChanged();
    void shuffleChanged();
};
#endif