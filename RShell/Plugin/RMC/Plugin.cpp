#include "Plugin.h"
Plugin::Plugin()
{
    m_watcher=new QDBusServiceWatcher("org.mpris.MediaPlayer2*",QDBusConnection::sessionBus(),QDBusServiceWatcher::WatchForOwnerChange,this);
    connect(m_watcher,&QDBusServiceWatcher::serviceRegistered,[=](const QString& service){
        m_player=new QDBusInterface(service,Path,"org.mpris.MediaPlayer2.Player",QDBusConnection::sessionBus(),this);
        if(m_player->isValid()){
            emit activeChanged();
            if(QDBusConnection::sessionBus().connect(service,m_player->path(),m_player->interface(),"Seeked",this,SLOT(seeked(qlonglong)))){
                if(QDBusConnection::sessionBus().connect(service,m_player->path(),"org.freedesktop.DBus.Properties","PropertiesChanged",this,SLOT(propertiesChanged(QString,QVariantMap,QStringList)))){

                    m_minRate=m_player->property("MinimumRate").toDouble();

                    qDebug() << "MINRATE:" << m_minRate;

                    emit minRateChanged();

                    m_maxRate=m_player->property("MaximumRate").toDouble();
                    emit maxRateChanged();

                    qDebug() << "MAXRATE:" << m_maxRate;

                    m_volume=m_player->property("Volume").toDouble();

                    qDebug() << "VOLUME:" << m_volume;

                    emit volumeChanged();


                    m_shuffle=m_player->property("Shuffle").toBool();
                    emit shuffleChanged();

                    m_canGoNext=m_player->property("CanGoNext").toBool();
                    emit canGoNextChanged();

                    m_canGoPrevious=m_player->property("CanGoPrevious").toBool();
                    emit canGoPreviousChanged();

                    m_canSeek=m_player->property("CanSeek").toBool();
                    emit canSeekChanged();

                    m_canPlay=m_player->property("CanPlay").toBool();
                    emit canPlayChanged();

                    m_players.insert(service,m_player);
                }
            }
        }

    });
    connect(m_watcher,&QDBusServiceWatcher::serviceUnregistered,[=](const QString& service){
        QDBusInterface* i=m_players.value(service);
        qDebug() << "SHOULD DELETE" << i;
        m_players.remove(service);
        qDebug() << "PLAYER REMOVED!";

        //m_active=!m_players.isEmpty();
        emit activeChanged();
    });

    // //init
    // QDBusConnectionInterface* interface=QDBusConnection::sessionBus().interface();
    // QDBusReply<QStringList> reply=interface->registeredServiceNames();
    // if(!reply.isValid()){
    //     //     qDebug() << "Error:" << reply.error().message();
    // }
    // const QStringList values=reply.value();
    // for(const QString &name:values)
    //     qDebug() << name;
}
void Plugin::load(QSettings* settings)
{
    m_announce=settings->value("announce",true).toBool();
}
void Plugin::save(QSettings* settings)
{
    settings->setValue("announce",m_announce);
}
void Plugin::setVolume(const double &volume)
{
    if(m_player)
        m_player->setProperty("Volume",volume);
}
void Plugin::setRate(const double& rate)
{
    if(m_player)
        m_player->setProperty("Rate",rate);
}
void Plugin::propertiesChanged(const QString& interface,const QVariantMap& properties,QStringList c)
{
    foreach(QString key,properties.keys()){
        QVariant v=properties.value(key);
        if(key=="PlaybackStatus"){
            QString status=v.toString();
            if(status=="Playing")
                m_playing=true;
            else if(status=="Paused")
                m_playing=false;
            else if(status=="Stopped")
                m_playing=false;
            emit playingChanged();
        }
        else if(key=="CanPause"){
            m_canPause=v.toBool();
            emit canPauseChanged();
        }
        else if(key=="CanPlay"){
            m_canPlay=v.toBool();
            emit canPlayChanged();
        }
        else if(key=="CanSeek"){
            m_canSeek=v.toBool();
            emit canSeekChanged();
        }        
        else if(key=="CanGoNext"){
            m_canGoNext=v.toBool();
            emit canGoNextChanged();
        }
        else if(key=="CanGoPrevious"){
            m_canGoPrevious=v.toBool();
            emit canGoPreviousChanged();
        }
        else if(key=="Volume"){
            m_volume=v.toDouble();
            emit volumeChanged();
        }
        else if(key=="Rate"){
            m_rate=v.toDouble();
            emit rateChanged();
        }
        else if(key=="Shuffle"){
            m_shuffle=v.toBool();
            emit shuffleChanged();
        }
        else if(key=="LoopStatus"){
            QString ls=v.toString();

            qDebug() << ls;
            // do things here
        }
        else if(key=="Metadata"){
            QDBusArgument arg=v.value<QDBusArgument>();
            QVariantMap map;
            arg >> map;
            foreach(QString meta,map.keys()){
                QVariant metaValue=map.value(meta);                
                if(meta=="xesam:url"){
                    m_url=metaValue.toString();
                    emit urlChanged();
                }
                else if(meta=="xesam:artist"){
                    m_artist=metaValue.toString();
                    emit artistChanged();
                }
                else if(meta=="xesam:title"){
                    m_title=metaValue.toString();
                    emit titleChanged();
                }
                else if(meta=="xesam:album"){
                    m_album=metaValue.toString();
                    emit albumChanged();
                }
                else if(meta=="xesam:genre"){
                    m_genre=metaValue.toStringList().first();
                    emit genreChanged();
                }
                else if(meta=="mpris:length"){
                    m_duration=metaValue.toLongLong();
                    emit durationChanged();
                }
                // else{
                //     qDebug() << "META" << meta << "=" << metaValue;
                // }
            }
            // if(m_announce)
            //     say("You are listening to "+m_title+" by "+m_artist);
        }
        else{
            qDebug() << "PROPERTY:" << key << "=" << properties.value(key);
        }
    }
}
void Plugin::seeked(qlonglong position)
{
    m_position=position;
    emit positionChanged();
}
void Plugin::setPosition(const qlonglong& position)
{
    m_player->asyncCall("Seek",position);
}
Plugin::~Plugin()
{
}