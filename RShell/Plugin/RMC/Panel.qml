import QtQuick
import org.rde.ui
RShellPanel{
    height:column.implicitHeight
    Column{
        id:column
        anchors.fill:parent
        Item{anchors{left:parent.left;right:parent.right}height:80;InfoDisplay{anchors{fill:parent;margins:4}}}
        Item{anchors{left:parent.left;right:parent.right}height:20;ProgressSlider{anchors{fill:parent;margins:4}}}
        Item{anchors{left:parent.left;right:parent.right}height:40;MediaControls{anchors.fill:parent}}
        Item{anchors{left:parent.left;right:parent.right}height:40;Dials{anchors.fill:parent}}
    }
    enabled:Plugin.active
}