import QtQuick
import org.rde.ui
Rectangle{
    color:'black'
    radius:8
    //property int ww
    Flickable{
        id:artistFlick
        anchors{left:parent.left;right:parent.right;top:parent.top;bottom:parent.verticalCenter;margins:4}
        contentWidth:artistLabel.implicitWidth
        //boundsMovement:Flickable.StopAtBounds
        interactive:false
        clip:true
        RText{
            id:artistLabel
            anchors.fill:parent
            verticalAlignment:Text.AlignVCenter
            //horizontalAlignment:Text.AlignHCenter
            text:Plugin.artist
            onTextChanged:artistFlick.contentX=0

        }
    }
    Flickable{
        id:titleFlick
        anchors{left:parent.left;right:parent.right;top:parent.verticalCenter;bottom:parent.bottom;margins:4}
        contentWidth:titleLabel.implicitWidth
        boundsMovement:Flickable.StopAtBounds
        interactive:false
        clip:true
        RText{
            id:titleLabel
            anchors.fill:parent
            verticalAlignment:Text.AlignVCenter
            //horizontalAlignment:Text.AlignHCenter
            text:Plugin.title
            onTextChanged:titleFlick.contentX=0
        }
    }
    Connections{
        target:MainPanel
        function onFrameSwapped(){
            if(!artistFlick.atXEnd){artistFlick.contentX++}
            if(!titleFlick.atXEnd){titleFlick.contentX++}
        }
    }
}


/*
import QtQuick
import QtQuick.Controls
import QtQuick.Effects
import org.rde.ui
RInfoDisplay{
    // ProgressBar{
    //     id:progressBar
    //     anchors.fill:parent
    //     indeterminate:visible
    //     visible:false
    // }
    Rectangle{
        id:textMask
        anchors.fill:parent
        layer.enabled:true
        visible:false
        gradient:Gradient{
            orientation:Gradient.Horizontal
            GradientStop{position:0;color:Qt.rgba(0,0,0,0.4)}
            GradientStop{position:0.2;color:Qt.rgba(0,0,0,1)}
            GradientStop{position:0.8;color:Qt.rgba(0,0,0,1)}
            GradientStop{position:1;color:Qt.rgba(0,0,0,0.4)}
        }
    }
    Flickable{
        id:flick
        anchors{fill:parent;margins:4}
        contentWidth:textLabel.implicitWidth
        //boundsMovement:Flickable.StopAtBounds
        boundsBehavior:Flickable.DragAndOvershootBounds
        //        interactive:false
        RText{
            id:textLabel
            anchors.fill:parent
            horizontalAlignment:Text.AlignHCenter
            text:RMC.title
            //opacity:Player.playing?1:0.3

            //Behavior on opacity{NumberAnimation{duration:200}}
            //opacity:0
            // onTextChanged:{
            //     opacity=1
            //     flick.contentX=0
            //     if(textLabel.implicitWidth>flick.width)
            //         timer.start()
            //     else
            //         timer.stop()
            // }
        }
        layer.smooth:true
        layer.enabled:true
        layer.effect:MultiEffect{
            maskEnabled:true
            maskSpreadAtMin:0.5
            //maskSpreadAtMax:1
            maskThresholdMin:0.5
            //maskThresholdMax:1
            maskSource:textMask
            brightness:-0.2
            saturation:-0.3
        }
    }
    // MouseArea{
    //     anchors.fill:parent
    //     //onClicked:RMC.announce()
    // }
}
*/