#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
#include <QRandomGenerator>
#include <QTimer>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rshell.rclock" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
    Q_PROPERTY(QVariant date READ date NOTIFY dateChanged)
    Q_PROPERTY(int hour READ hour NOTIFY hourChanged)
    Q_PROPERTY(int minute READ minute NOTIFY minuteChanged)
public:
    void load(QSettings* settings) override;
public slots:
    void sayTime();
    void sayDate();
private:
    QLocale locale;
    QTimer* m_initial=nullptr;
    QTime m_time;
    int hour()const{return m_time.hour();}
    int minute()const{return m_time.minute();}
    QVariant date()const{return locale.toString(QDate::currentDate(),"ddd d/M");}
    void inline updateTime(){
        m_time=QTime::currentTime();
        if(m_time.minute()==0){
            if(m_time.hour()==0){
                emit dateChanged();
                sayDate();
            }
            else
                sayTime();
            emit hourChanged();
        }
        emit minuteChanged();
    }
    void timerEvent(QTimerEvent *event) override;
    QStringList m_timeSpeech;
    QStringList m_dateSpeech;
signals:
    void dateChanged();
    void hourChanged();
    void minuteChanged();
};
#endif