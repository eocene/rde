import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.rde.ui
ApplicationWindow{
    visible:true
    title:'Calendar'
    color:Qt.rgba(0,0,0,0.5)
    width:640
    height:480
    header:RToolBar{
        id:toolBar
        RToolButton{
            id:prevButton
            anchors{left:parent.left;top:parent.top;bottom:parent.bottom}
            action:prevMonth
        }
        RText2{
            anchors{fill:parent;margins:RShell.margin}
            horizontalAlignment:Text.AlignHCenter
            verticalAlignment:Text.AlignVCenter
            text:monthGrid.title
        }
        RToolButton{
            id:nextButton
            anchors{right:parent.right;top:parent.top;bottom:parent.bottom}
            action:nextMonth
        }
    }
    ColumnLayout{
        anchors.fill:parent
        spacing:0

        RBarRaised{
            Layout.fillWidth:true
            DayOfWeekRow{
                anchors.fill:parent
                locale:monthGrid.locale
                delegate:RText2{
                    text:shortName
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment:Text.AlignVCenter
                    required property string shortName
                }
            }
        }
        MonthGrid{
            id:monthGrid
            Layout.fillWidth:true
            Layout.fillHeight:true
            spacing:0
            delegate:Rectangle{
                //border{width:1;color:BorderColor}
                border{width:1;color:'black'}
                color:model.today?'darkblue':'transparent'
                opacity:model.month===monthGrid.month?1:0
                RText{
                    anchors.fill:parent
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment:Text.AlignVCenter
                    text:model.day
                }
                required property var model
            }
        }
    }
    Action{
        id:prevMonth
        text:'Previous month'
        icon.name:'go-previous-view'
        enabled:monthGrid.month>0
        onTriggered:monthGrid.month--
    }
    Action{
        id:nextMonth
        text:'Next month'
        icon.name:'go-next-view'
        enabled:monthGrid.month<11
        onTriggered:monthGrid.month++
    }
    onClosing:destroy()
}