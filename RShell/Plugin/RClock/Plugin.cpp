#include "Plugin.h"
void Plugin::load(QSettings* settings)
{
    m_timeSpeech << "it's" << "it's now" << "it is" << "it is now" << "the time is" << "the time is now";
    m_dateSpeech << "it's" << "it's now" << "it is" << "it is now" << "today is" << "the date is";

    locale=QLocale(QLocale::Dutch,QLocale::Belgium); // unhardcode
    m_initial=new QTimer(this);
    m_initial->setSingleShot(true);
    m_initial->setInterval((60-QTime::currentTime().second())*1000);
    connect(m_initial,&QTimer::timeout,[=](){
        m_initial->deleteLater();
        updateTime();
        startTimer(60000,Qt::VeryCoarseTimer);
    });
    m_initial->start();
    updateTime();
}
void Plugin::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event)
    updateTime();
}
void Plugin::sayTime()
{
    QString a=m_timeSpeech.at(QRandomGenerator::global()->bounded(m_timeSpeech.size()))+" ";
    QString hour;
    a.append(hour.setNum(m_time.hour()));
    if(m_time.minute()==0){
        a.append(" o clock");
    }else{
        a.append(" hours ");
        a.append(hour.setNum(m_time.minute()));
        a.append(" minutes");
    }
    say(a);
}
void Plugin::sayDate()
{
    say(m_dateSpeech.at(QRandomGenerator::global()->bounded(m_dateSpeech.size()))+" "+QDate::currentDate().toString("dddd MMMM d yyyy"));
}