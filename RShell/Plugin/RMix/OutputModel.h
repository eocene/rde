#ifndef OUTPUTMODEL_H
#define OUTPUTMODEL_H
#include <QAbstractListModel>
#include <QAudioDevice>
#include <QMediaDevices>
#include <QQmlEngine>
class OutputModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit OutputModel();
private:
    int rowCount(const QModelIndex &parent)const override{Q_UNUSED(parent);return m_list.size();}
    QVariant data(const QModelIndex &index,int role) const override;
    QHash<int,QByteArray> roleNames() const override;
    QList<QAudioDevice> m_list;
};
#endif