#include "Plugin.h"
Plugin::Plugin()
{
    QAudioDevice dev=QMediaDevices::defaultAudioOutput();

    //QAudioDevice dev=QMediaDevices::audioOutputs().first();

    //qDebug() << "RMIX USING" << dev.description();


    //qDebug() << dev.preferredFormat();

    QAudioFormat format;
    format.setChannelCount(2);
    format.setSampleRate(44100);
    format.setSampleFormat(QAudioFormat::Int16);


    m_source=new QAudioSource(dev,format,this);
    //QAudioFormat format=m_source->format();


    m_device=m_source->start();
    m_device->setParent(this);
    connect(m_device,&QIODevice::readyRead,[=](){
        QByteArray data=m_device->readAll();
        QAudioBuffer buffer({data,static_cast<int>(data.size())},format);
        const int channelBytes=format.bytesPerSample();
        const int sampleBytes=format.bytesPerFrame();
        const int numSamples=buffer.byteCount()/sampleBytes;
        float maxValue=0;
        auto *ptr=reinterpret_cast<const unsigned char*>(buffer.constData<char>());
        for(int i=0;i<numSamples;++i){
            for(int j=0;j<format.channelCount();++j) {
                float value=format.normalizedSampleValue(ptr);
                maxValue=qMax(value,maxValue);
                ptr+=channelBytes;
            }
        }
        if(maxValue>0){
            m_level=maxValue;
            emit levelChanged();
        }
    });
}
void Plugin::load(QSettings* settings)
{    
}
void Plugin::save(QSettings* settings)
{
}