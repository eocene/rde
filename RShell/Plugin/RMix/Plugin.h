#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
#include <QAudioBuffer>
#include <QAudioSource>
#include <QMediaDevices>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rshell.rmix" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
    Q_PROPERTY(qreal volume READ volume WRITE setVolume NOTIFY volumeChanged)
    Q_PROPERTY(qreal level READ level NOTIFY levelChanged)
public:
    Plugin();
    void load(QSettings* settings);
    void save(QSettings* settings);
private:
    QAudioSource* m_source=nullptr;
    QIODevice* m_device=nullptr;
    inline qreal volume(){return m_source->volume();}
    inline void setVolume(const qreal& volume){m_source->setVolume(volume);}
    qreal m_level;
    inline qreal level(){return m_level;}
signals:
    void levelChanged();
    void volumeChanged();
};
#endif