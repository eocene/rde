#include "InputModel.h"
InputModel::InputModel()
{
    m_list=QMediaDevices::audioInputs();
    QMediaDevices devices;
    connect(&devices,&QMediaDevices::audioInputsChanged,[=](){
        beginResetModel();
        m_list=QMediaDevices::audioInputs();
        endResetModel();
    });
}
QHash<int,QByteArray> InputModel::roleNames()const
{
    QHash<int,QByteArray> h;
    h[Qt::DisplayRole]="Display";
    h[Qt::UserRole]="value";
    return h;
}
QVariant InputModel::data(const QModelIndex &index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return m_list.at(index.row()).description();
    case Qt::UserRole:return QVariant::fromValue(m_list.at(index.row()));
    }
}