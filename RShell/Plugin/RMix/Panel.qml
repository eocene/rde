import QtQuick
import QtQuick.Controls
import org.rde.ui
import org.rde.rshell.rmix
RShellPanel{
    height:200
    Rectangle{
        id:out
        anchors{left:parent.left;top:parent.top;bottom:parent.bottom}
        width:40
        border{width:1;color:Qt.rgba(0,0,0,0.3)}
        color:Qt.rgba(0,0,0,0)
        RSlider{
            id:outVolume
            anchors{left:parent.left;right:parent.horizontalCenter;rightMargin:2;top:parent.top;bottom:parent.bottom;margins:4}
            orientation:Qt.Vertical
            value:Plugin.volume
            onValueChanged:Plugin.volume=value
        }
        RVolumeMeter{
            id:outMeter
            value:Plugin.level
            anchors{left:parent.horizontalCenter;leftMargin:2;right:parent.right;top:parent.top;bottom:parent.bottom;margins:4}
        }
    }
}