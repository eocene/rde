#include "OutputModel.h"
OutputModel::OutputModel()
{
    QMediaDevices devices;    
    connect(&devices,&QMediaDevices::audioOutputsChanged,[=](){
        beginResetModel();
        m_list=QMediaDevices::audioOutputs();
        endResetModel();
    });
    m_list=QMediaDevices::audioOutputs();
}
QHash<int,QByteArray> OutputModel::roleNames()const
{
    QHash<int,QByteArray> h;
    h[Qt::DisplayRole]="Display";
    h[Qt::UserRole]="value";
    return h;
}
QVariant OutputModel::data(const QModelIndex &index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return m_list.at(index.row()).description();
    case Qt::UserRole:return QVariant::fromValue(m_list.at(index.row()));
    }
}