#ifndef INPUTMODEL_H
#define INPUTMODEL_H
#include <QAbstractListModel>
#include <QAudioDevice>
#include <QMediaDevices>
#include <QQmlEngine>
class InputModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit InputModel();
private:
    int rowCount(const QModelIndex &parent)const override{Q_UNUSED(parent);return m_list.size();}
    QVariant data(const QModelIndex &index,int role) const override;
    QHash<int,QByteArray> roleNames() const override;
    QList<QAudioDevice> m_list;
};
#endif