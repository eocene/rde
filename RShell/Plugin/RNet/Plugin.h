#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
#include <QNetworkInformation>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rshell.rnet" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
    Q_PROPERTY(QString status READ status NOTIFY statusChanged)
public:
    void load(QSettings* settings) override;
private:
    QNetworkInformation* m_netInfo=nullptr;
    QString status();
signals:
    void statusChanged();
};
#endif