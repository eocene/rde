#include "Plugin.h"
void Plugin::load(QSettings* settings)
{
    if(QNetworkInformation::loadDefaultBackend()){
        m_netInfo=QNetworkInformation::instance();
        connect(m_netInfo,&QNetworkInformation::reachabilityChanged,[=](QNetworkInformation::Reachability reachability){
            switch(reachability){
            case QNetworkInformation::Reachability::Unknown: qDebug() << "Reachability: Unknown";break;
            case QNetworkInformation::Reachability::Disconnected:
                say("Network connection lost");
                break;
            case QNetworkInformation::Reachability::Local: qDebug() << "Reachability: Local";break;
            case QNetworkInformation::Reachability::Site: qDebug() << "Reachability: Site";break;
            case QNetworkInformation::Reachability::Online:
                say("Network connection established");
                break;
            }
            emit statusChanged();
        });
        connect(m_netInfo,&QNetworkInformation::transportMediumChanged,[=](QNetworkInformation::TransportMedium medium){
            switch(medium){
            case QNetworkInformation::TransportMedium::Unknown: qDebug() << "Medium: Unknown";break;
            case QNetworkInformation::TransportMedium::Ethernet: qDebug() << "Medium: Ethernet";break;
            case QNetworkInformation::TransportMedium::Cellular: qDebug() << "Medium: Cellular";break;
            case QNetworkInformation::TransportMedium::WiFi: qDebug() << "Medium: Wifi";break;
            case QNetworkInformation::TransportMedium::Bluetooth: qDebug() << "Medium: Bluetooth";break;
            }
        });
    }
}
QString Plugin::status()
{
    switch(m_netInfo->reachability()){
    case QNetworkInformation::Reachability::Unknown:return "black";break;
    case QNetworkInformation::Reachability::Disconnected:return "red";break;
    case QNetworkInformation::Reachability::Local:return "yellow";break;
    case QNetworkInformation::Reachability::Site:return "blue";break;
    case QNetworkInformation::Reachability::Online:return "green";break;
    }
}