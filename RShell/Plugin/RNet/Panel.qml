import QtQuick
import org.rde.ui
import org.rde.rshell.rnet
RShellPanel{
    property Window window;
    ListView{
        id:view
        anchors{left:parent.left;right:status.left;top:parent.top;bottom:parent.bottom}
        orientation:Qt.Horizontal
        model:InterfaceModel{}
        delegate:RButton{
            icon.name:Icon
            //source:Icon
            // onPress:{
            //     if(window)
            //         window.destroy()
            //     else
            //         window=Qt.createComponent('Window.qml').createObject()
            // }
        }
    }
    Rectangle{
        id:status
        anchors{top:parent.top;bottom:parent.bottom;right:parent.right;margins:4}
        border{width:1;color:'black'}
        width:height
        radius:8
        color:Plugin.status
    }
}