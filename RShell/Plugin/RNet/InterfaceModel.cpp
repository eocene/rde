#include "InterfaceModel.h"
InterfaceModel::InterfaceModel()
{
    foreach(QNetworkInterface interface,QNetworkInterface::allInterfaces()){
        if(interface.isValid() && interface.type()!=QNetworkInterface::Loopback)
            m_list.append(interface);        
    }
}
QHash<int,QByteArray> InterfaceModel::roleNames() const
{
    QHash<int,QByteArray> h;
    h[Qt::DecorationRole]="Icon";
    return h;
}
QVariant InterfaceModel::data(const QModelIndex &index,int role) const
{
    QNetworkInterface interface=m_list.at(index.row());

    switch(role){
    case Qt::DecorationRole:{
        switch(interface.type()){
        case QNetworkInterface::Unknown:break;
        case QNetworkInterface::Loopback:break;
        case QNetworkInterface::Virtual:break;
        case QNetworkInterface::Ethernet:return QVariant("image://Icon/network-wired");
        case QNetworkInterface::Wifi:{
            if(interface.flags().testFlag(QNetworkInterface::IsRunning)){
                return QVariant("image://Icon/network-wireless");
            }
            else
                return QVariant("image://Icon/network-wireless-disconnected");
        }
        case QNetworkInterface::CanBus:break;
        case QNetworkInterface::Fddi:break;
        case QNetworkInterface::Ppp:break;
        case QNetworkInterface::Slip:break;
        case QNetworkInterface::Phonet:break;
        case QNetworkInterface::Ieee802154:break;
        case QNetworkInterface::SixLoWPAN:break;
        case QNetworkInterface::Ieee80216:break;
        case QNetworkInterface::Ieee1394:break;
        }
    }
    }
}