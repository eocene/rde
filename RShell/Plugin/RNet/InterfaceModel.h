#ifndef INTERFACEMODEL_H
#define INTERFACEMODEL_H
#include <QAbstractListModel>
#include <QQmlEngine>
#include <QNetworkInterface>
class InterfaceModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit InterfaceModel();
private:
    int rowCount(const QModelIndex &parent)const{Q_UNUSED(parent);return m_list.count();}
    QVariant data(const QModelIndex &index,int role) const;
    QHash<int,QByteArray> roleNames() const;
    QList<QNetworkInterface> m_list;
};
#endif