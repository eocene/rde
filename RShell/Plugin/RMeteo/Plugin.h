#ifndef PLUGIN_H
#define PLUGIN_H
#define WEATHERURL "https://api.open-meteo.com/v1/forecast"
#include "../../PluginInterface.h"
#include <QGeoPositionInfo>
#include <QGeoPositionInfoSource>
#include <QJsonDocument>
#include <QQmlEngine>
#include <QNetworkReply>
#include <QUrlQuery>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rshell.rmeteo" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
    Q_PROPERTY(QString weatherIcon READ weatherIcon NOTIFY weatherIconChanged)
    Q_PROPERTY(double temperature READ temperature NOTIFY temperatureChanged)
public:
    void load(QSettings* settings) override;
public slots:
    void announce();
private:
    QMap<double,QString> m_icons;
    QMap<double,QString> m_speech;
    QNetworkRequest m_request;
    QGeoPositionInfoSource* m_infoSource;
    QString m_weatherIcon;
    QString weatherIcon()const{return m_weatherIcon;}
    double temperature(){return m_temperature;}
    double m_temperature=0;
    void queryWeather();
    void timerEvent(QTimerEvent *event)override{queryWeather();}
    QString m_longitude;
    QString m_latitude;
private slots:
    void onReplyReceived(QNetworkReply* reply);
signals:    
    void weatherIconChanged();
    void temperatureChanged();
};
#endif