#include "Plugin.h"
void Plugin::load(QSettings* settings)
{
    m_icons.insert(0,"weather-clear");
    m_icons.insert(1,"weather-few-clouds"); // mainly clear
    m_icons.insert(2,"weather-many-clouds"); // partly cloudy
    m_icons.insert(3,"weather-overcast");
    m_icons.insert(45,"weather-fog"); // fog
    m_icons.insert(48,"weather-fog"); // depositing rime fog (?)
    m_icons.insert(51,"weather-showers-scattered"); // light drizzle
    m_icons.insert(53,""); // mid drizzle
    m_icons.insert(55,""); // heavy drizzle
    m_icons.insert(56,""); // light freezing drizzle
    m_icons.insert(57,""); // dense freezing drizzle
    m_icons.insert(61,"weather-showers-scattered"); // slight rain
    m_icons.insert(63,""); // mid rain
    m_icons.insert(65,""); // heavy rain
    m_icons.insert(66,"weather-freezing-rain"); // light freezing rain
    m_icons.insert(67,"weather-freezing-rain"); // heavy freezing rain
    m_icons.insert(71,"weather-snow-scattered"); // snow lo
    m_icons.insert(73,"weather-snow"); // snow mid
    m_icons.insert(75,""); // snow heavy
    m_icons.insert(77,"weather-snow");  // snow grains
    m_icons.insert(80,"weather-showers-scattered"); // slight rain shower
    m_icons.insert(81,""); // moderate rain shower
    m_icons.insert(82,""); // violent rain shower
    m_icons.insert(85,""); // slight snow shower
    m_icons.insert(86,""); // heavy snow shower
    m_icons.insert(95,""); // thunderstorm slight *moderate
    m_icons.insert(96,""); //Thunderstorm with slight hail
    m_icons.insert(99,""); //Thunders   torm with heavy hail

    m_speech.insert(0,"the weather is clear");
    m_speech.insert(1,"it's a little bit cloudy");
    m_speech.insert(2,"it's pretty cloudy");
    m_speech.insert(3,"it's overcast");
    m_speech.insert(45,"it's foggy");
    m_speech.insert(48,"it's foggy");
    m_speech.insert(51,"there's a bit of rain every now and then");
    m_speech.insert(53,"mid drizzle");
    m_speech.insert(55,"heavy drizzle");
    m_speech.insert(56,"light freezing drizzle");
    m_speech.insert(57,"dense freezing drizzle");
    m_speech.insert(61,"it's rainy");
    m_speech.insert(63,"it's raining");
    m_speech.insert(65,"it's raining heavily");
    m_speech.insert(66,"it's freezing and there's rain");
    m_speech.insert(67,"it's freezing and there's heavy rain");
    m_speech.insert(71,"it's snowing a little bit");
    m_speech.insert(73,"it's snowing");
    m_speech.insert(75,"it's snowing heavily");
    m_speech.insert(77,"it's snowing lightly");
    m_speech.insert(80,"there's a little bit of rain");
    m_speech.insert(81,"it's raining");
    m_speech.insert(82,"it's raining heavily");
    m_speech.insert(85,"there's a tiny little bit of snow");
    m_speech.insert(86,"there's a heavy snow shower");
    m_speech.insert(95,"there's a thunderstorm");
    m_speech.insert(96,"there's a thunderstorm with some hail");
    m_speech.insert(99,"there's a thunderstorm with heavy hail");

    m_infoSource=QGeoPositionInfoSource::createDefaultSource(this);
    if(m_infoSource){

        m_infoSource->setUpdateInterval(100000);

        connect(m_infoSource,&QGeoPositionInfoSource::positionUpdated,[=](const QGeoPositionInfo &info){
            qDebug() << "GEOPOSITION UPDATED!";
        });
        connect(m_infoSource,&QGeoPositionInfoSource::errorOccurred,[=](QGeoPositionInfoSource::Error error){
            switch(error){
            case QGeoPositionInfoSource::AccessError:qWarning() << "RMeteo: Access Error";break;
            case QGeoPositionInfoSource::ClosedError:qWarning() << "RMeteo: Closed Error";break;
            case QGeoPositionInfoSource::NoError:qWarning() << "RMeteo: No Error";break;
            case QGeoPositionInfoSource::UnknownSourceError:qWarning() << "RMeteo: Unknown Source Error";break;
            case QGeoPositionInfoSource::UpdateTimeoutError:qWarning() << "RMeteo: Update Timeout Error";break;
            }
        });
        m_infoSource->startUpdates();
    }

    m_latitude=settings->value("latitude","0").toString();
    m_longitude=settings->value("longitude","0").toString();

    QUrlQuery query;
    query.addQueryItem("latitude",m_latitude);
    query.addQueryItem("longitude",m_longitude);
    query.addQueryItem("current_weather","true");
    query.addQueryItem("hourly","temperature_2m");

    QUrl url(WEATHERURL);
    url.setQuery(query);

    m_request=QNetworkRequest(url);

    startTimer(settings->value("update_interval","3600000").toInt(),Qt::VeryCoarseTimer);
    queryWeather();
}
void Plugin::queryWeather()
{
    QNetworkReply* r=context()->engine()->networkAccessManager()->get(m_request);
    connect(r,&QNetworkReply::finished,this,[this,r](){onReplyReceived(r);});
}
void Plugin::onReplyReceived(QNetworkReply* reply)
{
    if(!reply){
        qWarning() << "RMeteo: Got empty reply from openmeteo";
        return;
    }
    if(!reply->error()){
        QJsonDocument doc=QJsonDocument::fromJson(reply->readAll());
        QJsonObject obj=doc.object();
        QJsonValue cw=obj.value(QStringLiteral("current_weather"));
        if(cw.isObject()){
            QJsonObject o=cw.toObject();
            if(o.contains("weathercode")){
                double d=o.value("weathercode").toDouble();
                //qDebug() << "weathercode is" << d; // temp
                say(m_speech.value(d));
                if(m_icons.keys().contains(d)){
                    m_weatherIcon=m_icons.value(d);
                    emit weatherIconChanged();
                }
            }
            if(o.contains("temperature")){
                m_temperature=o.value("temperature").toDouble();
                emit temperatureChanged();
            }
        }
    }
    else{
        qWarning() << "RMeteo Error:" << reply->errorString();
    }
    reply->deleteLater();
}
void Plugin::announce()
{
    QString v;
    v.setNum(m_temperature);
    //    say("the current temperature is "+v);
    say("it is "+v+" degrees outside");
}