import QtQuick
import org.rde.ui
RShellPanel{
    RIcon{
        id:icon
        source:'image://Icon/'+Plugin.weatherIcon
    }
    RInfoDisplay{
        anchors{left:icon.right;top:parent.top;bottom:parent.bottom;right:parent.right;margins:4}
        RText{
            id:temperatureLabel
            anchors.fill:parent
            horizontalAlignment:Text.AlignHCenter
            text:Plugin.temperature
        }
        MouseArea{
            anchors.fill:parent
            onClicked:Plugin.announce()
        }
    }
}