#ifndef PANELMODEL_H
#define PANELMODEL_H
#include <QAbstractListModel>
#include <QQmlEngine>
//#include "App.h"
//#include "SNI.h"
//#include "Plugin.h"

//#include "Events.h"

class PanelModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    PanelModel();
    QVariant data(const QModelIndex &index,int role)const;
    // Q_INVOKABLE void click(const int &index);
    // Q_INVOKABLE void hold(const int &index);
    // Q_INVOKABLE void refresh();
    // Q_INVOKABLE void toggle(const QString& appId,const bool& check);
private:
    //int rowCount(const QModelIndex &parent) const{Q_UNUSED(parent);return Plugin::Shortcuts.size();}
    int rowCount(const QModelIndex &parent) const{Q_UNUSED(parent);return 1;}


    QHash<int,QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QAbstractListModel::roleNames();
        //     h[Qt::UserRole]="Valid";
        //     h[Qt::UserRole+1]="Starting";
        h[Qt::UserRole+2]="Running";
        //     h[Qt::UserRole+3]="HasFocus";s
        //     h[Qt::UserRole+4]="HasNotifier";
        return h;
    }
    //QMap<QString,double> m_windows;
    //SNI* m_sni=nullptr;
    //QByteArray eventBuffer;
    //Events* m_events=nullptr;
};
#endif