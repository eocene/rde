cmake_minimum_required(VERSION 3.14)
project(RLaunch LANGUAGES CXX)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "$ENV{RDE_DIR}/rshell-plugin")
find_package(Qt6 REQUIRED COMPONENTS DBus Quick Concurrent)
find_package(PkgConfig REQUIRED)
pkg_check_modules(DF6SNI REQUIRED df6sni)
include_directories(${WayQt_INCLUDE_DIRS})
add_library(RLaunch SHARED
    Plugin.h Plugin.cpp
    #App.h App.cpp
    PanelModel.h PanelModel.cpp
    #SNI.h SNI.cpp
    #AppModel.h AppModel.cpp
    #Events.h Events.cpp
)
qt_add_resources(RLaunch "qml"
    PREFIX "/org.rde.rshell.rlaunch"
    FILES
    Panel.qml
    Window.qml
)
qt6_add_qml_module(RLaunch
    URI org.rde.rshell.rlaunch
    VERSION 1.0
)
target_link_libraries(RLaunch PRIVATE Qt6::DBus Qt6::Quick Qt6::Concurrent ${DF6SNI_LIBRARIES})
target_compile_definitions(RLaunch PRIVATE RLAUNCH_LIBRARY)
set_target_properties(RLaunch PROPERTIES PREFIX "")