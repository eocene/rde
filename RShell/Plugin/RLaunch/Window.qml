import QtQuick
import QtQuick.Controls
import org.rde.ui
import org.rde.rshell.rlaunch
ApplicationWindow{
    visible:true
    title:'Applications'
    color:Qt.rgba(0,0,0,0.5)
    width:640
    height:480
    RTable{
        anchors.fill:parent
        table.model:AppModel{}
        // onActivated:function(row){
        //     //table.model.launch(row)
        //     //grid.model.toggle(row)
        // }
        onChecked:function(row,check){
            grid.model.toggle(table.model.data(table.model.index(row,0),11),check)
        }
    }
}