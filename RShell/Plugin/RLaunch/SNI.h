#ifndef SNI_H
#define SNI_H
#define WATCHER_SERVICE "org.kde.StatusNotifierWatcher"
#define WATCHER_OBJECT "/StatusNotifierWatcher"
#define WATCHER_PATH "org.kde.StatusNotifierWatcher"
#include <DFL/DF6/StatusNotifierWatcher.hpp>
#include <QDBusInterface>
#include <QDebug>
class SNI:public QObject
{
    Q_OBJECT
public:
    explicit SNI(QObject *parent=nullptr);
private:
    DFL::StatusNotifierWatcher* m_watcher=nullptr;
    QDBusInterface* m_interface=nullptr;
signals:
    void addNotifier(const QString &service);
    void removeNotifier(const QString &service);
};
#endif