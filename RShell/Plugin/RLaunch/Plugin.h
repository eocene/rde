#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
#include <QDir>
#include <QFileSystemWatcher>
#include <QFutureWatcher>
#include <QtConcurrentRun>
#include <QStandardPaths>
#include <QMimeDatabase>
#include "App.h"
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rshell.rlaunch" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
public:
    Plugin();
    void load(QSettings* settings);
    void save(QSettings* settings);
    //static QMap<QString,App*> Apps;
    //static QStringList Shortcuts;
private:
    QFileSystemWatcher* m_folderWatcher=nullptr;
    QFutureWatcher<QMap<QString,App*>>* m_watcher=nullptr;
    QMap<QString,App*> scanThread();

private slots:
    void scan(const QString& path);
// signals:
//     void ready();
};
#endif