#include "AppModel.h"
AppModel::AppModel()
{
    //m_columns << "Name" << "GenericName" << "Comment" << "Favorite";
    m_columns << "Name" << "Favorite";
}
QVariant AppModel::headerData(int section,Qt::Orientation orientation,int role) const
{
    return m_columns.at(section);
}
QVariant AppModel::data(const QModelIndex &index,int role) const
{
    switch(role){
    case Qt::DisplayRole:{
        if(index.column()==0)
            return Plugin::Apps.values().at(index.row())->name();
        // else if(index.column()==1)
        //     return m_data.at(index.row()).at(1);
        // else if(index.column()==2)
        //     return m_data.at(index.row()).at(2);
    }
    case Qt::DecorationRole:return QVariant("image://Icon/"+Plugin::Apps.values().at(index.row())->icon());
        ////case Qt::DecorationRole:return QVariant("image://Icon/"+m_data.at(index.row()).at(3));
    // case Qt::TextAlignmentRole:{
    //     if(index.column()==0){return Qt::AlignCenter;}
    //     else if(index.column()==1){return Qt::AlignCenter;}
    //     else if(index.column()==2){return Qt::AlignCenter;}
    // }
    case Qt::CheckStateRole:{
        if(Plugin::Shortcuts.contains(Plugin::Apps.values().at(index.row())->wmclass()))
            return Qt::Checked;
        else
            return Qt::Unchecked;
    }
    case Qt::UserRole:{
        if(index.column()==0)return "IconText";
        else if(index.column()==1)return "CheckBox";
        else return "Text";
    }
    case Qt::AccessibleTextRole:return Plugin::Apps.values().at(index.row())->wmclass();
    }
}