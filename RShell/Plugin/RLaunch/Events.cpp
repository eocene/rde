#include "Events.h"
Events::Events(QObject* parent):QLocalSocket(parent)
{
    setServerName(qgetenv("RWAY_SOCKET"));

    connect(this,&QLocalSocket::connected,[=](){
        //send("{\"method\":\"window-rules/events/watch\",\"data\":{\"events\":[\"view-mapped\",\"view-unmapped\",\"view-focused\"]}}");
    });
    connect(this,&QLocalSocket::disconnected,[=](){
    });
    connect(this,&QLocalSocket::errorOccurred,[=](QLocalSocket::LocalSocketError error){
        switch(error){
        case QLocalSocket::ConnectionRefusedError:break;
        case QLocalSocket::PeerClosedError:break;
        case QLocalSocket::ServerNotFoundError:break;
        case QLocalSocket::SocketAccessError:break;
        case QLocalSocket::SocketResourceError:break;
        case QLocalSocket::SocketTimeoutError:break;
        case QLocalSocket::DatagramTooLargeError:break;
        case QLocalSocket::ConnectionError:break;
        case QLocalSocket::UnsupportedSocketOperationError:break;
        case QLocalSocket::OperationError:break;
        case QLocalSocket::UnknownSocketError:break;
        }
    });
    connect(this,&QLocalSocket::stateChanged,[=](QLocalSocket::LocalSocketState state){
        switch(state){
        case QLocalSocket::UnconnectedState:break;
        case QLocalSocket::ConnectingState:break;
        case QLocalSocket::ConnectedState:break;
        case QLocalSocket::ClosingState:break;
        }
    });
    connect(this,&QLocalSocket::readyRead,[=](){


        qDebug()  << readAll();
    });
    connectToServer(QIODevice::ReadWrite|QIODevice::Unbuffered);
}
void Events::send(QByteArray array)
{
    uint32_t s=array.size();
    char z[4]={};
    memcpy(z,&s,4);
    write(z,4);
    write(array);
    flush();
}