#ifndef APP_H
#define APP_H
#include <QProcess>
class App:public QProcess
{
    Q_OBJECT
public:
    explicit App(const QString& wmclass,const QString& name,const QString& icon,QString program,QStringList args);
    inline QString wmclass(){return m_wmclass;}
    inline QString name(){return m_name;}
    inline QString icon(){return m_icon;}
    bool isStarting();
    //    inline bool isRunning(){
    //        if(m_window!=0)return true;else return false;
    //    }
    //bool hasFocus();
    //inline bool isValid(){return !program().isEmpty();}
    //inline void setNotifier(bool active){m_notifier=active;emit update(this);}
    //inline bool notifier(){return m_notifier;}
private:
    QString m_wmclass;
    QString m_name;
    QString m_icon;
    //bool m_notifier=false;
signals:
    void update(App* app);
};
#endif