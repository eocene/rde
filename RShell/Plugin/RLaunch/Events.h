#ifndef EVENTS_H
#define EVENTS_H
#include <QJsonDocument>
#include <QLocalSocket>
#include <QDebug>
class Events:public QLocalSocket
{
    Q_OBJECT
public:
    explicit Events(QObject* parent);

private:
    void send(QByteArray array);
};
#endif