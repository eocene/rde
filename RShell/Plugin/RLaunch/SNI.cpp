#include "SNI.h"
SNI::SNI(QObject *parent):QObject{parent}
{
    m_watcher=new DFL::StatusNotifierWatcher();
    m_watcher->setParent(this);
    if(!m_watcher->isServiceRunning()){
        qWarning() << "RLaunch: SNI not running!";
    }
    else if(!m_watcher->isObjectRegistered()){
        qWarning() << "RLaunch: SNI not registered!";
    }
    else{
        QString dbusName="org.kde.StatusNotifierHost";
        m_interface=new QDBusInterface(WATCHER_SERVICE,WATCHER_OBJECT,WATCHER_PATH,QDBusConnection::sessionBus());
        m_interface->call("RegisterStatusNotifierHost",dbusName);
        QDBusConnection::sessionBus().connect(WATCHER_SERVICE,WATCHER_OBJECT,WATCHER_PATH,"StatusNotifierItemRegistered","s",this,SIGNAL(addNotifier(QString)));
        QDBusConnection::sessionBus().connect(WATCHER_SERVICE,WATCHER_OBJECT,WATCHER_PATH,"StatusNotifierItemUnregistered","s",this,SIGNAL(removeNotifier(QString)));
    }
}