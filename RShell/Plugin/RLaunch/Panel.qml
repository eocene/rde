import QtQuick
//import QtQuick.Controls
import org.rde.ui
import org.rde.rshell.rlaunch
RShellPanel{
    height:grid.contentHeight
    // property Window window
    GridView{
        id:grid
        anchors.fill:parent
        cellWidth:width/6;
        cellHeight:cellWidth
        model:PanelModel{}
        delegate:RButton{
            width:grid.cellWidth
            height:grid.cellHeight
            onPressed:grid.model.click(index)
            onPressAndHold:grid.model.hold(index)
            background:Rectangle{
                color:'red'
                visible:Running
            }
        }
    }
    Connections{
        target:Plugin
        function onReady(){grid.model.refresh()}
    }
}


/*
        delegate:Item{

            // Rectangle{
            //     anchors{fill:parent;margins:2}
            //     color:Qt.rgba(1,1,1,0.5)
            //     Behavior on opacity{NumberAnimation{duration:200}}
            //     opacity:HasFocus?1:0
            //     radius:6
            // }
            RIconButton{
                anchors.fill:parent
                //source:'image://Icon/'+decoration;
                source:decoration
                //enabled:true
                //enabled:statusTip
                //image.layer.enabled:statusTip

                //opacity:image.layer.enabled?1:0.5
                opacity:statusTip?1:0.5



                onPress:;
                onHold:grid.model.hold(index);
                onRightClick:{
                    if(window)
                        window.destroy()
                    else
                        window=Qt.createComponent('Window.qml').createObject()
                }
            }
            // Image{
            //     anchors{right:parent.right;bottom:parent.bottom;margins:RShell.margin}
            //     sourceSize{width:16;height:16}
            //     source:'image://Icon/emblem-important';
            //     Behavior on opacity{NumberAnimation{duration:200}}
            //     opacity:HasNotifier?1:0
            // }
        }

        //        delegate:Item{
        //            width:grid.cellWidth
        //            height:grid.cellHeight
        //            Rectangle{
        //                id:hasFocus
        //                anchors{fill:parent;margins:2}
        //                color:Qt.rgba(1,1,1,0.5)
        //                Behavior on opacity{NumberAnimation{duration:200}}
        //                opacity:HasFocus?1:0
        //                radius:6
        //            }
        //            Rectangle{
        //                id:isValid
        //                anchors{fill:parent;margins:2}
        //                color:Qt.rgba(1,0,0,1)
        //                //                Behavior on opacity{NumberAnimation{duration:200}}
        //                //                opacity:Valid?0
        //                visible:!Valid
        //                radius:6
        //            }
        //            BusyIndicator{
        //                id:busy
        //                anchors.fill:parent
        //                running:visible
        //                visible:Starting
        //            }
        //        }
    }

}