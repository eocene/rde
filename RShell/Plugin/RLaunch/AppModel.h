#ifndef APPMODEL_H
#define APPMODEL_H
#include <QAbstractTableModel>
#include "PanelModel.h"
class AppModel:public QAbstractTableModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit AppModel();
private:
    int rowCount(const QModelIndex& =QModelIndex()) const override{return Plugin::Apps.size();}
    int columnCount(const QModelIndex& =QModelIndex()) const override{return m_columns.size();}
    QVariant headerData(int section,Qt::Orientation orientation,int role=Qt::DisplayRole) const override;
    QVariant data(const QModelIndex &index,int role) const override;
    QHash<int, QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QAbstractTableModel::roleNames();
        h[Qt::CheckStateRole]="check";
        h[Qt::AccessibleTextRole]="WMClass";
        h[Qt::UserRole]="type";
        return h;
    }
    QStringList m_columns;
};
#endif