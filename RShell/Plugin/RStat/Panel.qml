import QtQuick
import QtQuick.Controls
import org.rde.ui
import org.rde.rshell.rstat
RShellPanel{
    height:content.implicitHeight+(8)
    Column{
        id:content
        anchors{fill:parent;margins:4}
        spacing:0
        CPU{
            anchors{left:parent.left;right:parent.right}
            height:16
        }
        RAM{
            anchors{left:parent.left;right:parent.right}
            height:16
        }
    }
}