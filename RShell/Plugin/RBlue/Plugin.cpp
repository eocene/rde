#include "Plugin.h"
void Plugin::load(QSettings* settings)
{
    Device=new QBluetoothLocalDevice(this);
    if(Device->isValid()){
        connect(Device,&QBluetoothLocalDevice::errorOccurred,[=](QBluetoothLocalDevice::Error error){
            switch(error){
            case QBluetoothLocalDevice::NoError:qWarning() << "Bluetooth: No Error";break;
            case QBluetoothLocalDevice::PairingError:qWarning() << "Bluetooth: Pairing Error";break;
            case QBluetoothLocalDevice::UnknownError:qWarning() << "Bluetooth: Unknown Error";break;
            }
        });
    }
}
bool Plugin::enabled() const
{
    switch(Device->hostMode()){
    case QBluetoothLocalDevice::HostPoweredOff:return false;
    case QBluetoothLocalDevice::HostConnectable:return true;
    case QBluetoothLocalDevice::HostDiscoverable:return true;
    case QBluetoothLocalDevice::HostDiscoverableLimitedInquiry:return true;
    }
}
void Plugin::setEnabled(const bool& enabled)
{
    if(enabled)
        Device->setHostMode(QBluetoothLocalDevice::HostConnectable);
    else
        Device->setHostMode(QBluetoothLocalDevice::HostPoweredOff);
    emit enabledChanged();
}
QString Plugin::iconName(const QBluetoothDeviceInfo& info)
{
    switch(info.majorDeviceClass()){
    case QBluetoothDeviceInfo::MiscellaneousDevice:{
        switch(QBluetoothDeviceInfo::MinorMiscellaneousClass(info.minorDeviceClass())){
        case QBluetoothDeviceInfo::UncategorizedMiscellaneous:break;
        }
        break;
    }
    case QBluetoothDeviceInfo::ComputerDevice:{
        switch(QBluetoothDeviceInfo::MinorComputerClass(info.minorDeviceClass())){
        case QBluetoothDeviceInfo::UncategorizedComputer:break;
        case QBluetoothDeviceInfo::DesktopComputer:return "computer";
        case QBluetoothDeviceInfo::ServerComputer:break;
        case QBluetoothDeviceInfo::LaptopComputer:return "computer-laptop";
        case QBluetoothDeviceInfo::HandheldClamShellComputer:break;
        case QBluetoothDeviceInfo::HandheldComputer:return "tablet";
        case QBluetoothDeviceInfo::WearableComputer:break;
        }
        break;
    }
    case QBluetoothDeviceInfo::PhoneDevice:{
        switch(QBluetoothDeviceInfo::MinorPhoneClass(info.minorDeviceClass())){
        case QBluetoothDeviceInfo::UncategorizedPhone:break;
        case QBluetoothDeviceInfo::CellularPhone:return "phone";
        case QBluetoothDeviceInfo::CordlessPhone:break;
        case QBluetoothDeviceInfo::SmartPhone:return "smartphone";
        case QBluetoothDeviceInfo::WiredModemOrVoiceGatewayPhone:break;
        case QBluetoothDeviceInfo::CommonIsdnAccessPhone:break;
        }
        break;
    }
    case QBluetoothDeviceInfo::NetworkDevice:{
        switch(QBluetoothDeviceInfo::MinorNetworkClass(info.minorDeviceClass())){
        case QBluetoothDeviceInfo::NetworkFullService:break;
        case QBluetoothDeviceInfo::NetworkLoadFactorOne:break;
        case QBluetoothDeviceInfo::NetworkLoadFactorTwo:break;
        case QBluetoothDeviceInfo::NetworkLoadFactorThree:break;
        case QBluetoothDeviceInfo::NetworkLoadFactorFour:break;
        case QBluetoothDeviceInfo::NetworkLoadFactorFive:break;
        case QBluetoothDeviceInfo::NetworkLoadFactorSix:break;
        case QBluetoothDeviceInfo::NetworkNoService:break;
        }
        break;
    }
    case QBluetoothDeviceInfo::AudioVideoDevice:{
        switch(QBluetoothDeviceInfo::MinorAudioVideoClass(info.minorDeviceClass())){
        case QBluetoothDeviceInfo::UncategorizedAudioVideoDevice:break;
        case QBluetoothDeviceInfo::WearableHeadsetDevice:return "audio-headset";
        case QBluetoothDeviceInfo::HandsFreeDevice:break;
        case QBluetoothDeviceInfo::Microphone:return "audio-input-microphone";
        case QBluetoothDeviceInfo::Loudspeaker:return "speaker";
        case QBluetoothDeviceInfo::Headphones:return "audio-headphones";
        case QBluetoothDeviceInfo::PortableAudioDevice:break;
        case QBluetoothDeviceInfo::CarAudio:break;
        case QBluetoothDeviceInfo::SetTopBox:break;
        case QBluetoothDeviceInfo::HiFiAudioDevice:break;
        case QBluetoothDeviceInfo::Vcr:break;
        case QBluetoothDeviceInfo::VideoCamera:break;
        case QBluetoothDeviceInfo::Camcorder:break;
        case QBluetoothDeviceInfo::VideoMonitor:break;
        case QBluetoothDeviceInfo::VideoDisplayAndLoudspeaker:return "video-display";
        case QBluetoothDeviceInfo::VideoConferencing:break;
        case QBluetoothDeviceInfo::GamingDevice:break;
        }
        break;
    }
    case QBluetoothDeviceInfo::PeripheralDevice:{
        switch(QBluetoothDeviceInfo::MinorPeripheralClass(info.minorDeviceClass())){
        case QBluetoothDeviceInfo::UncategorizedPeripheral:break;
        case QBluetoothDeviceInfo::KeyboardPeripheral:return "input-keyboard";
        case QBluetoothDeviceInfo::PointingDevicePeripheral:return "input-mouse";
        case QBluetoothDeviceInfo::KeyboardWithPointingDevicePeripheral:break;
        case QBluetoothDeviceInfo::JoystickPeripheral:break;
        //case QBluetoothDeviceInfo::GamepadPeripheral:return "input-gaming";
        case QBluetoothDeviceInfo::GamepadPeripheral:return "ds5";
        case QBluetoothDeviceInfo::RemoteControlPeripheral:break;
        case QBluetoothDeviceInfo::SensingDevicePeripheral:break;
        case QBluetoothDeviceInfo::DigitizerTabletPeripheral:break;
        case QBluetoothDeviceInfo::CardReaderPeripheral:break;
        }
        break;
    }
    case QBluetoothDeviceInfo::ImagingDevice:{
        switch(QBluetoothDeviceInfo::MinorImagingClass(info.minorDeviceClass())){
        case QBluetoothDeviceInfo::UncategorizedImagingDevice:break;
        case QBluetoothDeviceInfo::ImageDisplay:break;
        case QBluetoothDeviceInfo::ImageCamera:break;
        case QBluetoothDeviceInfo::ImageScanner:return "scanner";
        case QBluetoothDeviceInfo::ImagePrinter:return "printer";
        }
        break;
    }
    case QBluetoothDeviceInfo::WearableDevice:{
        switch(QBluetoothDeviceInfo::MinorWearableClass(info.minorDeviceClass())){
        case QBluetoothDeviceInfo::UncategorizedWearableDevice:break;
        case QBluetoothDeviceInfo::WearableWristWatch:break;
        case QBluetoothDeviceInfo::WearablePager:break;
        case QBluetoothDeviceInfo::WearableJacket:break;
        case QBluetoothDeviceInfo::WearableHelmet:break;
        case QBluetoothDeviceInfo::WearableGlasses:break;
        }
        break;
    }
    case QBluetoothDeviceInfo::ToyDevice:{
        switch(QBluetoothDeviceInfo::MinorToyClass(info.minorDeviceClass())){
        case QBluetoothDeviceInfo::UncategorizedToy:break;
        case QBluetoothDeviceInfo::ToyRobot:break;
        case QBluetoothDeviceInfo::ToyVehicle:break;
        case QBluetoothDeviceInfo::ToyDoll:break;
        case QBluetoothDeviceInfo::ToyController:break;
        case QBluetoothDeviceInfo::ToyGame:break;
        }
        break;
    }
    case QBluetoothDeviceInfo::HealthDevice:{
        switch(QBluetoothDeviceInfo::MinorHealthClass(info.minorDeviceClass())){
        case QBluetoothDeviceInfo::UncategorizedHealthDevice:break;
        case QBluetoothDeviceInfo::HealthBloodPressureMonitor:break;
        case QBluetoothDeviceInfo::HealthThermometer:break;
        case QBluetoothDeviceInfo::HealthWeightScale:break;
        case QBluetoothDeviceInfo::HealthGlucoseMeter:break;
        case QBluetoothDeviceInfo::HealthPulseOximeter:break;
        case QBluetoothDeviceInfo::HealthDataDisplay:break;
        case QBluetoothDeviceInfo::HealthStepCounter:break;
        }
        break;
    }
    case QBluetoothDeviceInfo::UncategorizedDevice:{
        break;
    }
    }
    return "preferences-system-bluetooth";
}
QBluetoothLocalDevice* Plugin::Device;