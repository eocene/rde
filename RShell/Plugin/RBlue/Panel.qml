import QtQuick
import QtQuick.Controls
import org.rde.ui
import org.rde.rshell.rblue
RShellPanel{
    property Window window;
    RButton{
        id:button
        anchors{left:parent.left;top:parent.top;bottom:parent.bottom}
        enabled:Plugin.enabled
        icon.name:'preferences-system-bluetooth'
        checkable:true
        onToggled:{
            if(checked)
                window=Qt.createComponent('Window.qml').createObject()
            else
                window.destroy()
        }
    }
    ListView{
        id:view
        anchors{left:button.right;right:btSwitch.left;top:parent.top;bottom:parent.bottom}
        orientation:Qt.Horizontal
        model:PairedModel{}
        delegate:RButton{
            opacity:Connected?1:0.5
            onPressAndHold:view.model.disconnectDevice(index)
        }
        visible:Plugin.enabled
    }
    Switch{
        id:btSwitch
        anchors{verticalCenter:parent.verticalCenter;right:parent.right}
        checked:Plugin.enabled
        onToggled:Plugin.enabled=checked
    }
}