import QtQuick
import QtQuick.Controls
import org.rde.ui
import org.rde.rshell.rblue
ApplicationWindow{
    visible:true
    title:'Bluetooth'
    color:Qt.rgba(0,0,0,0.5)
    width:640
    height:480
    header:RToolBar{
        id:toolBar
        Row{
            anchors.fill:parent
            spacing:0
            RToolButton{action:addAction}
            RToolButton{action:removeAction}
        }
    }
    RList{
        id:view
        anchors.fill:parent
        //        model:DeviceModel{}
        //        section.property:"Cached"
    }
    RBarShadow{
        parent:view
        anchors{left:parent.left;right:parent.right;top:view.top}
    }
    Action{
        id:addAction
        icon.name:'list-add'
        //        onTriggered:view.model.scan()
    }
    Action{
        id:removeAction
        icon.name:'list-remove'
        onTriggered:{}
    }
    onClosing:destroy()
}