#include "PairedModel.h"
PairedModel::PairedModel()
{
    connect(Plugin::Device,&QBluetoothLocalDevice::hostModeStateChanged,[=](QBluetoothLocalDevice::HostMode mode){
        switch(mode){
        case QBluetoothLocalDevice::HostPoweredOff:{
            beginResetModel();
            m_paired.clear();
            endResetModel();
            break;
        }
        case QBluetoothLocalDevice::HostConnectable:discover();break;
        case QBluetoothLocalDevice::HostDiscoverable:break;
        case QBluetoothLocalDevice::HostDiscoverableLimitedInquiry:break;
        }
    });
    connect(Plugin::Device,&QBluetoothLocalDevice::deviceConnected,[=](const QBluetoothAddress& address){

        if(m_paired.contains(address)){
            QBluetoothDeviceInfo info=m_paired.value(address);
            if(info.isValid()){
                Plugin::say(info.name()+" connected");
                m_connected << address;
                QModelIndex i=createIndex(m_paired.keys().indexOf(address),0);
                emit dataChanged(i,i);
            }
            else
                qWarning() << "RBlue: Device not valid";
        }else
            qWarning() << "RBlue: Unknown device connected?";

    });
    connect(Plugin::Device,&QBluetoothLocalDevice::deviceDisconnected,[=](const QBluetoothAddress& address){

        if(m_paired.contains(address)){
            QBluetoothDeviceInfo info=m_paired.value(address);
            if(info.isValid()){
                Plugin::say(info.name()+" disconnected");
                if(m_connected.contains(address)){
                    m_connected.removeAll(address);
                    QModelIndex i=createIndex(m_paired.keys().indexOf(address),0);
                    emit dataChanged(i,i);
                }
            }
        }
    });
    connect(Plugin::Device,&QBluetoothLocalDevice::pairingFinished,[=](const QBluetoothAddress& address,QBluetoothLocalDevice::Pairing pairing){
        qDebug() << "Pairing finished";
    });
    if(Plugin::Device->hostMode()==QBluetoothLocalDevice::HostConnectable){
        m_connected=Plugin::Device->connectedDevices();
        discover();
    }
}
void PairedModel::discover()
{
    beginResetModel();
    m_agent=new QBluetoothDeviceDiscoveryAgent(this);
    m_agent->setLowEnergyDiscoveryTimeout(1);
    connect(m_agent,&QBluetoothDeviceDiscoveryAgent::finished,m_agent,&QBluetoothDeviceDiscoveryAgent::deleteLater);
    connect(m_agent,&QBluetoothDeviceDiscoveryAgent::destroyed,this,&PairedModel::endResetModel);
    connect(m_agent,&QBluetoothDeviceDiscoveryAgent::deviceDiscovered,[=](const QBluetoothDeviceInfo& info){
        if(info.isValid() && info.majorDeviceClass()!=0 && !m_paired.keys().contains(info.address()))
            m_paired.insert(info.address(),info);
    });
    m_agent->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);
}
QVariant PairedModel::data(const QModelIndex &index,int role) const
{
    QBluetoothDeviceInfo device=m_paired.values().at(index.row());
    switch(role){
    case Qt::DisplayRole:return QVariant(device.name());
    case Qt::DecorationRole:return QVariant(Plugin::iconName(device));
    case Qt::UserRole:return QVariant(m_connected.contains(device.address()));
    }
}