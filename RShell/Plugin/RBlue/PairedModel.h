#ifndef PAIREDMODEL_H
#define PAIREDMODEL_H
#include <QAbstractListModel>
#include <QBluetoothAddress>
#include <QBluetoothDeviceInfo>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QQmlEngine>
#include "Plugin.h"
class PairedModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit PairedModel();
    Q_INVOKABLE void disconnectDevice(int index);
private:
    int rowCount(const QModelIndex & = QModelIndex()) const override{return m_paired.size();}
    QVariant data(const QModelIndex &index,int role) const override;
    QHash<int,QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QAbstractListModel::roleNames();
        h[Qt::UserRole]="Connected";
        return h;
    }
    QList<QBluetoothAddress> m_connected;
    QMap<QBluetoothAddress,QBluetoothDeviceInfo> m_paired;
    void discover();
    QBluetoothDeviceDiscoveryAgent* m_agent=nullptr;
};
#endif