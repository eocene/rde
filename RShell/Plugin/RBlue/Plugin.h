#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
#include <QBluetoothDeviceInfo>
#include <QBluetoothLocalDevice>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rshell.rblue" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
public:
    void load(QSettings* settings) override;
    static QString iconName(const QBluetoothDeviceInfo& info);
    static QBluetoothLocalDevice* Device;
private:
    bool enabled() const;
    void setEnabled(const bool& enabled);
signals:
    void enabledChanged();
};
#endif