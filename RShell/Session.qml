import QtQuick
import org.rde.ui
Window{    
    width:200
    height:column.implicitHeight
    visible:true
    title:'Session'
    color:Qt.rgba(0,0,0,0.5)
    Column{
        id:column
        anchors.fill:parent
        spacing:0
        RListDelegate{
            text:'Log Out'
            icon.name:'system-log-out'
            onClicked:RShell.session("logout")
        }
        RListDelegate{
            text:'Switch user'
            icon.name:'system-switch-user'
            onClicked:RShell.session("switchuser")
        }
        RListDelegate{
            text:'Lock screen'
            icon.name:'system-lock-screen'
            onClicked:RShell.session("lockscreen")
        }
        RListDelegate{
            text:'Suspend'
            icon.name:'system-suspend'
            onClicked:RShell.session("suspend")
        }
        RListDelegate{
            text:'Hibernate'
            icon.name:'system-suspend-hibernate'
            onClicked:RShell.session("hibernate")
        }
        RListDelegate{
            text:'Reboot'
            icon.name:'system-reboot'
            onClicked:RShell.session("reboot")
        }
        RListDelegate{
            text:'Shutdown'
            icon.name:'system-shutdown'
            onClicked:RShell.session("shutdown")
        }
    }
    onClosing:destroy()
}