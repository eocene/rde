#ifndef SCREENMODEL_H
#define SCREENMODEL_H
#include <QAbstractListModel>
#include <QScreen>
#include <QQmlEngine>
#include <QGuiApplication>
//#include <wayqt/Output.hpp>
//#include <wayqt/OutputPower.hpp>
//#include <wayqt/OutputPowerManager.hpp>
class ScreenModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit ScreenModel();
private:
    int rowCount(const QModelIndex &parent) const{Q_UNUSED(parent);return m_screens.size();}
    QVariant data(const QModelIndex &index,int role)const override;
    QHash<int,QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QAbstractListModel::roleNames();
        h[Qt::UserRole]="Enabled";
        h[Qt::UserRole+1]="SerialNumber";
        h[Qt::UserRole+2]="Model";
        h[Qt::UserRole+3]="Manufacturer";
        h[Qt::UserRole+4]="Width";
        h[Qt::UserRole+5]="Height";
        h[Qt::UserRole+6]="ScreenObject";
        return h;
    }
    QList<QScreen*> m_screens;    
    //WQt::OutputPowerManager* m_powerManager=nullptr;
    //inline WQt::OutputPower* getPower(QScreen* screen){return m_powerManager->getOutputPower(WQt::Utils::wlOutputFromQScreen(screen));}
};
#endif