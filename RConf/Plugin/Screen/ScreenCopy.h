#ifndef SCREENCOPY_H
#define SCREENCOPY_H
#include <QtConcurrentRun>
#include <QFutureWatcher>
#include <QQuickItem>
#include <QVideoSink>
#include <QVideoFrame>
#include "wayqt/ScreenCopy.hpp"
#include "wayqt/Registry.hpp"
#include "Window.h"
class ScreenCopy:public QQuickItem
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(QVideoSink* sink READ sink WRITE setSink NOTIFY sinkChanged)
    Q_PROPERTY(QScreen* screen READ screen WRITE setScreen NOTIFY screenChanged)
public:
    ScreenCopy();
    ~ScreenCopy();
    inline QScreen* screen(){return m_screen;}
    inline void setScreen(QScreen* screen){m_screen=screen;m_output=WQt::Utils::wlOutputFromQScreen(screen);}
    inline QVideoSink* sink(){return m_sink;}
    inline void setSink(QVideoSink* sink){m_sink=sink;}
public slots:
    inline void nextFrame(){
        //thread()->usleep(1000*1000*1);
        // WQt::ScreenCopyFrame* frame=Manager->captureOutput(false,m_output);
        // connect(frame,&WQt::ScreenCopyFrame::bufferDone,frame,&WQt::ScreenCopyFrame::copy);
        // connect(frame,&WQt::ScreenCopyFrame::ready,[=](WQt::ScreenFrameBuffer* buffer){
        //     m_watcher->setFuture(QtConcurrent::run(&ScreenCopy::setFrame,this,buffer));
        // });
        // frame->setup();
    }
private:
    QVideoSink* m_sink=nullptr;
    QScreen* m_screen=nullptr;
    wl_output* m_output=nullptr;
    QFutureWatcher<void>* m_watcher=nullptr;    
    void setFrame(WQt::ScreenFrameBuffer* buffer);
    QMap<wl_shm_format, QImage::Format> m_formats;
protected:
    static WQt::ScreenCopyManager* Manager;
signals:
    void sinkChanged();
    void screenChanged();
};
#endif