#include "ScreenModel.h"
ScreenModel::ScreenModel()
{
    m_screens=QGuiApplication::screens();

    // m_powerManager=Window::registry->outputPowerManager();
    // if(Window::registry->waitForInterface(WQt::Registry::OutputPowerManagerInterface)){
    //     qDebug() << "Output power manager OK!";
    // }

    connect(qGuiApp,&QGuiApplication::screenAdded,[=](QScreen* screen){
        beginResetModel();
        m_screens.append(screen);
        endResetModel();
    });
    connect(qGuiApp,&QGuiApplication::screenRemoved,[=](QScreen* screen){
        beginResetModel();
        m_screens.removeAll(screen);
        endResetModel();
    });
    connect(qGuiApp,&QGuiApplication::primaryScreenChanged,[=](QScreen* screen){
    });
}
QVariant ScreenModel::data(const QModelIndex &index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return m_screens.at(index.row())->name();
    case Qt::DecorationRole:{
        if(m_screens.at(index.row())->model().contains("TV"))
            return "video-television";
        else
            return "video-display";
    }
    case Qt::UserRole:{
        //WQt::OutputPower* power=getPower(m_screens.at(index.row()));
        //  connect(power,&WQt::OutputPower::failed,[=](){
        //      qDebug() << "Power failed";
        //  });
        // return power->isOn();
        return true;
    }
    case Qt::UserRole+1:return m_screens.at(index.row())->serialNumber();
    case Qt::UserRole+2:return m_screens.at(index.row())->model();
    case Qt::UserRole+3:return m_screens.at(index.row())->manufacturer();
    case Qt::UserRole+4:return m_screens.at(index.row())->physicalSize().width();
    case Qt::UserRole+5:return m_screens.at(index.row())->physicalSize().height();
    case Qt::UserRole+6:return QVariant::fromValue(m_screens.at(index.row()));
    }
}