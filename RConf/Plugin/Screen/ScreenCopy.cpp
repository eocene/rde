#include "ScreenCopy.h"
ScreenCopy::ScreenCopy()
{
    if(!Manager)
        Manager=Window::registry->screenCopier();

    if(Window::registry->waitForInterface(WQt::Registry::ScreenCopyManagerInterface)){

        m_formats.insert(WL_SHM_FORMAT_XRGB8888, QImage::Format_ARGB32);
        m_formats.insert(WL_SHM_FORMAT_ARGB8888, QImage::Format_ARGB32);
        m_formats.insert(WL_SHM_FORMAT_XBGR8888, QImage::Format_RGBA8888);
        m_formats.insert(WL_SHM_FORMAT_ABGR8888, QImage::Format_RGBA8888);

        m_watcher=new QFutureWatcher<void>(this);
        connect(m_watcher,SIGNAL(finished()),this,SLOT(nextFrame()));
    }
}
extern void ScreenCopy::setFrame(WQt::ScreenFrameBuffer* buffer)
{
    QVideoFrameFormat fmt(
        QSize(buffer->info.width,buffer->info.height),
        QVideoFrameFormat::pixelFormatFromImageFormat(m_formats[buffer->info.format])
        );

    int imgSize=buffer->info.width*buffer->info.height*4;
    QVideoFrame frame(fmt);
    frame.map(QVideoFrame::ReadWrite);
    memcpy(frame.bits(0),buffer->data,imgSize);
    frame.unmap();
    m_sink->setVideoFrame(frame);
}
ScreenCopy::~ScreenCopy()
{
    qDebug() << m_screen->name() << "OFF";
}
WQt::ScreenCopyManager* ScreenCopy::Manager;