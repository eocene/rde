import QtQuick
import QtQuick.Layouts
import org.rde.rconf
import org.rde.rconf.theme
import org.rde.ui
Panel{
    anchors.fill:parent
    ColumnLayout{
        anchors.fill:parent
        spacing:0
        RComboBar{
            id:style
            Layout.fillWidth:true
            label:'Style'
            box{
                model:StyleModel{}
                displayText:Plugin.style
                onActivated:Plugin.style=box.currentText
            }
        }
        RComboBar{
            Layout.fillWidth:true
            label:'Font'
            box{
                model:FontModel{}
                displayText:Plugin.font
                onActivated:Plugin.font=box.currentText
            }
        }
        RComboBar{
            Layout.fillWidth:true
            label:'Icons'
            box{
                model:IconThemeModel{}
                displayText:Plugin.iconTheme
                onActivated:Plugin.iconTheme=box.currentText
            }
        }
        RComboBar{
            Layout.fillWidth:true
            label:'Cursor'
            box{
                model:CursorThemeModel{}
                displayText:Plugin.cursorTheme
                onActivated:Plugin.cursorTheme=box.currentText
            }
        }
        Item{
            Layout.fillWidth:true
            Layout.fillHeight:true
        }
    }
}