#ifndef FONTMODEL_H
#define FONTMODEL_H
#include <QStringListModel>
#include <QFontDatabase>
#include <QQmlEngine>
class FontModel:public QStringListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit FontModel(){setStringList(QFontDatabase::families());}
};
#endif