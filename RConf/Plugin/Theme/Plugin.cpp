#include "Plugin.h"
Plugin::Plugin()
{
    m_interface=new QDBusInterface("org.rde.theme","/org/rde/theme","org.rde.theme.theme",QDBusConnection::sessionBus(),this);
    if(m_interface->isValid()){
    }
}
void Plugin::setFont(const QString& font)
{
    QFont f(font);
    QGuiApplication::setFont(f);
    emit fontChanged();
}
void Plugin::setIconTheme(const QString& iconTheme)
{
    m_interface->setProperty("IconTheme",iconTheme);

    QIcon::setThemeName(iconTheme);
    emit iconThemeChanged();
}
void Plugin::setCursorTheme(const QString& cursorTheme)
{
    m_cursorTheme=cursorTheme;
    emit cursorThemeChanged();
}
void Plugin::setStyle(const QString& style)
{
    m_style=style;
    emit styleChanged();
    //QStyle* style=qApp->setStyle("Fusion");
    //qApp->setStyle();
}