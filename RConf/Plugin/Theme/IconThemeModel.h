#ifndef ICONTHEMEMODEL_H
#define ICONTHEMEMODEL_H
#include <QtConcurrent>
#include <QFutureWatcher>
#include <QIcon>
#include <QStringListModel>
#include <QQmlEngine>
class IconThemeModel:public QStringListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit IconThemeModel();
private:
    QStringList scan();
    QFutureWatcher<QStringList>* m_watcher=nullptr;
};
#endif