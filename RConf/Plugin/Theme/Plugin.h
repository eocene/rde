#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
#include <QDBusInterface>
#include <QGuiApplication>
#include <QFont>
#include <QIcon>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rconf.theme" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
    Q_PROPERTY(QString font READ font WRITE setFont NOTIFY fontChanged)
    Q_PROPERTY(QString iconTheme READ iconTheme WRITE setIconTheme NOTIFY iconThemeChanged)
    Q_PROPERTY(QString cursorTheme READ cursorTheme WRITE setCursorTheme NOTIFY cursorThemeChanged)
    Q_PROPERTY(QString style READ style WRITE setStyle NOTIFY styleChanged)
public:
    Plugin();
    QString font()const{return QGuiApplication::font().toString();}
    void setFont(const QString& font);
    QString iconTheme()const{return QIcon::themeName();}
    void setIconTheme(const QString& iconTheme);
    QString cursorTheme()const{return m_cursorTheme;}
    void setCursorTheme(const QString& cursorTheme);
    QString style()const{return m_style;}
    void setStyle(const QString& style);
private:
    QString m_cursorTheme;
    QString m_style;
    QDBusInterface* m_interface=nullptr;
signals:
    void fontChanged();
    void iconThemeChanged();
    void cursorThemeChanged();
    void styleChanged();
};
#endif