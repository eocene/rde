#include "IconThemeModel.h"
IconThemeModel::IconThemeModel()
{
    m_watcher=new QFutureWatcher<QStringList>(this);
    connect(m_watcher,&QFutureWatcherBase::finished,[=](){
        setStringList(m_watcher->result());
        m_watcher->deleteLater();
    });
    m_watcher->setFuture(QtConcurrent::run(&IconThemeModel::scan,this));
}
extern QStringList IconThemeModel::scan()
{
    QStringList l;
    foreach(QString path,QIcon::themeSearchPaths()){
        QDir iconDir(path);
        if(iconDir.isReadable()){
            foreach(QFileInfo info,iconDir.entryInfoList(QDir::Dirs|QDir::NoDotAndDotDot,QDir::Name)){
                QString base=info.baseName();
                if(base!="default" && base!="locolor" && base!="hicolor"){
                    QFileInfo themeFile(info.absoluteFilePath()+"/index.theme");
                    if(themeFile.isReadable()){
                        QSettings s(themeFile.absoluteFilePath(),QSettings::NativeFormat);
                        if(s.childGroups().contains("Icon Theme")){
                            l << base;
                        }
                    }
                }
            }
        }
    }
    return l;
}