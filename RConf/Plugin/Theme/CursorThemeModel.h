#ifndef CURSORTHEMEMODEL_H
#define CURSORTHEMEMODEL_H
#include <QtConcurrent>
#include <QFutureWatcher>
#include <QIcon>
#include <QQmlEngine>
#include <QStringListModel>
class CursorThemeModel:public QStringListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit CursorThemeModel();
private:
    QStringList scan();
    QFutureWatcher<QStringList>* m_watcher=nullptr;
};
#endif