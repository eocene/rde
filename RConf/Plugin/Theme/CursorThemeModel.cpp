#include "CursorThemeModel.h"
CursorThemeModel::CursorThemeModel()
{
    m_watcher=new QFutureWatcher<QStringList>(this);
    connect(m_watcher,&QFutureWatcherBase::finished,[=](){
        setStringList(m_watcher->result());
        m_watcher->deleteLater();
    });
    m_watcher->setFuture(QtConcurrent::run(&CursorThemeModel::scan,this));
}
extern QStringList CursorThemeModel::scan()
{
    QStringList l;
    foreach(QString path,QIcon::themeSearchPaths()){
        QDir iconDir(path);
        if(iconDir.isReadable()){
            foreach(QFileInfo info,iconDir.entryInfoList(QDir::Dirs|QDir::NoDotAndDotDot,QDir::Name)){
                if(info.isDir()){
                    QDir cursorDir(info.absoluteFilePath()+"/cursors");
                    if(cursorDir.isReadable()){
                        l << info.baseName();
                    }
                }
            }
        }
    }
    return l;
}