#ifndef STYLEMODEL_H
#define STYLEMODEL_H
#include <QQmlEngine>
#include <QStringListModel>
#include <QStyleFactory>
class StyleModel:public QStringListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit StyleModel(){setStringList(QStyleFactory::keys());}
};
#endif