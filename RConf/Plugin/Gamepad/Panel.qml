import QtQuick
import QtQuick.Layouts
import org.rde.rconf
import org.rde.rconf.gamepad
import org.rde.ui
Panel{
    anchors.fill:parent
    RowLayout{
        anchors.fill:parent
        spacing:0

        RList{
            Layout.preferredWidth:400
            Layout.fillHeight:true
            model:GamepadModel{}
            //model:page.model
        }
        Rectangle{
            Layout.fillWidth:true
            Layout.fillHeight:true
        }
    }
}