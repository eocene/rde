#ifndef GAMEPADMODEL_H
#define GAMEPADMODEL_H
#include <QAbstractListModel>
#include <QQmlEngine>
#include <QtGamepadLegacy/QGamepad>
#include <QtGamepadLegacy/QGamepadManager>
class GamepadModel:public QAbstractListModel
{    
    Q_OBJECT
    QML_ELEMENT
public:
    GamepadModel();
private:
    int rowCount(const QModelIndex &parent) const{Q_UNUSED(parent);return m_pads.size();}
    QVariant data(const QModelIndex &index,int role) const override;
    // QHash<int,QByteArray> roleNames() const override
    // {
    //     QHash<int,QByteArray> h=QAbstractListModel::roleNames();
    //     // h[Qt::UserRole]="Enabled";
    //     // h[Qt::UserRole+1]="SerialNumber";
    //     // h[Qt::UserRole+2]="Model";
    //     // h[Qt::UserRole+3]="Manufacturer";
    //     // h[Qt::UserRole+4]="Width";
    //     // h[Qt::UserRole+5]="Height";
    //     // h[Qt::UserRole+6]="ScreenObject";
    //     return h;
    // }

    QMap<int,QGamepad*> m_pads;

    //QList<QGamepad*> m_pads;
    QGamepadManager* m_manager=nullptr;
};
#endif