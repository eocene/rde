#ifndef GAMEPADPAGE_H
#define GAMEPADPAGE_H
#include "AbstractPage.h"
#include "Model/GamepadModel.h"
class GamepadPage:public AbstractPage
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(GamepadModel* model READ model CONSTANT)
public:
    GamepadPage();
    ~GamepadPage() override;
    GamepadModel* model(){return m_model;}
private:
    GamepadModel* m_model=nullptr;
};
#endif