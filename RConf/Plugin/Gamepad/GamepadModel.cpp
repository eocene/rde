#include "GamepadModel.h"
GamepadModel::GamepadModel()
{
    m_manager=QGamepadManager::instance();
    connect(m_manager,&QGamepadManager::gamepadConnected,[=](int id){
        int row=m_pads.size();
        beginInsertRows(index(row,row),row,row);
        m_pads.insert(id,new QGamepad(id,this));
        endInsertRows();
    });
    connect(m_manager,&QGamepadManager::gamepadDisconnected,[=](int id){
        int row=m_pads.keys().indexOf(id);
        beginRemoveRows(index(row,row),row,row);
        m_pads.remove(id);
        endRemoveRows();
    });
    foreach(int i,m_manager->connectedGamepads())
        m_pads.insert(i,new QGamepad(i,this));
}
QVariant GamepadModel::data(const QModelIndex &index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return QVariant(m_pads.values().at(index.row())->name());
    case Qt::DecorationRole:return QVariant("image://Icon/input-gaming");
    }
}