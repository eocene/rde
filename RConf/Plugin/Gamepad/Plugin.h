#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rconf.gamepad" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)
public:
    Plugin();
};
#endif