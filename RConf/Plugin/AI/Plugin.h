#ifndef PLUGIN_H
#define PLUGIN_H
#include "../../PluginInterface.h"
#include <QStringListModel>
#include <QFileInfo>
#include <QDBusInterface>
class Plugin:public QObject,public PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.rde.rconf.ai" FILE "metadata.json")
    Q_INTERFACES(PluginInterface)    
    Q_PROPERTY(QStringListModel* voiceModel READ voiceModel CONSTANT)
    Q_PROPERTY(QString voice READ voice WRITE setVoice NOTIFY voiceChanged)
    Q_PROPERTY(QString voicePath READ voicePath WRITE setVoicePath NOTIFY voicePathChanged)
    Q_PROPERTY(double speed READ speed WRITE setSpeed NOTIFY speedChanged)
public:
    Plugin();
    QStringListModel* voiceModel(){return m_voiceModel;}
    bool active(){return m_active;}
    QString voice(){return m_voice;}
    void setVoice(const QString& voice);
    QString voicePath(){return m_voicePath;}
    void setVoicePath(const QString& path);
    double speed(){return m_speed;}
    void setSpeed(const double& speed);
    Q_SCRIPTABLE void speak(const QString& text);
private:
    QStringListModel* m_voiceModel=nullptr;
    QDBusInterface* m_mouth=nullptr;
    bool m_active=false;
    QString m_voice;
    QString m_voicePath;
    double m_speed;
signals:
    void activeChanged();
    void voiceChanged();
    void voicePathChanged();
    void speedChanged();
};
#endif