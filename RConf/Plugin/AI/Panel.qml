import QtQuick
import QtQuick.Layouts
import org.rde.rconf
import org.rde.ui
Panel{
    anchors.fill:parent
    ColumnLayout{
        anchors.fill:parent
        spacing:0
        RPathBar{
            Layout.fillWidth:true
            label:'Voice path'
            input{
                text:Plugin.voicePath
                onAccepted:Plugin.voicePath=input.text
            }
        }
        RComboBar{
            Layout.fillWidth:true
            label:'Voice'
            box{
                model:Plugin.voiceModel
                displayText:Plugin.voice
                onActivated:Plugin.voice=box.currentValue
            }
        }
        RSliderBar{
            Layout.fillWidth:true
            label:'Speed'
            slider{
                from:0.1
                to:2.0
                stepSize:0.1
                value:Plugin.speed
                onMoved:Plugin.speed=slider.value
            }
        }
        RBar{
            Layout.fillWidth:true
            RTextField{
                anchors{fill:parent;margins:4}
                onAccepted:Plugin.speak(text)
                placeholderText:'Test voice here'
            }
        }
        Item{
            Layout.fillWidth:true
            Layout.fillHeight:true
        }
    }
}
//}