#include "Plugin.h"
Plugin::Plugin()
{
    m_mouth=new QDBusInterface("org.rde.ai","/org/rde/ai/Mouth","org.rde.ai.mouth",QDBusConnection::sessionBus(),this);
    if(m_mouth->isValid()){
        m_active=true;
        m_voice=m_mouth->property("voice").toString();
        m_voicePath=m_mouth->property("voicePath").toString();
        m_voiceModel=new QStringListModel(m_mouth->property("voices").toStringList(),this);
        m_speed=m_mouth->property("speed").toDouble();
    }
    else{
        qWarning() << "Failed to connect to rde-ai service";
    }
}
void Plugin::setVoice(const QString& voice)
{
    if(voice!=m_voice){
        m_mouth->setProperty("voice",voice);
        m_voice=voice;
        emit voiceChanged();
        speak(m_voice);
    }
}
void Plugin::setVoicePath(const QString& path)
{
    QFileInfo info(path);
    if(info.isDir()){
        m_mouth->setProperty("voicePath",path);
        m_voicePath=path;
        emit voicePathChanged();
    }else{
        qWarning() << path << "is not valid!";
    }
}
void Plugin::setSpeed(const double &speed)
{
    m_mouth->setProperty("speed",speed);
}
void Plugin::speak(const QString &text)
{
    m_mouth->asyncCall("speak",text);
}