#define Service "org.rde.rconf"
#define Path "/RConf"
#include "Application.h"
#include <QDBusInterface>
int main(int argc,char *argv[])
{
    if(QDBusConnection::systemBus().isConnected()){
        if(QDBusConnection::sessionBus().registerService(Service)){
            Application app=Application(argc,argv);
            if(QDBusConnection::sessionBus().registerObject(Path,&app,QDBusConnection::ExportScriptableSlots))
                return app.exec();
            else return 3;
        }else{
            QDBusInterface i(Service,Path,"",QDBusConnection::sessionBus());
            if(i.isValid())
                i.call("quit");
            return 2;
        }
    } else return 1;
}