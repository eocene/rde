import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.rde.ui
import org.rde.rconf
ApplicationWindow{
    id:window
    color:Qt.rgba(0,0,0,0.5)
    visible:true
    visibility:Window.Maximized
    RowLayout{
        anchors.fill:parent
        spacing:0
        Rectangle{
            Layout.preferredWidth:80
            Layout.fillHeight:true
            color:Qt.rgba(0,0,0,0)
            border{width:1;color:Qt.rgba(0,0,0,0.3)}
            RList{
                id:list
                anchors.fill:parent                
                currentIndex:PluginModel.currentIndex
                onCurrentIndexChanged:PluginModel.currentIndex=currentIndex
                model:PluginModel
                delegate:ItemDelegate{
                    display:AbstractButton.IconOnly
                    width:parent.width
                    height:width
                    background:Item{}
                    icon{
                        width:64
                        height:64
                        name:decoration
                    }
                    onPressed:{
                        list.currentIndex=index;
                    }
                }
            }
        }
        PanelContainer{
            id:container
            Layout.fillWidth:true
            Layout.fillHeight:true
        }
    }
}