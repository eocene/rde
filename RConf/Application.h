#ifndef APPLICATION_H
#define APPLICATION_H
#include <QGuiApplication>
#include "PluginModel.h"
class Application:public QGuiApplication
{
    Q_OBJECT
public:
    explicit Application(int &argc,char **argv);
public slots:
    Q_SCRIPTABLE void quit();
};
#endif