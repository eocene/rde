#ifndef PLUGINMODEL_H
#define PLUGINMODEL_H
#include <QAbstractListModel>
#include <QSettings>
#include "Plugin.h"
class PluginModel:public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY currentIndexChanged)
public:
    static PluginModel* instance(QObject* parent=nullptr);
    ~PluginModel();
    QVariant data(const QModelIndex& index,int role) const override;
    int currentIndex()const{return m_currentIndex;}
    void setCurrentIndex(const int& index);
private:
    PluginModel(QObject* parent=nullptr);
    static PluginModel* s_instance;
    int rowCount(const QModelIndex &parent)const{Q_UNUSED(parent);return Plugin::s_plugins.size();}
    int m_currentIndex;
signals:
    void currentIndexChanged();
};
#endif