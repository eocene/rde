 #include "Application.h"
Application::Application(int &argc,char **argv):QGuiApplication(argc,argv)
{
    setOrganizationName("rde");
    setApplicationName("rconf");
    setApplicationDisplayName("RDE Configuration");

    Plugin::s_engine=new QQmlApplicationEngine(this);
    Plugin::s_engine->rootContext()->setContextProperty("PluginModel",PluginModel::instance(this));
    Plugin::s_engine->load("qrc:/Window.qml");
}
void Application::quit()
{
    qGuiApp->quit();
}