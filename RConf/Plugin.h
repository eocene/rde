#ifndef PLUGIN_H
#define PLUGIN_H
#include <QDir>
#include <QPluginLoader>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <QResource>
#include "Panel.h"
#include "PluginInterface.h"
class Plugin:public QPluginLoader
{
    Q_OBJECT
public:
    Plugin(const QString& file);
    ~Plugin();
    Q_INVOKABLE void open();
    void close();
    QString iid()const{return m_iid;}
    QString name()const{return m_name;}
    QString icon()const{return m_icon;}
    static void loadPlugins();
    static QList<Plugin*> s_plugins;
    static QQmlApplicationEngine* s_engine;
    static Plugin* s_currentPlugin;
private:
    QObject* m_instance=nullptr;
    PluginInterface* m_interface=nullptr;
    QQmlComponent* m_component=nullptr;
    QQmlContext* m_context=nullptr;
    Panel* m_panel=nullptr;
    QString m_iid;
    QString m_name;
    QString m_icon;
};
#endif