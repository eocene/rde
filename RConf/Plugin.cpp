#include "Plugin.h"
Plugin::Plugin(const QString& file):QPluginLoader(file)
{
    QJsonObject meta=metaData();
    m_iid=meta.value("IID").toString();
    QJsonObject data=meta.value("MetaData").toObject();
    m_name=data.value("name").toString();
    m_icon=data.value("icon").toString();
}
void Plugin::open()
{
    if(load()){
        m_instance=instance();
        if(m_instance){
            m_instance->setParent(this);
            m_interface=qobject_cast<PluginInterface*>(m_instance);
            QResource panel("/"+m_iid+"/Panel.qml");
            if(panel.isValid()){
                m_context=new QQmlContext(s_engine,this);
                m_context->setContextProperty("Plugin",m_instance);
                m_component=new QQmlComponent(s_engine);
                connect(m_component,&QQmlComponent::statusChanged,[=](QQmlComponent::Status status){
                    switch(status){
                    case QQmlComponent::Null:break;
                    case QQmlComponent::Loading:break;
                    case QQmlComponent::Ready:
                        m_panel=qobject_cast<Panel*>(m_component->create(m_context));
                        m_panel->setParent(this);
                        m_component->deleteLater();

                        if(s_currentPlugin)
                            s_currentPlugin->close();
                        s_currentPlugin=this;

                        break;
                    case QQmlComponent::Error:
                        qWarning() << m_component->errorString();
                        break;
                    }
                });
                m_component->loadUrl("qrc"+panel.absoluteFilePath(),QQmlComponent::Asynchronous);
            }
        }
    }
}
void Plugin::close()
{
    delete m_panel;
    delete m_context;
    //delete m_interface;
    delete m_instance;

    if(unload()){

    }
}
Plugin::~Plugin()
{
}
void Plugin::loadPlugins()
{
    qRegisterMetaType<PluginInterface*>("PluginInterface");

    QDir dir("/usr/lib/rde/rconf","*.so");
    if(dir.isReadable()){
        foreach(QFileInfo info,dir.entryInfoList()){
            Plugin* plugin=new Plugin(info.absoluteFilePath());
            if(plugin->iid().startsWith("org.rde.rconf."))
                Plugin::s_plugins.append(plugin);
            else
                plugin->deleteLater();
        }
    }
}
QQmlApplicationEngine* Plugin::s_engine;
QList<Plugin*> Plugin::s_plugins;
Plugin* Plugin::s_currentPlugin;