#ifndef PANELCONTAINER_H
#define PANELCONTAINER_H
#include <QQuickItem>
class PanelContainer:public QQuickItem
{
    Q_OBJECT
    QML_ELEMENT    
public:
    PanelContainer();
    static PanelContainer* s_instance;
};
#endif
