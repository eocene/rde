#include "PluginModel.h"
PluginModel::PluginModel(QObject* parent):QAbstractListModel(parent)
{
    QSettings s;
    m_currentIndex=s.value("page",0).toInt();

    Plugin::loadPlugins();
}
void PluginModel::setCurrentIndex(const int& index)
{
    m_currentIndex=index;
    Plugin::s_plugins.at(index)->open();
}
QVariant PluginModel::data(const QModelIndex& index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return QVariant(Plugin::s_plugins.at(index.row())->name());
    case Qt::DecorationRole:return QVariant(Plugin::s_plugins.at(index.row())->icon());
    }
}
PluginModel::~PluginModel()
{
    QSettings settings;
    settings.setValue("page",m_currentIndex);
}
PluginModel* PluginModel::instance(QObject* parent){if(!s_instance)s_instance=new PluginModel(parent);return s_instance;}
PluginModel* PluginModel::s_instance;