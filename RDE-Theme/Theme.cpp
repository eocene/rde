#include "Theme.h"
Theme::Theme()
{
    if(QDBusConnection::systemBus().isConnected()){
        if(QDBusConnection::sessionBus().registerService("org.rde.theme")){
            m_adaptor=new Adaptor(this);            
            if(QDBusConnection::sessionBus().registerObject("/org/rde/theme",this)){
            }
        }
        m_interface=new QDBusInterface("org.rde.theme","/org/rde/theme","org.rde.theme.theme",QDBusConnection::sessionBus(),this);
        if(m_interface->isValid()){
            if(QDBusConnection::sessionBus().connect("org.rde.theme","/org/rde/theme","org.freedesktop.DBus.Properties","PropertiesChanged",this,SLOT(propertiesChanged(QString,QVariantMap,QStringList)))){
                m_iconTheme=m_interface->property("IconTheme").toString();
                m_cursorTheme=m_interface->property("CursorTheme").toString();
            }
        }
    }
}
QVariant Theme::themeHint(QPlatformTheme::ThemeHint hint) const
{
    switch(hint){
    // case CursorFlashTime:break;
    // case KeyboardInputInterval:break;
    case MouseDoubleClickInterval:return 400;
    // case StartDragDistance:break;
    // case StartDragTime:break;
    // case KeyboardAutoRepeatRate:break;
    // case PasswordMaskDelay:break;
    // case StartDragVelocity:break;
    // case TextCursorWidth:break;
    // case DropShadow:break;
    // case MaximumScrollBarDragDistance:break;
    // case ToolButtonStyle:break;
    // case ToolBarIconSize:break;
    case ItemViewActivateItemOnSingleClick:{
        return false;
    }
    case SystemIconThemeName:return m_iconTheme;
    case SystemIconFallbackThemeName:break;
    case IconThemeSearchPaths:{
        QStringList l;
        foreach(QString p,QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation)){
            QFileInfo inf(p+"/icons");
            if(inf.isDir())
                l << inf.absoluteFilePath();
        }
        return l;
    }
    // case StyleNames:break;
    // case WindowAutoPlacement:break;
    // case DialogButtonBoxLayout:break;
    // case DialogButtonBoxButtonsHaveIcons:break;
    // case UseFullScreenForPopupMenu:break;
    // case KeyboardScheme:break;
    // case UiEffects:break;
    // case SpellCheckUnderlineStyle:break;
    // case TabFocusBehavior:break;
    // case IconPixmapSizes:{

    //     QSize size(32,32);
    //     return size;
    //     //break;
    // }
    // case PasswordMaskCharacter:break;
    // case DialogSnapToDefaultButton:break;
    // case ContextMenuOnMouseRelease:break;
    // case MousePressAndHoldInterval:break;
    // case MouseDoubleClickDistance:break;
    case WheelScrollLines:return 3;
    // case TouchDoubleTapDistance:break;
    // case ShowShortcutsInContextMenus:break;
    // case IconFallbackSearchPaths:break;
    // case MouseQuickSelectionThreshold:break;
    // case InteractiveResizeAcrossScreens:break;
    // case ShowDirectoriesFirst:break;
    // case PreselectFirstFileInDirectory:break;
    // case ButtonPressKeys:break;
    // case SetFocusOnTouchRelease:break;
    // case FlickStartDistance:break;
    // case FlickMaximumVelocity:break;
    // case FlickDeceleration:break;
    // case MenuBarFocusOnAltPressRelease:break;
    case MouseCursorTheme:return m_cursorTheme;
    // case MouseCursorSize:break;
    // case UnderlineShortcut:break;
    default:{
        return QPlatformTheme::themeHint(hint);
    }
    }
}
void Theme::propertiesChanged(const QString& interface,const QVariantMap& properties,QStringList c)
{
    foreach(QString key,properties.keys()){
        QVariant v=properties.value(key);
        if(key=="IconTheme"){
            m_iconTheme=v.toString();
            QIconLoader::instance()->updateSystemTheme();
        }else if(key=="CursorTheme"){
            m_cursorTheme=v.toString();
        }
    }    
}