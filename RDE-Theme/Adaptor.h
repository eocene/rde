#ifndef ADAPTOR_H
#define ADAPTOR_H
#include <QDBusAbstractAdaptor>
#include <QDBusConnection>
#include <QDBusMessage>
#include <QMetaClassInfo>
#include <QSettings>
class Adaptor:public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface","org.rde.theme.theme")
    Q_PROPERTY(QString IconTheme READ iconTheme WRITE setIconTheme)
    Q_PROPERTY(QString CursorTheme READ cursorTheme WRITE setCursorTheme)
public:
    explicit Adaptor(QObject* object):QDBusAbstractAdaptor(object)    
    {
        QSettings settings("rde","theme");
        settings.beginGroup("Theme");
        m_iconTheme=settings.value("icontheme").toString();
        m_cursorTheme=settings.value("cursortheme").toString();
        settings.endGroup();

        connect(this,&Adaptor::setIconTheme,[=](const QString& iconTheme){
            m_iconTheme=iconTheme;
            m_updatedProperties.insert("IconTheme",iconTheme);
            signalPropertyChange("IconTheme");
        });

        connect(this,&Adaptor::setCursorTheme,[=](const QString& cursorTheme){
            m_cursorTheme=cursorTheme;
            m_updatedProperties.insert("CursorTheme",cursorTheme);
            signalPropertyChange("CursorTheme");
        });
    }
    QString iconTheme()const{return m_iconTheme;}
    QString cursorTheme()const{return m_cursorTheme;}


    // Adaptor::~Adaptor()
    // {
    //     QSettings settings("rde","theme");
    //     settings.beginGroup("Theme");
    //     settings.setValue("icontheme",m_iconTheme);
    //     settings.setValue("cursortheme",m_cursorTheme);
    //     settings.endGroup();
    // }
private:
    QString m_iconTheme;
    QString m_cursorTheme;
    QVariantMap m_updatedProperties;
    QStringList m_invalidatedProperties;
    void signalPropertyChange(const QString& property)
    {
        QMetaObject::invokeMethod( this,"_m_emitPropertiesChanged",Qt::QueuedConnection);
    }
    void signalPropertyChange(const QString& property,const QVariant& value)
    {
        QMetaObject::invokeMethod(this,"_m_emitPropertiesChanged",Qt::QueuedConnection);
        // if ( m_updatedProperties.isEmpty() && m_invalidatedProperties.isEmpty() ) {
        //     QMetaObject::invokeMethod( this, "_m_emitPropertiesChanged", Qt::QueuedConnection );
        //     debug() << "MPRIS2: Queueing up a PropertiesChanged signal";
        // }

        // m_updatedProperties[property] = value;
    }
private Q_SLOTS:
    void _m_emitPropertiesChanged()
    {
        if(m_updatedProperties.isEmpty() && m_invalidatedProperties.isEmpty()){
            return;
        }
        int index=metaObject()->indexOfClassInfo("D-Bus Interface");
        QDBusMessage signal=QDBusMessage::createSignal("/org/rde/theme","org.freedesktop.DBus.Properties","PropertiesChanged");
        signal << QLatin1String(metaObject()->classInfo(index).value());
        signal << m_updatedProperties;
        signal << m_invalidatedProperties;

        if(QDBusConnection::sessionBus().send(signal)){
        }

        m_updatedProperties.clear();
        m_invalidatedProperties.clear();
    }
Q_SIGNALS:
    void setIconTheme(const QString& iconTheme);
    void setCursorTheme(const QString& cursorTheme);
};
#endif