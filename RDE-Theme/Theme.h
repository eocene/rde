#ifndef THEME_H
#define THEME_H
#include "Adaptor.h"
#include <qpa/qplatformtheme.h>
#include <private/qiconloader_p.h>
#include <QDBusInterface>
#include <QFileInfo>
#include <QStandardPaths>
#include <QSettings>
#include <QSize>
#include <QVariant>
#include <QDebug>
class Theme:public QObject,public QPlatformTheme
{
    Q_OBJECT
public:
    Theme();
    virtual QVariant themeHint(ThemeHint hint) const override;
private:
    QDBusInterface* m_interface=nullptr;
    Adaptor* m_adaptor=nullptr;
    QString m_iconTheme;
    QString m_cursorTheme;
private slots:
    void propertiesChanged(const QString& interface,const QVariantMap& properties,QStringList c);
};
#endif