#ifndef RDETHEME_H
#define RDETHEME_H
#include <qpa/qplatformthemeplugin.h>
#include "Theme.h"
class rdetheme:public QPlatformThemePlugin
{
    Q_OBJECT    
    Q_PLUGIN_METADATA(IID QPlatformThemeFactoryInterface_iid FILE "metadata.json")
public:
    QPlatformTheme* create(const QString&,const QStringList&) override;
};
#endif