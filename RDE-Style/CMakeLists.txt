cmake_minimum_required(VERSION 3.16)
project(rdestyle LANGUAGES CXX)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "$ENV{RDE_DIR}/interface")
find_package(Qt6 REQUIRED COMPONENTS Core Widgets)
add_library(rdestyle SHARED  
  rdestyle.h rdestyle.cpp
  ProxyStyle.h ProxyStyle.cpp
)
target_link_libraries(rdestyle PRIVATE Qt6::Core Qt::Widgets)
target_compile_definitions(rdestyle PRIVATE RDESTYLE_LIBRARY)