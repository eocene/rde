#include "ProxyStyle.h"
ProxyStyle::ProxyStyle(const QString& key):QProxyStyle(key)
{
}
int ProxyStyle::styleHint(QStyle::StyleHint hint,const QStyleOption* option,const QWidget* widget,QStyleHintReturn* returnData) const
{
    return QProxyStyle::styleHint(hint,option,widget,returnData);
}
ProxyStyle::~ProxyStyle()
{
}