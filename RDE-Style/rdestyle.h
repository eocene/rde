#ifndef RDESTYLE_H
#define RDESTYLE_H
#include <QStylePlugin>
#include "ProxyStyle.h"
class rdestyle:public QStylePlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QStyleFactoryInterface" FILE "metadata.json")
public:
    QStyle* create(const QString& key) override;
};
#endif