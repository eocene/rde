#ifndef PROXYSTYLE_H
#define PROXYSTYLE_H
#include <QProxyStyle>
#include <QStyleFactory>
#include <QStyleOption>
#include <QDebug>
class ProxyStyle : public QProxyStyle
{
    Q_OBJECT
public:
    explicit ProxyStyle(const QString& key);
    virtual ~ProxyStyle();
    int styleHint(StyleHint hint,const QStyleOption* option,const QWidget* widget,QStyleHintReturn* returnData) const override;
};
#endif