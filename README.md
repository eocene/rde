# RDE - R Desktop Environment

RDE is a feature-rich modular desktop environment for use with the [Wayfire](https://wayfire.org/) compositor.

It's design goal is to provide the user with a fast, easy-to-use and aesthetically pleasing working environment,
to provide near-instant access to all of your media, and to reduce the need for third-party applications for daily
tasks as much as possible.

It works especially well with multimonitor setups.

Read the [wiki](https://gitlab.com/eocene/rde/-/wikis/RDE) for more information. 
You can also join the RDE [Discord](https://discord.gg/JC48Q2ebdY).

![Preview](data/preview/rde.png "RDE")