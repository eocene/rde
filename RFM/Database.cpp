#include "Database.h"
Database::Database()
{
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));
    if(dir.mkpath(dir.absolutePath())){
        QSqlDatabase db=QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(dir.absolutePath()+"/database");
        db.setConnectOptions("QSQLITE_ENABLE_SHARED_CACHE");
        if(db.open()){
            addTable("audio",QStringList("title") << "artist" << "album" << "genre" << "year");
            addTable("video",QStringList("title"));
            addTable("pictures",QStringList("title"));
            addTable("documents",QStringList("title"));


        }
    }
}
void Database::addTable(const QString &name,const QStringList& columns)
{
    QSqlDatabase db=QSqlDatabase::database();
    if(db.open()){
        QSqlQuery q(db);
        if(q.exec("CREATE TABLE "+name+"(file,"+columns.join(",")+",PRIMARY KEY(file))")){
            qDebug() << "Table created";

        }



    }
    //     if(db.tables().contains(table))
    //         setTable(table);
    //     else{
    //             import(table);
    //         q.finish();
    //     }
    // }
    // else
    //     qDebug() << "Failed to open " << table;


}
Database::~Database()
{
    qDebug() << "Deleting database!";
}
Database* Database::instance(){if(!s_instance)s_instance=new Database;return s_instance;}
Database* Database::s_instance;