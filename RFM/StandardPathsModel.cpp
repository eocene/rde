#include "StandardPathsModel.h"
StandardPathsModel::StandardPathsModel()
{
    m_Paths.append(QStringList("Audio") << "folder-sound" << QStandardPaths::standardLocations(QStandardPaths::MusicLocation).first());
    m_Paths.append(QStringList("Video") << "folder-video" << QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).first());
    m_Paths.append(QStringList("Pictures") << "folder-image" << QStandardPaths::standardLocations(QStandardPaths::PicturesLocation).first());
    m_Paths.append(QStringList("Documents") << "folder-documents" << QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first());
}
QVariant StandardPathsModel::data(const QModelIndex &index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return QVariant(m_Paths.at(index.row()).first());
    case Qt::DecorationRole:return QVariant(m_Paths.at(index.row()).at(1));
    case Qt::UserRole:return QVariant(m_Paths.at(index.row()).last());
    }
}