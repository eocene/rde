#include "PreviewItem.h"
void PreviewItem::setFilePath(const QString& path)
{
    m_file.setFile(path);
    QMimeType mime=mimeDb.mimeTypeForFile(path);
    if(mime.name().startsWith("audio/"))
        emit previewAudio();
    else if(mime.name()=="image/gif")
        emit previewAnimation();
    else if(mime.name().startsWith("image/"))
        emit previewImage();
    else if(mime.name()=="application/pdf")
        emit previewPDF();
    else if(mime.name()=="application/zip") // or other archives
        emit previewArchive();
    else if(mime.name()=="video/x-mng")
        emit previewAnimation();
    else if(mime.name().startsWith("video/"))
        emit previewVideo();
    else if (mime.inherits("text/plain"))
        emit previewText();
    emit filePathChanged();
    emit fileNameChanged();
}