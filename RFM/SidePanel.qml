import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
Pane{
    padding:0
    background:Item{}    
    contentItem:ColumnLayout{
        spacing:0
        VolumeList{
            id:volumeList
            Layout.fillWidth:true
            Layout.preferredHeight:contentHeight
        }
        MediaList{
            id:mediaList
            Layout.fillWidth:true
            Layout.preferredHeight:contentHeight
        }
        BookmarkList{
            id:bookmarkList
            Layout.fillWidth:true
            Layout.fillHeight:true
        }
    }
}