#include "TextPreview.h"
TextPreview::TextPreview()
{
    m_watcher=new QFutureWatcher<QByteArray>(this);

    connect(m_watcher,&QFutureWatcher<QByteArray>::started,this,&TextPreview::started);
    connect(m_watcher,&QFutureWatcher<QByteArray>::finished,this,&TextPreview::onFinished);

    //    connect(watcher,SIGNAL(started()),this,SIGNAL(started()));
    //    connect(watcher,SIGNAL(finished()),this,SLOT(finished()));
    m_fileWatcher=new QFileSystemWatcher(this);


//    connect(m_fileWatcher,SIGNAL(fileChanged(QString)),this,SLOT(fileChanged(QString)));
    connect(m_fileWatcher,&QFileSystemWatcher::fileChanged,this,&TextPreview::onFileChanged);
}
void TextPreview::setFile(const QString& file)
{
    if(m_fileWatcher->addPath(file)){
        onFileChanged(file);
    }
}
void TextPreview::onFileChanged(const QString& file)
{
//    m_watcher->setFuture(QtConcurrent::run(this,&TextPreview::thread,file));
    m_watcher->setFuture(QtConcurrent::run(&TextPreview::thread,this,file));
}
extern QByteArray TextPreview::thread(const QString& file)
{
    QFile f(file);
    if(f.open(QIODevice::ReadOnly))
        return f.readAll();
    return 0;
}
void TextPreview::onFinished()
{
    m_text=m_watcher->result();
    emit textChanged();
}