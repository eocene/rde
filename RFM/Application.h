#ifndef APPLICATION_H
#define APPLICATION_H
#include <QDesktopServices>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSettings>
#include "MimeIconProvider.h"

#include "Database.h"


class Application:public QGuiApplication
{
    Q_OBJECT
    Q_PROPERTY(bool sidePanel READ sidePanel WRITE setSidePanel NOTIFY sidePanelChanged)
    Q_PROPERTY(bool splitView READ splitView WRITE setSplitView NOTIFY splitViewChanged)
    Q_PROPERTY(bool preview READ preview WRITE setPreview NOTIFY previewChanged)
    Q_PROPERTY(QString currentFile READ currentFile WRITE setCurrentFile NOTIFY currentFileChanged)
    Q_PROPERTY(bool isCurrentFileDir READ isCurrentFileDir WRITE setIsCurrentFileDir NOTIFY isCurrentFileDirChanged FINAL)
public:
    explicit Application(int &argc,char **argv);
    ~Application();
    Q_INVOKABLE bool openCurrentFile(){return QDesktopServices::openUrl(QUrl::fromLocalFile(m_currentFile));}    
public slots:
    Q_SCRIPTABLE void quit();
private:
    QQmlApplicationEngine* m_engine=nullptr;
    bool m_sidePanel;
    inline bool sidePanel(){return m_sidePanel;}
    inline void setSidePanel(bool visible){m_sidePanel=visible;}
    bool m_splitView;
    inline bool splitView(){return m_splitView;}
    void setSplitView(bool visible){m_splitView=visible;}
    bool m_preview;
    inline bool preview(){return m_preview;}
    void setPreview(bool visible){m_preview=visible;}
    QString m_currentFile;
    QString currentFile(){return m_currentFile;}
    void setCurrentFile(const QString& file){m_currentFile=file;emit currentFileChanged();}
    bool m_isCurrentFileDir;
    bool isCurrentFileDir(){return m_isCurrentFileDir;}
    void setIsCurrentFileDir(bool dir){m_isCurrentFileDir=dir;emit isCurrentFileDirChanged();}
signals:
    void currentFileChanged();
    void isCurrentFileDirChanged();
    void sidePanelChanged();
    void splitViewChanged();
    void previewChanged();
};
#endif