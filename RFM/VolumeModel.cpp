#include "VolumeModel.h"
VolumeModel::VolumeModel()
{
    m_watcher=new QFutureWatcher<QList<QStorageInfo>>(this);
    connect(m_watcher,&QFutureWatcherBase::finished,[=](){
        beginResetModel();
        m_volumes=m_watcher->result();
        endResetModel();
        m_watcher->deleteLater();
    });
    m_watcher->setFuture(QtConcurrent::run(&VolumeModel::getVolumes,this));
}
extern QVector<QStorageInfo> VolumeModel::getVolumes()
{
    QVector<QStorageInfo> v;
    foreach(QStorageInfo s,QStorageInfo::mountedVolumes()){
        if(s.isValid() && s.isReady() && !s.isReadOnly() && !s.isRoot() && s.device()!="tmpfs" && !s.displayName().startsWith("/var") && !s.displayName().startsWith("/.") && s.displayName()!="/boot")
            v.append(s);        
    }
    return v;
}
QVariant VolumeModel::data(const QModelIndex &index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return QVariant(m_volumes.at(index.row()).displayName());
    case Qt::DecorationRole:return QVariant("image://MimeIcon/"+m_volumes.at(index.row()).rootPath());
    case Qt::UserRole:return QVariant(m_volumes.at(index.row()).rootPath());
    case Qt::UserRole+1:return QVariant(m_volumes.at(index.row()).bytesTotal());
    case Qt::UserRole+2:return QVariant(m_volumes.at(index.row()).bytesTotal()-m_volumes.at(index.row()).bytesAvailable());
    }
}