#ifndef DATABASE_H
#define DATABASE_H
#include <QDir>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QDebug>
class Database:public QObject
{
    Q_OBJECT
public:
    static Database* instance();
    ~Database();
    void addTable(const QString& name,const QStringList& columns);
private:
    Database();
    static Database* s_instance;
};
#endif