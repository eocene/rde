#ifndef PREVIEWITEM_H
#define PREVIEWITEM_H
#include <QFileInfo>
#include <QMimeDatabase>
#include <QQuickItem>
class PreviewItem:public QQuickItem
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(QString filePath READ filePath WRITE setFilePath NOTIFY filePathChanged)
    Q_PROPERTY(QString fileName READ fileName NOTIFY fileNameChanged)
public:
    PreviewItem(){}
private:
    QFileInfo m_file;
    QMimeDatabase mimeDb;
    inline QString filePath(){return m_file.absoluteFilePath();}
    void setFilePath(const QString& path);
    inline QString fileName(){return m_file.fileName();}
signals:
    void filePathChanged();
    void fileNameChanged();
    void previewText();
    void previewAudio();
    void previewImage();
    void previewVideo();
    void previewPDF();
    void previewAnimation();
    void previewArchive();
};
#endif