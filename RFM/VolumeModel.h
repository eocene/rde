#ifndef VOLUMEMODEL_H
#define VOLUMEMODEL_H
#include <QtConcurrentRun>
#include <QAbstractListModel>
#include <QFutureWatcher>
#include <QQmlEngine>
#include <QStorageInfo>
class VolumeModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit VolumeModel();    
private:
    int rowCount(const QModelIndex &parent)const{Q_UNUSED(parent);return m_volumes.count();}
    QVariant data(const QModelIndex &index,int role)const override;
    QHash<int,QByteArray>roleNames()const override
    {
        QHash<int,QByteArray> h=QAbstractListModel::roleNames();
        h[Qt::UserRole]="path";
        h[Qt::UserRole+1]="Total";
        h[Qt::UserRole+2]="Percent";
        return h;
    }
    QFutureWatcher<QVector<QStorageInfo>>* m_watcher=nullptr;
    QList<QStorageInfo> m_volumes;
    QList<QStorageInfo> getVolumes();
};
#endif