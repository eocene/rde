import org.rde.ui
import org.rde.rfm
RList{
    header:RListHeader{label:'Bookmarks'}
    model:BookmarkModel{}
    delegate:RListDelegate{
        onClicked:currentBrowser.model.folder=Folder
    }
}