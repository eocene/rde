#include "BookmarkModel.h"
BookmarkModel::BookmarkModel()
{
    m_file.setFile(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation)+"/rfm_bookmarks.cfg");
    if(!m_file.isReadable()){
        // .. create default bookmarks file?
    }
    m_fileWatcher=new QFileSystemWatcher(this);
    connect(m_fileWatcher,&QFileSystemWatcher::fileChanged,this,&BookmarkModel::onFileChanged);
    if(m_fileWatcher->addPath(m_file.absoluteFilePath()))
        onFileChanged(m_fileWatcher->files().first());
}
// file needs to be re-added to fileWatcher after it has been changed
void BookmarkModel::onFileChanged(const QString& path)
{
    m_watcher=new QFutureWatcher<QFileInfoList>(this);
    connect(m_watcher,&QFutureWatcherBase::finished,[=](){
        beginResetModel();
        m_list=m_watcher->result();
        endResetModel();
        m_watcher->deleteLater();
    });
    m_watcher->setFuture(QtConcurrent::run(&BookmarkModel::readFile,this,path));
}
extern QFileInfoList BookmarkModel::readFile(const QString& path)
{
    QFileInfoList l;
    QSettings settings(path,QSettings::NativeFormat);
    settings.beginGroup("Bookmarks");
    foreach(QString s,settings.allKeys()){
        QFileInfo i(settings.value(s).toString());
        if(i.isDir()){
            l << i;
        }
    }
    settings.endGroup();
    return l;
}
QVariant BookmarkModel::data(const QModelIndex &index,int role) const
{
    switch(role){
    case Qt::DisplayRole:return QVariant(m_list.at(index.row()).baseName());
    case Qt::DecorationRole:return QVariant("image://MimeIcon/"+m_list.at(index.row()).absoluteFilePath());
    case Qt::UserRole:return QVariant(m_list.at(index.row()).absoluteFilePath());
    }
}