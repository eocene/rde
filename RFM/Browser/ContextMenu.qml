import org.rde.ui
RMenu{
    property string file;
    property bool isFolder;
    RMenuItem{action:openAction}
    RMenuItem{action:cutAction}
    RMenuItem{action:copyAction}
    RMenuItem{action:pasteAction}
    RMenuItem{action:deleteAction}
    RMenuItem{action:renameAction}
    RMenuItem{action:propertiesAction}
}