import QtQuick
import org.rde.ui
import org.rde.rfm
FileView{
    property alias view:view
    RGrid{
        id:view
        anchors.fill:parent
        model:browserItem.model
        onCurrentItemChanged:{
            RFM.currentFile=currentItem.file
            RFM.isCurrentFileDir=currentItem.dir
        }
        Keys.onPressed:(event)=>{
                           if(event.key===Qt.Key_Return){
                               openAction.trigger()
                           }
                           else if(event.key===Qt.Key_Delete){
                               deleteAction.trigger()
                           }
                       }


        // Shortcut{
        //     sequence:'Return'
        //     onActivated:openAction.trigger()
        // }

        delegate:Item{
            width:view.cellWidth
            height:view.cellHeight
            clip:true
            property string file:decoration
            property bool dir:fileIsDir
            RIcon{
                anchors{left:parent.left;right:parent.right;top:parent.top;bottom:parent.verticalCenter}
                source:'image://MimeIcon/'+decoration
                image.width:64
            }
            RText{
                id:label
                text:model.display
                anchors{left:parent.left;right:parent.right;top:parent.verticalCenter;bottom:parent.bottom}
                verticalAlignment:Text.AlignTop
                horizontalAlignment:Text.AlignHCenter
            }
            MouseArea{
                anchors.fill:parent
                acceptedButtons:Qt.LeftButton|Qt.RightButton
                onPressed:(mouse)=>{
                              if(mouse.button===Qt.LeftButton){view.currentIndex=index}
                          }
                onClicked:(mouse)=>{
                              if(mouse.button===Qt.RightButton){
                                  view.currentIndex=index
                                  menu=Qt.createComponent('ContextMenu.qml').createObject(view)
                                  menu.file=decoration
                                  // menu.isFolder=fileIsDir
                                  menu.popup()
                              }
                          }
                onDoubleClicked:{
                    openAction.trigger()
                    //open(fileIsDir,decoration)

                }
            }
        }
    }
    RBarShadow{
        parent:view;
        anchors{left:parent.left;right:parent.right;top:parent.top}
    }
    Component.onCompleted:view.forceActiveFocus()
}