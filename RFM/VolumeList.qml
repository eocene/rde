import QtQuick.Controls
import org.rde.ui
import org.rde.rfm
RList{
    header:RListHeader{label:'Volumes'}
    model:VolumeModel{}
    delegate:RListDelegate{
        background:ProgressBar{
            to:Total
            value:Percent
            background:RBar{}
        }
        onClicked:currentBrowser.model.rootPath=path
    }
}