#ifndef MIMEICONPROVIDER_H
#define MIMEICONPROVIDER_H
#include <QFileInfo>
#include <QIcon>
#include <QMimeDatabase>
#include <QQuickImageProvider>
#include <QSettings>
class MimeIconProvider:public QQuickImageProvider
{
public:
    MimeIconProvider():QQuickImageProvider(Pixmap,ForceAsynchronousImageLoading){}
    QPixmap requestPixmap(const QString &id,QSize *size,const QSize &requestedSize)
    {
        Q_UNUSED(size);
        QMimeType type=mimeDb.mimeTypeForFile(id);
        if(type.name()=="inode/directory"){
            QFileInfo f(id+"/.directory");
            if(f.exists()){
                QSettings s(f.absoluteFilePath(),QSettings::NativeFormat);
                QString i=s.value("Desktop Entry/Icon").toString();
                if(!i.isEmpty() && QIcon::hasThemeIcon(i))
                    return QIcon::fromTheme(i).pixmap(requestedSize);
            }
        }
        else if(type.name().startsWith("image/")){
            QPixmap pm;
            if(pm.load(id))
                return pm.scaled(requestedSize,Qt::KeepAspectRatio,Qt::SmoothTransformation);
        }
        if(QIcon::hasThemeIcon(type.iconName()))
            return QIcon::fromTheme(type.iconName()).pixmap(requestedSize);
        else
            return QIcon::fromTheme("unknown").pixmap(requestedSize);
    }
private:
    QMimeDatabase mimeDb;
};
#endif