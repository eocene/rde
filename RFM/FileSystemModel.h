#ifndef FILESYSTEMMODEL_H
#define FILESYSTEMMODEL_H
#include <QFileSystemModel>
#include <QQmlEngine>
class FileSystemModel:public QFileSystemModel
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(QString rootPath READ rootPath WRITE setRootPath NOTIFY rootPathChanged)
public:
    FileSystemModel();





    // enum Roles  {
    //     SizeRole = Qt::UserRole + 4,
    //     DisplayableFilePermissionsRole = Qt::UserRole + 5,
    //     LastModifiedRole = Qt::UserRole + 6,
    //     UrlStringRole = Qt::UserRole + 7
    // };
    // Q_ENUM(Roles)

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE
    {
        // if (index.isValid() && role >= SizeRole) {
        //     switch (role) {
        //     case SizeRole:
        //         //return QVariant(sizeString(fileInfo(index)));
        //         return 8;
        //     case DisplayableFilePermissionsRole:
        //         return "wee";
        //         //return QVariant(permissionString(fileInfo(index)));
        //     case LastModifiedRole:
        //         return "eeke";
        //         //return QVariant(fileInfo(index).lastModified().toString(Qt::SystemLocaleShortDate));
        //     case UrlStringRole:
        //         return "url";
        //         //return QVariant(QUrl::fromLocalFile(filePath(index)).toString());
        //     default:
        //         break;
        //     }
        // }
        return QFileSystemModel::data(index, role);
    }

    QHash<int,QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QFileSystemModel::roleNames();

        qDebug() << h;


        // result.insert(SizeRole, QByteArrayLiteral("size"));
        // result.insert(DisplayableFilePermissionsRole, QByteArrayLiteral("displayableFilePermissions"));
        // result.insert(LastModifiedRole, QByteArrayLiteral("lastModified"));
        return h;
    }
};
#endif