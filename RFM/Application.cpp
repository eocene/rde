#include "Application.h"
Application::Application(int &argc,char **argv):QGuiApplication(argc,argv)
{
    setOrganizationName("rde");
    setApplicationName("rfm");
    setApplicationDisplayName("RFM");

    QSettings s;
    m_sidePanel=s.value("sidepanel",true).toBool();
    m_splitView=s.value("splitview",false).toBool();
    m_preview=s.value("preview",false).toBool();

    m_engine=new QQmlApplicationEngine(this);

    m_engine->rootContext()->setContextProperty("RFM",this);
    m_engine->rootContext()->setContextProperty("Database",Database::instance());

    m_engine->addImageProvider("MimeIcon",new MimeIconProvider);
    m_engine->load("qrc:/Window.qml");
}
void Application::quit()
{
    qGuiApp->quit();
}
Application::~Application()
{
    QSettings s;
    s.setValue("sidepanel",m_sidePanel);
    s.setValue("splitview",m_splitView);
    s.setValue("preview",m_preview);
}