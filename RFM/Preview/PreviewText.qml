import QtQuick
import org.rde.rfm
import org.rde.ui
TextPreview{
    id:textPreview
    anchors.fill:parent
    file:preview.filePath
    RTextEdit{
        anchors{fill:parent;margins:RShell.margin}
        text:textPreview.text
//        readOnly:true
    }
}