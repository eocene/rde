import QtQuick
Image{
    id:imagePreview
    fillMode:Image.PreserveAspectFit    
    antialiasing:true
    mipmap:true
    asynchronous:true
    source:'file://'+preview.filePath
    cache:false
}