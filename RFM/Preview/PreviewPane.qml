import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.rde.ui
import org.rde.rfm
Pane{
    padding:0
    background:Item{}
    property alias preview:preview
    contentItem:ColumnLayout{
        spacing:0
        RBarRaised{
            Layout.fillWidth:true
            RText2{
                anchors{fill:parent;margins:4}
                verticalAlignment:Text.AlignVCenter
                horizontalAlignment:Text.AlignHCenter
                text:preview.fileName
            }
        }
        Loader{
            id:loader
            asynchronous:true
            Layout.fillWidth:true
            Layout.fillHeight:true
        }
        PreviewItem{
            id:preview
            onPreviewAnimation:loader.source='PreviewAnimation.qml'
            onPreviewArchive:loader.source='PreviewArchive.qml'
            onPreviewAudio:loader.source='PreviewAudio.qml'
            onPreviewText:loader.source='PreviewText.qml'
            onPreviewImage:loader.source='PreviewImage.qml'
            onPreviewVideo:loader.source='PreviewVideo.qml'
            onPreviewPDF:loader.source='PreviewPDF.qml'
            filePath:RFM.currentFile
        }
    }
}