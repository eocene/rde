import QtQuick
import QtMultimedia
Item{
    MediaPlayer{
        id:mediaPlayer
        source:'file://'+preview.filePath
        loops:200
        audioOutput:AudioOutput{}
        videoOutput:output
        onSourceChanged:play()
    }
    VideoOutput{
        id:output
        anchors.fill:parent
    }
}