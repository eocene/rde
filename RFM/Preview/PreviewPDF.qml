import QtQuick
import QtQuick.Pdf
Item{
    PdfDocument{
        id:doc
        source:'file:/'+preview.filePath
    }
    PdfMultiPageView{
        id:view
        clip:true
        anchors.fill:parent
        document:doc
    }
}