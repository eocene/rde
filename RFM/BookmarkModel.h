#ifndef BOOKMARKMODEL_H
#define BOOKMARKMODEL_H
#include <QtConcurrentRun>
#include <QAbstractListModel>
#include <QFileSystemWatcher>
#include <QFileInfo>
#include <QFileInfoList>
#include <QFutureWatcher>
#include <QQmlEngine>
#include <QSettings>
#include <QStandardPaths>
class BookmarkModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit BookmarkModel();
private:
    int rowCount(const QModelIndex &parent)const override{Q_UNUSED(parent);return m_list.size();}
    QVariant data(const QModelIndex &index,int role) const override;
    QHash<int,QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QAbstractListModel::roleNames();
        h[Qt::UserRole]="Folder";
        return h;
    }
    QFutureWatcher<QFileInfoList>* m_watcher=nullptr;
    QFileSystemWatcher* m_fileWatcher=nullptr;
    QFileInfoList readFile(const QString& path);
    QFileInfoList m_list;
    QFileInfo m_file;
private slots:
    void onFileChanged(const QString& path);
};
#endif