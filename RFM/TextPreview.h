#ifndef TEXTPREVIEW_H
#define TEXTPREVIEW_H
#include <QQuickItem>
#include <QtConcurrentRun>
#include <QFutureWatcher>
#include <QFileSystemWatcher>
#include <QFile>
class TextPreview:public QQuickItem
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(QString file WRITE setFile)
    Q_PROPERTY(QByteArray text READ text NOTIFY textChanged)
public:
    explicit TextPreview();
private:
    QString m_file;
    void setFile(const QString& file);
    QByteArray m_text;
    QByteArray text()const{return m_text;}
    QByteArray thread(const QString &file);
    QFileSystemWatcher* m_fileWatcher;
    QFutureWatcher<QByteArray>* m_watcher;
private slots:
    void onFinished();
    void onFileChanged(const QString& file);
signals:
    void started();
    void textChanged();
};
#endif