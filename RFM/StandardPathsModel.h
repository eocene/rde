#ifndef STANDARDPATHSMODEL_H
#define STANDARDPATHSMODEL_H
#include <QQmlEngine>
#include <QAbstractListModel>
#include <QStandardPaths>
class StandardPathsModel:public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit StandardPathsModel();
private:
    int rowCount(const QModelIndex &parent)const{Q_UNUSED(parent);return m_Paths.size();}
    QVariant data(const QModelIndex &index,int role)const override;
    QHash<int,QByteArray>roleNames()const override
    {
        QHash<int,QByteArray> h=QAbstractListModel::roleNames();
        h[Qt::UserRole]="path";
        return h;
    }
    QList<QStringList> m_Paths;
};
#endif