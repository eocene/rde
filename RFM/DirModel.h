#ifndef DIRMODEL_H
#define DIRMODEL_H
#include <QtConcurrent>
#include <QAbstractTableModel>
#include <QDir>
#include <QFileSystemWatcher>
#include <QFutureWatcher>
#include <QMimeDatabase>
#include <QQmlEngine>
#include <QDebug>
class DirModel:public QAbstractTableModel
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(QString rootPath READ rootPath WRITE setRootPath NOTIFY rootPathChanged)
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY filterChanged)
    //Q_PROPERTY(int currentRow READ currentRow WRITE setCurrentRow NOTIFY currentRowChanged)
    Q_PROPERTY(int itemCount READ itemCount NOTIFY itemCountChanged)
public:
    explicit DirModel();
    ~DirModel();
    Q_INVOKABLE inline void goPrevious(){}
    Q_INVOKABLE inline void goNext(){}
    Q_INVOKABLE inline void goUp(){if(m_folder.cdUp())update();}
private:
    int rowCount(const QModelIndex& =QModelIndex())const override{return m_list.size();}
    int columnCount(const QModelIndex& =QModelIndex())const override{return m_columns.size();}
    QHash<int,QByteArray> roleNames() const override
    {
        QHash<int,QByteArray> h=QAbstractTableModel::roleNames();
        h[Qt::TextAlignmentRole]="TextAlignment";
        h[Qt::UserRole]="type";
        h[Qt::UserRole+1]="fileIsDir";
        h[Qt::UserRole+2]="filePath";
        return h;
    }
    QVariant headerData(int section,Qt::Orientation orientation,int role=Qt::DisplayRole) const override;
    QVariant data(const QModelIndex &index,int role) const override;
    QStringList m_columns;
    QFutureWatcher<QFileInfoList>* m_futureWatcher=nullptr;
    QFileSystemWatcher* m_fileWatcher=nullptr;
    QFileInfoList m_list;
    inline void update(){m_futureWatcher->setFuture(QtConcurrent::run(&DirModel::updateThread,this));}
    QFileInfoList updateThread();
    QMimeDatabase mimeDb;
    int itemCount(){return m_list.size();}
    QDir m_folder;
    QString rootPath(){return m_folder.path();}
    inline void setRootPath(const QString& folder){m_folder.setPath(folder);m_filter.clear();emit filterChanged();update();}
    QString m_filter;
    QString filter(){return m_filter;}
    inline void setFilter(const QString& filter){m_filter=filter;update();}

    // int m_currentRow;
    // inline int currentRow(){return m_currentRow;}
    // inline void setCurrentRow(int row){


    //     qDebug() << "DIR MODEL ROW:" << row;

    //     m_currentRow=row;
    //     emit currentRowChanged();
    //     emit fileSelected(m_list.at(row).filePath());


    //     qDebug() << "SELECTED FILE" << m_list.at(row).filePath();


    // }


    QString m_name;
private slots:
    void onDirectoryChanged(const QString& path);
signals:
    void rootPathChanged();
    void filterChanged();
    void currentRowChanged();
    void itemCountChanged();
    void fileSelected(QString file);
};
#endif