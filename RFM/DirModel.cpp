#include "DirModel.h"
DirModel::DirModel()
{

    m_name="LeftPanel";

    m_futureWatcher=new QFutureWatcher<QFileInfoList>(this);
    connect(m_futureWatcher,&QFutureWatcher<QFileInfoList>::finished,[=](){
        beginResetModel();
        m_list=m_futureWatcher->result();
        endResetModel();
        if(!m_fileWatcher->directories().isEmpty())
            m_fileWatcher->removePaths(m_fileWatcher->directories());
        if(m_fileWatcher->addPath(m_folder.path())){
            //...
        }

        emit rootPathChanged();
        emit itemCountChanged();
    });
    //m_currentRow=0;
    //    m_columns << "File" << "Type" << "Size" << "Modified";
    m_columns << "File" << "Type" << "Size";
    m_fileWatcher=new QFileSystemWatcher(this);
    connect(m_fileWatcher,&QFileSystemWatcher::directoryChanged,this,&DirModel::onDirectoryChanged);
    m_folder=QDir::home();
}
extern QFileInfoList DirModel::updateThread()
{
    return m_folder.entryInfoList(QStringList("*"+m_filter+"*"),QDir::Files|QDir::Dirs|QDir::NoDotAndDotDot,QDir::DirsFirst);
}
void DirModel::onDirectoryChanged(const QString& path)
{
    // this works, but it resets the whole model
    setRootPath(path);
}
QVariant DirModel::headerData(int section,Qt::Orientation orientation,int role) const
{
    return m_columns.at(section);
}
QVariant DirModel::data(const QModelIndex &index,int role) const
{
    switch(role){
    case Qt::DisplayRole:{
        QFileInfo i=m_list.at(index.row());
        if(index.column()==0)return QVariant(i.baseName());
        else if(index.column()==1)return QVariant(mimeDb.mimeTypeForFile(i).comment());
        else if(index.column()==2)return QVariant(i.size());
    }
    case Qt::DecorationRole:return QVariant(m_list.at(index.row()).absoluteFilePath());
    case Qt::TextAlignmentRole:return Qt::AlignLeft;
    case Qt::UserRole:{if(index.column()==0)return "IconText";else return "Text";}
    case Qt::UserRole+1:return m_list.at(index.row()).isDir();
    case Qt::UserRole+2:{
        qDebug() << "FILE PATH REQUESTED:" << m_list.at(index.row()).absoluteFilePath();
        return m_list.at(index.row()).absoluteFilePath();
    }
    }
}
DirModel::~DirModel()
{
    QSettings s;
    s.beginGroup(m_name);
    s.setValue("path",rootPath());
}